<?php
/* Smarty version 3.1.30, created on 2018-07-19 06:54:08
  from "/var/www/biyebiye/public_html/ow_plugins/protectedphotos/views/components/album_name_list.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b506dd06705b2_70224805',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ba8d4893227d1eedced39d0c01516533481c45ef' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_plugins/protectedphotos/views/components/album_name_list.html',
      1 => 1479204282,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b506dd06705b2_70224805 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
?>
<div class="ow_dropdown_list_wrap">
    <ul class="ow_dropdown_list" style="z-index: 10;">
        <li><?php echo smarty_function_text(array('key'=>"photo+create_album"),$_smarty_tpl);?>
<span class="ow_add_item"></span></li>
        <?php if (!empty($_smarty_tpl->tpl_vars['albumNameList']->value)) {?>
            <li class="ow_dropdown_delimeter"><div></div></li>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['albumNameList']->value, 'album', false, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['album']->value) {
?>
                <li data-name="<?php echo $_smarty_tpl->tpl_vars['album']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['album']->value;
if (in_array($_smarty_tpl->tpl_vars['id']->value,$_smarty_tpl->tpl_vars['protectedAlbums']->value)) {?><span class="ow_photo_icon ow_pass_protected_icon ow_right"></span><?php }?></li>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

        <?php }?>
    </ul>
</div>
<div class="ow_dropdown_arrow_down upload_photo_spinner"></div><?php }
}
