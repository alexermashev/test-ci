<?php

STRIPETRIAL_CLASS_EventHandler::getInstance()->init();

OW::getRouter()->addRoute(new OW_Route(STRIPETRIAL_BOL_Service::PLUGIN_KEY . '_main_index', STRIPETRIAL_BOL_Service::PLUGIN_KEY . '/index/', 'STRIPETRIAL_CTRL_Main', 'index'));

OW::getRouter()->addRoute(new OW_Route(STRIPETRIAL_BOL_Service::PLUGIN_KEY . '_main_ajaxResponder', STRIPETRIAL_BOL_Service::PLUGIN_KEY . '/ajax-responder/', 'STRIPETRIAL_CTRL_Main', 'ajaxResponder'));

OW::getRouter()->addRoute(
    new OW_Route(STRIPETRIAL_BOL_Service::PLUGIN_KEY . '.handler', STRIPETRIAL_BOL_Service::PLUGIN_KEY . '/handler', 'STRIPETRIAL_CTRL_Main', 'handler')
);