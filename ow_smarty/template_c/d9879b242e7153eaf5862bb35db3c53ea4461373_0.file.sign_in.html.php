<?php
/* Smarty version 3.1.30, created on 2018-07-19 05:49:21
  from "/var/www/biyebiye/public_html/ow_system_plugins/base/mobile/views/components/sign_in.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b505ea1e1a656_12942333',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd9879b242e7153eaf5862bb35db3c53ea4461373' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_system_plugins/base/mobile/views/components/sign_in.html',
      1 => 1479204252,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b505ea1e1a656_12942333 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_form')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_url_for_route')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.url_for_route.php';
if (!is_callable('smarty_function_input')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_label')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.label.php';
if (!is_callable('smarty_function_submit')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.submit.php';
if (!is_callable('smarty_function_component')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.component.php';
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('name'=>'sign-in'));
$_block_repeat1=true;
echo smarty_block_form(array('name'=>'sign-in'), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

<div class="owm_login_form">
    <div class="clearfix owm_std_margin_top">
        <div class="owm_login_txt owm_float_left">
            <?php echo smarty_function_text(array('key'=>'base+base_sign_in_cap_label'),$_smarty_tpl);?>

        </div>
        <a href="<?php echo smarty_function_url_for_route(array('for'=>'base_forgot_password'),$_smarty_tpl);?>
" class="owm_forgot_txt owm_float_right"><?php echo smarty_function_text(array('key'=>'base+forgot_password_label'),$_smarty_tpl);?>
</a>
    </div>
    <div class="owm_login_field owm_login_username">
        <?php echo smarty_function_input(array('name'=>'identity'),$_smarty_tpl);?>

    </div>
    <div class="owm_login_field owm_login_pass">
        <?php echo smarty_function_input(array('name'=>'password'),$_smarty_tpl);?>

    </div>
    <div style="display:none;"><?php echo smarty_function_input(array('name'=>'remember'),$_smarty_tpl);
echo smarty_function_label(array('name'=>'remember'),$_smarty_tpl);?>
</div>
    <div class="owm_btn_wide owm_btn_positive owm_std_margin_top">
        <?php echo smarty_function_submit(array('name'=>'submit'),$_smarty_tpl);?>

    </div>
</div>
<?php $_block_repeat1=false;
echo smarty_block_form(array('name'=>'sign-in'), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>


<?php echo smarty_function_component(array('class'=>"BASE_MCMP_ConnectButtonList"),$_smarty_tpl);?>


<?php }
}
