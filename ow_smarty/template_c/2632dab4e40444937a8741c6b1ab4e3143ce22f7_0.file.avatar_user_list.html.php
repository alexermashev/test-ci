<?php
/* Smarty version 3.1.30, created on 2019-01-18 01:23:37
  from "/Users/esase/Sites/8418/ow_plugins/bookmarks/views/components/avatar_user_list.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c4170e9c3d816_51586841',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2632dab4e40444937a8741c6b1ab4e3143ce22f7' => 
    array (
      0 => '/Users/esase/Sites/8418/ow_plugins/bookmarks/views/components/avatar_user_list.html',
      1 => 1547792045,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c4170e9c3d816_51586841 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.text.php';
?>


<div class="ow_lp_avatars<?php if (!empty($_smarty_tpl->tpl_vars['css_class']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['css_class']->value;
}?>">
    <?php if (empty($_smarty_tpl->tpl_vars['users']->value)) {?>
        <div class="ow_nocontent"><?php echo smarty_function_text(array('key'=>'base+empty_user_avatar_list'),$_smarty_tpl);?>
</div>
    <?php } else { ?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['users']->value, 'user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['user']->value) {
?>
            <div class="ow_avatar<?php if (!empty($_smarty_tpl->tpl_vars['user']->value['class'])) {?> <?php echo $_smarty_tpl->tpl_vars['user']->value['class'];
}?>">
                <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['url'])) {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['user']->value['url'];?>
"><img alt=""<?php if (!empty($_smarty_tpl->tpl_vars['user']->value['title'])) {?> title="<?php echo $_smarty_tpl->tpl_vars['user']->value['title'];?>
"<?php }?> src="<?php echo $_smarty_tpl->tpl_vars['user']->value['src'];?>
" /></a>
                <?php } else { ?>
                <img alt="" <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['title'])) {?> title="<?php echo $_smarty_tpl->tpl_vars['user']->value['title'];?>
"<?php }?> src="<?php echo $_smarty_tpl->tpl_vars['user']->value['src'];?>
" />
                <?php }?>
                <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['label'])) {?><span class="ow_avatar_label"<?php if (!empty($_smarty_tpl->tpl_vars['user']->value['labelColor'])) {?> style="background-color: <?php echo $_smarty_tpl->tpl_vars['user']->value['labelColor'];?>
"<?php }?>><?php echo $_smarty_tpl->tpl_vars['user']->value['label'];?>
</span><?php }?>
            </div>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

        <?php if (!empty($_smarty_tpl->tpl_vars['view_more_array']->value)) {?>
            <a href="<?php echo $_smarty_tpl->tpl_vars['view_more_array']->value['url'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['view_more_array']->value['title'];?>
" class="avatar_list_more_icon"></a>
        <?php }?>
    <?php }?>
</div>
<?php }
}
