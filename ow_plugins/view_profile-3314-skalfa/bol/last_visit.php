<?php

class VIEWPROFILE_BOL_LastVisit extends OW_Entity
{
    /**
     * @var int
     */
    public $userId;

    /**
     * @var int
     */
    public $dateTime;

    /**
     * @var int
     */
    public $completeTime;
}
