<?php

use Behat\MinkExtension\Context\MinkContext;
use Skadate\Traits\Db;
use Behat\Behat\Event\ScenarioEvent;
use WebDriver\Key;

/**
 * Features context.
 */
class FeatureContext extends MinkContext
{
    use Db;

    protected $baseFixturesPath;
    protected $params = [];
    protected $skadateConfig = [];

    protected $applyStartFixtures = [
        'core/change_configs.sql',
        'core/clean_basic_tables.sql'
    ];

    protected $applyEndFixtures = [];

    /**
     * Initializes context.
     * Every scenario gets its own context object.
     *
     * @param array $parameters context parameters (set them up through behat.yml)
     */
    public function __construct(array $parameters)
    {
        $this->params = $parameters;
        $this->baseFixturesPath = realpath(__DIR__ . '/../../fixtures') . '/';

        // parse the skadate config file
        $skadateConfigFile = file_get_contents($this->params['skadate_config']);

        preg_match_all(
            "/define\('(?<configName>.*?)',\s*(?<configValue>.*)\);/",
            $skadateConfigFile,
            $configMatches
        );

        // process the one
        for ($i = 0; $i < count($configMatches['configName']); $i++) {
            $this->skadateConfig[$configMatches['configName'][$i]] = trim($configMatches['configValue'][$i], "'");
        }

        // set db necessary settings
        $this->setDbParams($this->skadateConfig['OW_DB_NAME'], 
                $this->skadateConfig['OW_DB_HOST'], 
                $this->skadateConfig['OW_DB_USER'], 
                $this->skadateConfig['OW_DB_PASSWORD'],
                $this->skadateConfig['OW_DB_PREFIX']);
    }

    /**
     * @BeforeScenario
     */
    public function setBaseUrl(ScenarioEvent $event)
    {
        $this->setMinkParameter('base_url', $this->skadateConfig['OW_URL_HOME']);
    }

    /**
     * @BeforeScenario
     */
    public function applyStartFixtures(ScenarioEvent $event)
    {
        if (count($this->applyStartFixtures)) {
            foreach($this->applyStartFixtures as $fixture) {
                $this->executeSqlFile($this->baseFixturesPath . $fixture);
            }

            $this->applyStartFixtures = [];
        }
    }

    /**
     * @AfterScenario
     */
    public function applyEndFixtures(ScenarioEvent $event)
    {
        if (count($this->applyEndFixtures)) {
            foreach($this->applyEndFixtures as $fixture) {
                $this->executeSqlFile($this->baseFixturesPath . $fixture);
            }

            $this->applyEndFixtures = [];
        }
    }

    /**
     * @Given /^I key press "(\d+)" on "([^"]*)"$/
     */
    public function iKeyPressOn($keyCode, $element)
    {
        $$element = $this->getSession()->getPage()->find('css', $element);
        $$element->focus();
        $this->getSession()->getDriver()->keyPress($$element->getXPath(), $keyCode);
    }

    /**
     * @Given /^I wait for "([^"]*)" is visible "(true|false)"$/
     */
    public function iWaitForIsVisible($element, $isVisible = true)
    {
        $timeOut = 10;
        $i = 0;

        while ($i < $timeOut) {
            try {
                $isVisible 
                    ? $this->assertElementOnPage($element)
                    : $this->assertElementNotOnPage($element);

                return;
            } catch (Exception $e) {
                ++$i;

                sleep(1);
            }
        }

        if ($isVisible) {
            throw new Exception("The element '$element' was not found after a $timeOut seconds timeout");
        }

        throw new Exception("The element '$element' was not hidden after a $timeOut seconds timeout");
    }

    /**
     * @Given /^I fill location question with name "([^"]*)" and location "([^"]*)"$/
     */
    public function iFillLocationQuestionWithNameAndLocation($fieldName, $location)
    {
        $field = $this->getSession()->getPage()->findField($fieldName);

        if (null === $field) {
            throw new Exception('Location question with name '  . $fieldName . ' not found');
        }

        $element = $this->getSession()->
                getDriver()->getWebDriverSession()->element('xpath', $field->getXpath());
 
        // enter the location
        $existingValueLength = strlen($element->attribute('value'));
        $value = str_repeat(Key::BACKSPACE.Key::DELETE, $existingValueLength) . $location;
        $element->postValue(['value' => [$value]]);

        // wait for arriving the autocomplite menu
        $this->iWaitForIsVisible('.googlelocation_autocomplite_menu');

        // click a first location
        $locations = $this->getSession()->getPage()->findAll('css', '.ui-menu-item>a');
        current($locations)->click();
    }

    /**
     * @Given /^I select radio question with name "([^"]*)" and value "([^"]*)"$/
     * @Given /^I select checkbox question with name "([^"]*)" and value "([^"]*)"$/
     */
    public function iSelectQuestionWithNameAndValue($fieldName, $value)
    {
        $values = is_array($value) ? $value : [$value];

        foreach($values as $questionValue) {
            // search for the question by a label
            $fieldLabel = $this->getSession()->getPage()->
                    find('xpath', "//div[not(contains(@style,'display:none'))]//label[.='{$fieldName}']//ancestor::tr[1]/descendant::label[text()='{$questionValue}']");

            if (null === $fieldLabel) {
                throw new Exception('The question label with name "'  . $fieldName . '" and value "' . $questionValue . '" not found');
            }

            $element = $this->getSession()->
                    getPage()->find('css', "#{$fieldLabel->getAttribute('for')}");

            $element->click();
        }
    }

    /**
     * @Given /^I select date question with name "([^"]*)" and month "(\d+)" and day "(\d+)" and year "(\d+)"$/
     */
    public function iSelectDateQuestionWithNameAndMonthAndDayAndYear($fieldName, $month, $day, $year)
    {
        // search for the question by a label
        $dateFieldLabel = $this->getSession()->
                getPage()->find('xpath', "//div[not(contains(@style,'display:none'))]//label[.='{$fieldName}']");

        if (null === $dateFieldLabel) {
            throw new Exception('Date question with name '  . $fieldName . ' not found');
        }

        // search for the hidden date field
        $hiddenDateField = $this->getSession()->getPage()->
                find('css', 'input[id="' . $dateFieldLabel->getAttribute('for') .'"]', false);

        $hiddenQuestionName = $hiddenDateField->getAttribute('name');

        // enter the date
        $this->getSession()->getPage()->
                find('css', 'select[name="' . 'month_' . $hiddenQuestionName .'"]')->selectOption($month);

        $this->getSession()->getPage()->
                find('css', 'select[name="' . 'day_' . $hiddenQuestionName .'"]')->selectOption($day);

        $this->getSession()->getPage()->
                find('css', 'select[name="' . 'year_' . $hiddenQuestionName .'"]')->selectOption($year);
    }

    /**
     * @Given /^Created user with id "([^"]*)" and username "([^"]*)" and password "([^"]*)" and email "([^"]*)"$/
     * @Given /^Created user with id "([^"]*)" and username "([^"]*)" and password "([^"]*)" and email "([^"]*)" and sex="([^"]*)"$/
     */
    public function createdUserWithIdAndUsernameAndPasswordAndEmail($id, $username, $password, $email, $sex = '')
    {
        $sexOptions =  !$sex
            ? $this->params['account_types'][$this->params['default_profile']['sex']] // use the default account
            : $this->params['account_types'][$sex];

        // create a user
        $this->executeSqlFile($this->baseFixturesPath . 'core/user.sql', [
            '__id__' => $id,
            '__email__' => $email,
            '__username__' => $username,
            '__account_type__' => $sexOptions['hash'],
            '__password__' => hash('sha256', $this->skadateConfig['OW_PASSWORD_SALT'] . $password),
            '__email_verified__' => 1
        ]);

        // create the user's questions data
        $this->executeSqlFile($this->baseFixturesPath . 'core/question_data.sql', [
            '__id__' => $id,
            '__sex__' => $sexOptions['sex'],
            '__match_sex__' => $this->params['default_profile']['match_sex'],
            '__match_age__' => $this->params['default_profile']['match_age'],
            '__birthdate__' => $this->params['default_profile']['birthdate'],
            '__realname__' => $username,
            '__aboutme__' => $this->params['default_profile']['about'],
        ]);
    }

    /**
     * @Given /^I am a logged user with id "(\d+)" and username "([^"]*)"$/
     * @Given /^I am a logged user with id "([^"]*)" and username "([^"]*)" and sex="([^"]*)"$/
     */
    public function iAmALoggedUserWithIdAndUsername($id, $username, $sex = '')
    {
        $sexOptions =  !$sex
            ? $this->params['account_types'][$this->params['default_profile']['sex']] // use the default account
            : $this->params['account_types'][$sex];

        // create a user
        $this->executeSqlFile($this->baseFixturesPath . 'core/user.sql', [
            '__id__' => $id,
            '__email__' => $this->params['default_profile']['email'],
            '__username__' => $username,
            '__account_type__' => $sexOptions['hash'],
            '__password__' => hash('sha256', $this->skadateConfig['OW_PASSWORD_SALT'] . 'tester'),
            '__email_verified__' => 1
        ]);

        // create the user's questions data
        $this->executeSqlFile($this->baseFixturesPath . 'core/question_data.sql', [
            '__id__' => $id,
            '__sex__' => $sexOptions['sex'],
            '__match_sex__' => $this->params['default_profile']['match_sex'],
            '__match_age__' => $this->params['default_profile']['match_age'],
            '__birthdate__' => $this->params['default_profile']['birthdate'],
            '__realname__' => $this->params['default_profile']['realname'],
            '__aboutme__' => $this->params['default_profile']['about'],
        ]);

        $loginHash =  md5(time());

        // login user
        $this->executeSqlFile($this->baseFixturesPath . 'core/login_cookie.sql', [
            '__id__' => $id,
            '__hash__' => $loginHash
        ]);

        // create a login cookie
        $this->getSession()->visit($this->locatePath('/'));
        $this->getSession()->setCookie('ow_login', $loginHash);
        $this->getSession()->reload();

        $this->getSession()->wait(10000, '(0 === jQuery.active && 0 === jQuery(\':animated\').length)');
    }

    /**
     * @Given /^Loaded sql data from "([^"]*)"$/
     * @Given /^Loaded sql data from "([^"]*)" with keys "([^"]*)" and params "([^"]*)"$/
     */
    public function loadedSqlDataFrom($fixture, $keys = [], $params = [])
    {
        $combinedParams = array_combine($keys, $params);

        $this->executeSqlFile($this->baseFixturesPath . $fixture . '.sql', $combinedParams);
    }

    /**
     * @Transform /^(true|false)$/
     */
    public function castStringToBoolean($booleanValue)
    {
        return $booleanValue === 'true';
    }

    /**
     * @Transform /^(\d+)$/
     */
    public function castStringToNumber($numberValue)
    {
        return intval($numberValue);
    }

    /**
     * @Transform /^\[(.*)\]$/
     */
    public function castStringToArray($string)
    {
        return explode(',', $string);
    }
}
