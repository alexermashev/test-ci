<?php

class AVATARDIMENSIONS_CTRL_Admin extends ADMIN_CTRL_Abstract
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index( $params = array() )
    {
        $pluginKey = AVATARDIMENSIONS_BOL_Service::PLUGIN_KEY;
        $language = OW::getLanguage();
        $config = OW::getConfig();

        $this->setPageHeading($language->text($pluginKey, 'admin_heading_settings'));
        $this->setPageHeadingIconClass('ow_ic_gear_wheel');

        $settingsForm = new Form('settingsForm');
        $settingsForm->setId('settingsForm');

        $formInput = new TextField('custom_avatar_size');
        $formInput->setRequired();
        $formInput->setLabel($language->text($pluginKey, 'label_custom_avatar_size'));
        $formInputValue = (int) $config->getValue($pluginKey, 'custom_avatar_size');
        $formInput->setValue($formInputValue);
        $settingsForm->addElement($formInput);

        $submit = new Submit('save');
        $submit->addAttribute('class', 'ow_ic_save');
        $submit->setValue($language->text($pluginKey, 'label_save_btn_label'));

        $settingsForm->addElement($submit);

        $this->addForm($settingsForm);

        if ( OW::getRequest()->isPost() )
        {
            if ( $settingsForm->isValid($_POST) )
            {
                $data = $settingsForm->getValues();

                $config->saveConfig($pluginKey, 'custom_avatar_size', $data['custom_avatar_size']);

                OW::getFeedback()->info($language->text($pluginKey, 'settings_saved'));
                $this->redirect();
            }
        }
    }
}
