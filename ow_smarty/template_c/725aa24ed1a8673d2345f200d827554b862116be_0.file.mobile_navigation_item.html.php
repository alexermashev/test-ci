<?php
/* Smarty version 3.1.30, created on 2018-07-21 15:11:17
  from "/var/www/biyebiye/public_html/ow_system_plugins/admin/views/components/mobile_navigation_item.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b53855551e015_28377033',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '725aa24ed1a8673d2345f200d827554b862116be' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_system_plugins/admin/views/components/mobile_navigation_item.html',
      1 => 1479204252,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b53855551e015_28377033 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="clearfix component <?php if (isset($_smarty_tpl->tpl_vars['item']->value['custom']) && $_smarty_tpl->tpl_vars['item']->value['custom']) {?>dnd-item-custom<?php }?>" data-key="<?php echo $_smarty_tpl->tpl_vars['item']->value['key'];?>
" <?php if (isset($_smarty_tpl->tpl_vars['item']->value['type'])) {?>data-type=<?php echo $_smarty_tpl->tpl_vars['item']->value['type'];
}?> <?php if (isset($_smarty_tpl->tpl_vars['item']->value['custom'])) {?>data-custom=<?php echo $_smarty_tpl->tpl_vars['item']->value['custom'];
}?> <?php if (isset($_smarty_tpl->tpl_vars['item']->value['data'])) {?>data-data="<?php echo $_smarty_tpl->tpl_vars['item']->value['data'];?>
"<?php }?>>
    <div class="schem_component dd_handle">
        <div class="ow_dnd_schem_item schem_component">
            <span class="ow_label dnd-title">
                <?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>

            </span>
            <span class="action dnd-actions">
                <a class="ow_ic_gear_wheel dnd-control dd_edit" href="javascript://;" data-action="edit">&nbsp;</a>
                <a class="ow_ic_delete close dnd-control dd_delete" href="javascript://;" data-action="delete">&nbsp;</a>
            </span>
        </div>
    </div>
</div><?php }
}
