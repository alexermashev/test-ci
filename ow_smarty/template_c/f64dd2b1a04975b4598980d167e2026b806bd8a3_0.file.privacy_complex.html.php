<?php
/* Smarty version 3.1.30, created on 2018-07-19 06:54:08
  from "/var/www/biyebiye/public_html/ow_plugins/protectedphotos/views/components/privacy_complex.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b506dd06f1383_63040535',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f64dd2b1a04975b4598980d167e2026b806bd8a3' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_plugins/protectedphotos/views/components/privacy_complex.html',
      1 => 1479204282,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b506dd06f1383_63040535 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
?>
<div class="ow_photo_privacy_settings ow_smallmargin clearfix">
    <?php echo $_smarty_tpl->tpl_vars['privacyMarkup']->value;?>

</div>
<div id="protectedphotos-content">
    <div class="ow_pass_protected_privacy_userlist ow_privacy_userlist ow_smallmargin clearfix">
        <div class="ow_privacy_context" style="display: none;">
            <div class="ow_pass_protected_userlist_wrap ow_privacy_userlist_main_wrap ow_left ow_bg_color">
                <?php echo $_smarty_tpl->tpl_vars['searchMarkup']->value;?>

                <div class="ow_privacy_userlist_main_label ow_small"><?php echo smarty_function_text(array('key'=>'protectedphotos+or_choose_from_list'),$_smarty_tpl);?>
</div>
                <?php echo $_smarty_tpl->tpl_vars['friendListMarkup']->value;?>

            </div>
            <?php echo $_smarty_tpl->tpl_vars['selectedListMarkup']->value;?>

        </div>
    </div>
    <?php echo $_smarty_tpl->tpl_vars['passwordMarkup']->value;?>

</div>
<?php }
}
