<?php
/* Smarty version 3.1.30, created on 2018-07-19 06:55:33
  from "/var/www/biyebiye/public_html/ow_system_plugins/base/views/components/drag_and_drop_item_customize.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b506e25c32b05_07279766',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd81184e773878ce9d3c2ea918e6314248995ef44' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_system_plugins/base/views/components/drag_and_drop_item_customize.html',
      1 => 1479204252,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b506e25c32b05_07279766 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_block_block_decorator')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.block_decorator.php';
?>
<div <?php if (!empty($_smarty_tpl->tpl_vars['box']->value['avaliable_sections'])) {?>ow_avaliable_sections="<?php echo $_smarty_tpl->tpl_vars['box']->value['avaliable_sections'];?>
"<?php }?> class="<?php if ($_smarty_tpl->tpl_vars['box']->value['clone']) {?>clone<?php }
if ($_smarty_tpl->tpl_vars['box']->value['freeze']) {?> ow_dnd_freezed<?php }?> access_<?php echo $_smarty_tpl->tpl_vars['box']->value['access'];?>
 clearfix component" id="<?php echo $_smarty_tpl->tpl_vars['box']->value['uniqName'];?>
">
    <div class="schem_component dd_handle" <?php if ($_smarty_tpl->tpl_vars['render']->value) {?> style="display: none" <?php }?>>
        <div class="ow_dnd_schem_item schem_component <?php echo $_smarty_tpl->tpl_vars['box']->value['icon'];?>
" >
            <span class="ow_label dd_title">
                <?php echo $_smarty_tpl->tpl_vars['box']->value['title'];?>

            </span>
            <span class="action" style="display: none">
                
                <a class="ow_ic_gear_wheel dd_edit" href="javascript://;" title="<?php echo smarty_function_text(array('key'=>"base+widgets_action_edit"),$_smarty_tpl);?>
">&nbsp;</a>
                <a class="ow_ic_delete close dd_delete" href="javascript://;" title="<?php echo smarty_function_text(array('key'=>"base+widgets_action_delete"),$_smarty_tpl);?>
">&nbsp;</a>
                
            </span>
        </div>
    </div>

    <?php if ($_smarty_tpl->tpl_vars['render']->value) {?>
        <div class="view_component ow_dnd_widget ow_dnd_widget_customize <?php echo $_smarty_tpl->tpl_vars['box']->value['uniqName'];?>
">
            <?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, "boxCap", null, null);
?>

                <?php if (!$_smarty_tpl->tpl_vars['box']->value['freeze']) {?>
                    <div class="ow_box_icons actions">
                        <a href="javascript://;" class="ow_ic_gear_wheel control dd_edit">&nbsp;</a>
                        <a href="javascript://;" class="ow_ic_delete control dd_delete">&nbsp;</a>
                    </div>
                <?php }?>
            <?php $_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);
?>


            <?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('block_decorator', array('name'=>'box','iconClass'=>$_smarty_tpl->tpl_vars['box']->value['icon'],'label'=>$_smarty_tpl->tpl_vars['box']->value['title'],'capAddClass'=>"ow_dnd_configurable_component dd_handle clearfix",'capContent'=>$_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'boxCap'),'type'=>$_smarty_tpl->tpl_vars['box']->value['type'],'addClass'=>"ow_stdmargin clearfix",'toolbar'=>$_smarty_tpl->tpl_vars['box']->value['toolbar']));
$_block_repeat1=true;
echo smarty_block_block_decorator(array('name'=>'box','iconClass'=>$_smarty_tpl->tpl_vars['box']->value['icon'],'label'=>$_smarty_tpl->tpl_vars['box']->value['title'],'capAddClass'=>"ow_dnd_configurable_component dd_handle clearfix",'capContent'=>$_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'boxCap'),'type'=>$_smarty_tpl->tpl_vars['box']->value['type'],'addClass'=>"ow_stdmargin clearfix",'toolbar'=>$_smarty_tpl->tpl_vars['box']->value['toolbar']), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>


                <?php echo $_smarty_tpl->tpl_vars['content']->value;?>


            <?php $_block_repeat1=false;
echo smarty_block_block_decorator(array('name'=>'box','iconClass'=>$_smarty_tpl->tpl_vars['box']->value['icon'],'label'=>$_smarty_tpl->tpl_vars['box']->value['title'],'capAddClass'=>"ow_dnd_configurable_component dd_handle clearfix",'capContent'=>$_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'boxCap'),'type'=>$_smarty_tpl->tpl_vars['box']->value['type'],'addClass'=>"ow_stdmargin clearfix",'toolbar'=>$_smarty_tpl->tpl_vars['box']->value['toolbar']), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

        </div>
    <?php }?>

</div><?php }
}
