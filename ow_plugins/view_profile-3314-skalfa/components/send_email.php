<?php

class VIEWPROFILE_CMP_SendEmail extends OW_Component
{
    protected $userId;
    protected $backUrl;
    protected $whoseId;

    public function __construct($userId, $whoseId, $backUrl = '')
    {
        parent::__construct();

        $this->userId = $userId;
        $this->backUrl = $backUrl;
        $this->whoseId = $whoseId;
    }

    public function onBeforeRender()
    {
        parent::onBeforeRender();

        $language = OW::getLanguage();

        $sendingForm = new Form('sendingForm');
        $sendingForm->setId('sendingForm');
        $sendingForm->setAction(OW::getRouter()->urlForRoute(VIEWPROFILE_BOL_Service::PLUGIN_KEY . '_main_index'));

        $email = new TextField('sendEmail');
        $email->setRequired();
        $email->setLabel($language->text(VIEWPROFILE_BOL_Service::PLUGIN_KEY, 'label_send_email'));
        $email->addValidator(new EmailValidator());
        $sendingForm->addElement($email);

        $back = new HiddenField('back_uri');
        $back->setValue($this->backUrl);
        $sendingForm->addElement($back);

        $whose = new HiddenField('whoseId');
        $whose->setValue($this->whoseId);
        $sendingForm->addElement($whose);

        $submit = new Submit('save');
        $submit->addAttribute('class', 'ow_ic_save');
        $submit->setValue($language->text(VIEWPROFILE_BOL_Service::PLUGIN_KEY, 'app_confirm'));
        $sendingForm->addElement($submit);

        $this->addForm($sendingForm);
    }
}
