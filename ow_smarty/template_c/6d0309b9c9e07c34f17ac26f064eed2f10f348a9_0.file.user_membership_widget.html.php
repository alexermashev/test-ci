<?php
/* Smarty version 3.1.30, created on 2019-01-22 05:17:53
  from "/Users/esase/Sites/8418/ow_plugins/membership/views/components/user_membership_widget.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c46edd1e15fd2_04456239',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6d0309b9c9e07c34f17ac26f064eed2f10f348a9' => 
    array (
      0 => '/Users/esase/Sites/8418/ow_plugins/membership/views/components/user_membership_widget.html',
      1 => 1547792046,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c46edd1e15fd2_04456239 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.text.php';
?>
<table class="ow_table_3 ow_automargin ow_smallmargin">
    <tr class="<?php if ($_smarty_tpl->tpl_vars['isModerator']->value && !$_smarty_tpl->tpl_vars['isOwner']->value) {?>ow_tr_first<?php } else { ?>ow_tr_last ow_tr_first<?php }?> ">
        <td class="ow_label"><?php echo smarty_function_text(array('key'=>'membership+membership'),$_smarty_tpl);?>
</td>
        <td class="ow_value"><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</td>
    </tr>
    <?php if ($_smarty_tpl->tpl_vars['isModerator']->value && !$_smarty_tpl->tpl_vars['isOwner']->value) {?>
    <tr class="ow_tr_last">
        <td class="ow_label"><?php echo smarty_function_text(array('key'=>'membership+expires'),$_smarty_tpl);?>
</td>
        <td class="ow_value"><?php echo MEMBERSHIP_BOL_MembershipService::formatDate(array('timestamp'=>$_smarty_tpl->tpl_vars['membership']->value->expirationStamp),$_smarty_tpl);?>
</td>
    </tr>
    <?php }?>
</table><?php }
}
