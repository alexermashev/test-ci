<?php
/* Smarty version 3.1.30, created on 2018-07-23 06:17:03
  from "/var/www/biyebiye/public_html/ow_plugins/matchmaking/views/components/notification_html.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b55ab1fd4e803_23184003',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7a2c2743c1b5a061576a039c52cb11f08aff91aa' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_plugins/matchmaking/views/components/notification_html.html',
      1 => 1531989433,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b55ab1fd4e803_23184003 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
?>
<body style="font-size:13px;font-family:Lucida Grande,Verdana;">
<p>
    <?php echo smarty_function_text(array('key'=>"matchmaking+email_html_head",'userName'=>$_smarty_tpl->tpl_vars['userName']->value),$_smarty_tpl);?>

</p>
<p>
    <?php echo smarty_function_text(array('key'=>"matchmaking+email_html_description"),$_smarty_tpl);?>

</p>

<table cellpadding="0" cellspacing="0">
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['list']->value, 'user', false, NULL, 'userList', array (
  'first' => true,
  'iteration' => true,
  'last' => true,
  'index' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['user']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['index']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['first'] = !$_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['index'];
$_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['iteration'] == $_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['total'];
?>
    <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['first']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['first'] : null)) {?><tr><?php }?>
    <td valign="top" style="padding: 6px;">
        <table width="286" height="162"  cellpadding="0" cellspacing="0" style="background: #f3f4f7; border: 1px solid #e0e2e8;">
            <tr>
                <td valign="top" width="140" height="140" style="padding: 11px 6px 11px 11px;">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['user']->value['userUrl'];?>
" target="_blank" style="display: inline-block; vertical-align: top;"><img src="<?php echo $_smarty_tpl->tpl_vars['user']->value['avatarUrl'];?>
" alt="" title="test" style="width: 140px; vertical-align: top;"></a>
                </td>
                <td valign="top" style="padding: 11px 11px 11px 5px;">
                    <div style="min-height: 123px;">
                        <a style="margin-bottom: 6px; display: block;" href="<?php echo $_smarty_tpl->tpl_vars['user']->value['userUrl'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['user']->value['displayName'];?>
</a>
                        <div style="margin-bottom: 6px;"><?php echo $_smarty_tpl->tpl_vars['user']->value['sex'];?>
 <?php echo $_smarty_tpl->tpl_vars['user']->value['age'];?>
</div>
                        <div style="margin-bottom: 6px;"><?php echo $_smarty_tpl->tpl_vars['user']->value['googlemap_location'];?>
</div>
                    </div>
                    <div style="color: #31b231; font-size: 11px;"><?php echo smarty_function_text(array('key'=>"matchmaking+compatibility"),$_smarty_tpl);?>
: <?php echo $_smarty_tpl->tpl_vars['user']->value['compatibility'];?>
%</div>
                </td>
            </tr>
        </table>
    </td>
    <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['iteration'] : null)%2 == 0 && !(isset($_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['last'] : null)) {?>
</tr><tr>
    <?php }?>
    <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['last'] : null)) {?>
</tr>
    <?php }?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</table>

<p>
    <a href="<?php echo $_smarty_tpl->tpl_vars['matchesUrl']->value;?>
"><?php echo smarty_function_text(array('key'=>"base+view_more_label"),$_smarty_tpl);?>
</a>
</p>

<p>
    <?php echo smarty_function_text(array('key'=>"matchmaking+email_html_bottom"),$_smarty_tpl);?>

</p>
<a href="<?php echo $_smarty_tpl->tpl_vars['unsubscribeUrl']->value;?>
"><?php echo smarty_function_text(array('key'=>'base+massmailing_unsubscribe'),$_smarty_tpl);?>
</a>
</body>
<?php }
}
