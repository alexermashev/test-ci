<?php

class VIEWPROFILE_CTRL_Main extends OW_ActionController
{
    /**
     * @var VIEWPROFILE_BOL_Service
     */
    private $service;

    public function __construct()
    {
        $this->service = VIEWPROFILE_BOL_Service::getInstance();
    }

    public function index( $params = array() )
    {
        if ( OW::getRequest()->isPost() )
        {
            if ( isset($_POST['form_name']) && $_POST['form_name'] == 'sendingForm' )
            {
                $language = OW::getLanguage();
                $userId = OW::getUser()->getId();

                $status = BOL_AuthorizationService::getInstance()->getActionStatus('viewprofile', 'send_profile');

                if ( $status['status'] != BOL_AuthorizationService::STATUS_AVAILABLE )
                {
                    OW::getFeedback()->error($status['msg']);

                    if ( isset($_POST['back_uri']) && !empty($_POST['back_uri']) )
                    {
                        $this->redirect(urldecode($_POST['back_uri']));
                    }

                    $this->redirect('/');
                }

                $spentCredit = BOL_AuthorizationService::getInstance()->trackAction('viewprofile', 'send_profile');

                if ( $spentCredit['status'] )
                {
                    OW::getFeedback()->info($spentCredit['msg']);
                }

                $hash = $this->service->generateHash();

                $sendData = new VIEWPROFILE_BOL_SendProfile();
                $sendData->userId = $userId;
                $sendData->whoseId = $_POST['whoseId'];
                $sendData->email = $_POST['sendEmail'];
                $sendData->status = 0;
                $sendData->hash = $hash;
                $sendData->createdDatetime = time();

                $sendData = $this->service->sendProfile($sendData);

                $this->service->sendToEmail($sendData);

                OW::getFeedback()->info($language->text(VIEWPROFILE_BOL_Service::PLUGIN_KEY, 'notification_send_profile'));
            }

            if ( isset($_POST['back_uri']) && !empty($_POST['back_uri']) )
            {
                $this->redirect(urldecode($_POST['back_uri']));
            }

            $this->redirect('/');
        }
    }

    public function sentProfiles( $params )
    {
        $language = OW::getLanguage();
        $userId = OW::getUser()->getId();
        $page = ( empty($_GET['page']) || (int) $_GET['page'] < 0 ) ? 1 : (int) $_GET['page'];

        $this->setPageHeading($language->text(VIEWPROFILE_BOL_Service::PLUGIN_KEY, 'sent_profiles_page_heading'));
        $this->setPageHeadingIconClass('ow_ic_calendar');
        OW::getDocument()->setDescription($language->text(VIEWPROFILE_BOL_Service::PLUGIN_KEY, 'sent_profiles_page_desc'));

        if ( !OW::getUser()->isAdmin() && !OW::getUser()->isAuthorized('viewprofile', 'send_profile') )
        {
            $status = BOL_AuthorizationService::getInstance()->getActionStatus('viewprofile', 'send_profile');
            throw new AuthorizationException($status['msg']);

            return;
        }

        $data = $this->service->findMySentProfiles($userId, $page);
        $dataCount = $this->service->findMySentProfilesCount($userId);

        $this->addComponent('paging', new BASE_CMP_Paging($page, ceil($dataCount / VIEWPROFILE_BOL_Service::CONF_SENT_PROFILES_COUNT_ON_PAGE), 5));

        if ( empty($data) )
        {
            $this->assign('no_data', true);
        }

        $listingData = $this->service->getListingData($data);

        $newListingData = [];

        $shortContents = [];
        $fullContents = [];

        foreach ( $listingData as $key => $item )
        {
            $item['fullContent'] = $item['content'];
            $btn = ' <span class="ow_lbutton more" data-id="' . $key . '">' . $language->text(VIEWPROFILE_BOL_Service::PLUGIN_KEY, 'show_more') . '</span>';

            if ( mb_strlen(strip_tags($item['content']), 'UTF-8') >= 100 )
            {
                $item['content'] = UTIL_String::truncate(strip_tags($item['content']), 100, '...');
                $shortContents[$key] = $item['content'];
                $item['content'] .= $btn;
            }
            else
            {
                $shortContents[$key] = $item['content'];
            }

            $fullContents[$key] = $item['fullContent'];

            $newListingData[$key] = $item;
        }

        OW::getDocument()->addScriptDeclaration(
            UTIL_JsGenerator::composeJsString(';
                var lessLabel = {$less};
                var moreLabel = {$more};
                
                $(document).delegate(".ow_lbutton.more", "click", function() {
                    var dataId = $(this).attr("data-id");
                    var btn = \' <span class=\"ow_lbutton less\" data-id=\"\' + dataId + \'\">\' + lessLabel + \'</span>\';
                    var parent = $(this).parent(".ow_ipc_content");
                    
                    $(parent).text({$fullContents}[dataId]);
                    $(parent).append(btn);
                });
                
                $(document).delegate(".ow_lbutton.less", "click", function() {
                    var dataId = $(this).attr("data-id");
                    var btn = \' <span class=\"ow_lbutton more\" data-id=\"\' + dataId + \'\">\' + moreLabel + \'</span>\';
                    var parent = $(this).parent(".ow_ipc_content");
                    
                    $(parent).text({$shortContents}[dataId]);
                    $(parent).append(btn);
                });
                    ', array(
                    'shortContents' => $shortContents,
                    'fullContents' => $fullContents,
                    'more' => $language->text(VIEWPROFILE_BOL_Service::PLUGIN_KEY, 'show_more'),
                    'less' => $language->text(VIEWPROFILE_BOL_Service::PLUGIN_KEY, 'show_less')
                )
            )
        );

        $this->assign('page', $page);
        $this->assign('data', $newListingData);
    }

    public function opinion( $params )
    {
        $language = OW::getLanguage();

        $this->setPageHeading($language->text(VIEWPROFILE_BOL_Service::PLUGIN_KEY, 'opinion_page_heading'));
        $this->setPageHeadingIconClass('ow_ic_calendar');
        OW::getDocument()->setDescription($language->text(VIEWPROFILE_BOL_Service::PLUGIN_KEY, 'opinion_page_heading_desc'));

        if ( OW::getRequest()->isPost() && isset($_POST['form_name']) && $_POST['form_name'] == 'opinionForm' )
        {
            $this->service->saveOpinion($_POST);
        }

        if ( empty($_GET['status']) || empty($_GET['hash']) )
        {
            $this->redirect('/');
        }

        $sentProfile = $this->service->findSendProfileByHash($_GET['hash']);

        if ( ! $sentProfile )
        {
            $this->redirect('/');
        }

        $opinionForm = new Form('opinionForm');
        $opinionForm->setId('opinionForm');
        $opinionForm->setMethod(FORM::METHOD_POST);

        $email = new Textarea('opinion');
        $email->setRequired();
        $email->setLabel($language->text(VIEWPROFILE_BOL_Service::PLUGIN_KEY, 'label_send_opinion'));
        $opinionForm->addElement($email);

        $sentId = new HiddenField('hash');
        $sentId->setValue($sentProfile->hash);
        $opinionForm->addElement($sentId);

        $status = new HiddenField('status');
        $status->setValue((int) $_GET['status']);
        $opinionForm->addElement($status);

        $submit = new Submit('save');
        $submit->addAttribute('class', 'ow_ic_save');
        $submit->setValue($language->text(VIEWPROFILE_BOL_Service::PLUGIN_KEY, 'send_opinion'));
        $opinionForm->addElement($submit);

        $this->addForm($opinionForm);

        $this->assign('headerText', $language->text(VIEWPROFILE_BOL_Service::PLUGIN_KEY, 'label_accepted_answer'));
    }
}