<?php
/* Smarty version 3.1.30, created on 2018-07-19 06:53:01
  from "/var/www/biyebiye/public_html/ow_system_plugins/base/views/components/site_statistic.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b506d8d0839c7_01868711',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3de19a49a85f0429d624b04ea02aaaf72ec8a5ef' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_system_plugins/base/views/components/site_statistic.html',
      1 => 1479204252,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b506d8d0839c7_01868711 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_style')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.style.php';
if (!is_callable('smarty_block_script')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.script.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('style', array());
$_block_repeat1=true;
echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

    .statistic_amount {
        margin-top:10px;
    }

    .statistic_amount h3 {
        margin-bottom: 10px;
    }
<?php $_block_repeat1=false;
echo smarty_block_style(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>


<?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('script', array());
$_block_repeat1=true;
echo smarty_block_script(array(), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

    var ctx = $("#<?php echo $_smarty_tpl->tpl_vars['chartId']->value;?>
").get(0).getContext("2d");
    ctx.canvas.height = 100;

    var data = {
        labels: <?php echo $_smarty_tpl->tpl_vars['categories']->value;?>
,
        datasets: <?php echo $_smarty_tpl->tpl_vars['data']->value;?>

    };

    
        var lineChart = new Chart(ctx).Line(data, {
            animation: false,
            responsive : true,
            tooltipTemplate: "<?php echo '<%'; ?>
= datasetLabel <?php echo '%>'; ?>
 - <?php echo '<%'; ?>
= value <?php echo '%>'; ?>
",
            multiTooltipTemplate: "<?php echo '<%'; ?>
= datasetLabel <?php echo '%>'; ?>
 - <?php echo '<%'; ?>
= value <?php echo '%>'; ?>
"
        });
    

    if ( typeof OW.WidgetPanel != "undefined" )
    {
        // Rebuild the chart
        OW.WidgetPanel.bind("move", function(e)
        {
            var canvasId = $(e.widget).find("canvas").attr("id");

            if (canvasId == "<?php echo $_smarty_tpl->tpl_vars['chartId']->value;?>
")
            {
                lineChart.destroy();
                lineChart = new Chart(ctx).Line(data, {
                    animation: false,
                    responsive : true,
                    tooltipTemplate: "<?php echo '<%';?>= datasetLabel <?php echo '%>';?> - <?php echo '<%';?>= value <?php echo '%>';?>",
                    multiTooltipTemplate: "<?php echo '<%';?>= datasetLabel <?php echo '%>';?> - <?php echo '<%';?>= value <?php echo '%>';?>"
                });
            }
        });
    }
<?php $_block_repeat1=false;
echo smarty_block_script(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>


<div class="statistic_chart_wrapper">
    <canvas id="<?php echo $_smarty_tpl->tpl_vars['chartId']->value;?>
"></canvas>
</div>
<div class="statistic_amount">
    <h3><?php echo smarty_function_text(array('key'=>'admin+statistics_amount_for_period'),$_smarty_tpl);?>
 :</h3>
    <ul>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['total']->value, 'info');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['info']->value) {
?>
        <li>
            <?php echo $_smarty_tpl->tpl_vars['info']->value['label'];?>
: <b><?php echo $_smarty_tpl->tpl_vars['info']->value['count'];?>
</b>
        </li>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

    </ul>
</div>
<?php }
}
