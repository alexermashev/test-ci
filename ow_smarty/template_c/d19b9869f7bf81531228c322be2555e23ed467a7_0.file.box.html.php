<?php
/* Smarty version 3.1.30, created on 2018-07-19 04:37:29
  from "/var/www/biyebiye/public_html/ow_system_plugins/base/mobile/decorators/box.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b504dc9d2d254_22085795',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd19b9869f7bf81531228c322be2555e23ed467a7' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_system_plugins/base/mobile/decorators/box.html',
      1 => 1479204252,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b504dc9d2d254_22085795 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_decorator')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.decorator.php';
?>
<div class="owm_box<?php echo $_smarty_tpl->tpl_vars['data']->value['addClass'];?>
">
    <?php if ($_smarty_tpl->tpl_vars['data']->value['capEnabled']) {?>
        <div class="owm_box_cap<?php echo $_smarty_tpl->tpl_vars['data']->value['capAddClass'];?>
 clearfix">
            <?php if (!empty($_smarty_tpl->tpl_vars['data']->value['label'])) {?><h3 class="<?php echo $_smarty_tpl->tpl_vars['data']->value['iconClass'];?>
"><?php echo $_smarty_tpl->tpl_vars['data']->value['label'];?>
</h3><?php }
echo $_smarty_tpl->tpl_vars['data']->value['capContent'];?>

        </div>
    <?php }?>
    <div class="owm_box_body">
        <div class="owm_box_body_cont clearfix">
            <div class="owm_box_padding">
                <?php echo $_smarty_tpl->tpl_vars['data']->value['content'];?>

            </div>
        </div>
    </div>
    <?php if (!empty($_smarty_tpl->tpl_vars['data']->value['toolbar'])) {?>
        <div class="owm_box_bottom">
            <?php echo smarty_function_decorator(array('name'=>'box_toolbar','itemList'=>$_smarty_tpl->tpl_vars['data']->value['toolbar']),$_smarty_tpl);?>

        </div>
    <?php }?>
</div><?php }
}
