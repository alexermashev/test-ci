<?php
/* Smarty version 3.1.30, created on 2018-07-19 06:32:27
  from "/var/www/biyebiye/public_html/ow_system_plugins/base/views/components/sort_control.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5068bb3a6709_86919829',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7ecc91233fade793d23f189d825ff6c6ef73f513' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_system_plugins/base/views/components/sort_control.html',
      1 => 1479204252,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b5068bb3a6709_86919829 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
?>

<div class="ow_sort_control ow_smallmargin ow_small ow_alt2"><span class="ow_sort_control_label"><?php echo smarty_function_text(array('key'=>"base+sort_control_sortby"),$_smarty_tpl);?>
:</span><?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['itemList']->value, 'item');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
?><a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['url'];?>
" <?php if ($_smarty_tpl->tpl_vars['item']->value['isActive']) {?>class="active"<?php }?>><span><?php echo $_smarty_tpl->tpl_vars['item']->value['label'];?>
</span></a><?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</div><?php }
}
