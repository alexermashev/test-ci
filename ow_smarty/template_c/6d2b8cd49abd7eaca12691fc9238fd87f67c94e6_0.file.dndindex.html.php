<?php
/* Smarty version 3.1.30, created on 2018-07-20 02:05:30
  from "/var/www/biyebiye/public_html/ow_themes/trend/master_pages/dndindex.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b517baa7b07b9_48305660',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6d2b8cd49abd7eaca12691fc9238fd87f67c94e6' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_themes/trend/master_pages/dndindex.html',
      1 => 1479204282,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b517baa7b07b9_48305660 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_component')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.component.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_decorator')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.decorator.php';
?>
<div class="ow_page_wrap">
	<div class="ow_page_padding">
		<div class="ow_site_panel">
				<?php echo smarty_function_component(array('class'=>'BASE_CMP_Console'),$_smarty_tpl);?>

		</div>
		<div class="ow_header">
		</div>
		<div class="ow_menu_wrap"><?php echo $_smarty_tpl->tpl_vars['main_menu']->value;?>
</div>
		<div class="ow_page_container">
		
			<div class="ow_canvas">
				<div class="ow_page ow_bg_color clearfix">
					<?php if ($_smarty_tpl->tpl_vars['heading']->value) {?><h1 class="ow_stdmargin <?php echo $_smarty_tpl->tpl_vars['heading_icon_class']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['heading']->value;?>
</h1><?php }?>
					<div class="ow_dndindex clearfix">
						<?php echo $_smarty_tpl->tpl_vars['content']->value;?>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ow_footer">
	<div class="ow_canvas">
		<div class="ow_page clearfix">
			<?php echo $_smarty_tpl->tpl_vars['bottom_menu']->value;?>

			<div class="ow_copyright">
				<?php echo smarty_function_text(array('key'=>'base+copyright'),$_smarty_tpl);?>

			</div>
			<div style="float:right;">
				<?php echo $_smarty_tpl->tpl_vars['bottomPoweredByLink']->value;?>

			</div>
		</div>
	</div>
</div>    
<?php echo smarty_function_decorator(array('name'=>'floatbox'),$_smarty_tpl);
}
}
