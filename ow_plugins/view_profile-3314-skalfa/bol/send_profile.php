<?php

class VIEWPROFILE_BOL_SendProfile extends OW_Entity
{
    /**
     * @var integer
     */
    public $userId;

    /**
     * @var integer
     */
    public $whoseId;

    /**
     * @var string
     */
    public $email;

    /**
     * @var integer
     */
    public $status;

    /**
     * @var string
     */
    public $comment;

    /**
     * @var string
     */
    public $hash;

    /**
     * @var integer
     */
    public $createdDatetime;
}