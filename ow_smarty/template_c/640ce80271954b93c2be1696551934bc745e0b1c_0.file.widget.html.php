<?php
/* Smarty version 3.1.30, created on 2019-01-18 01:23:37
  from "/Users/esase/Sites/8418/ow_plugins/contactimporter/views/components/widget.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c4170e933ed55_31180478',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '640ce80271954b93c2be1696551934bc745e0b1c' => 
    array (
      0 => '/Users/esase/Sites/8418/ow_plugins/contactimporter/views/components/widget.html',
      1 => 1547792045,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c4170e933ed55_31180478 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_style')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/block.style.php';
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('style', array());
$_block_repeat1=true;
echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>


.contactimporter_provider_button
{
	margin: 3px;
        display: inline-block;
	cursor: pointer;
}

.contactimporter_provider_button img
{
    width: 45px;
}

<?php $_block_repeat1=false;
echo smarty_block_style(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>


<center>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['btns']->value, 'b');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['b']->value) {
?>
        <a href="<?php echo $_smarty_tpl->tpl_vars['b']->value['href'];?>
" onclick="<?php echo $_smarty_tpl->tpl_vars['b']->value['onclick'];?>
" class="<?php echo $_smarty_tpl->tpl_vars['b']->value['class'];?>
 contactimporter_provider_button">
            <img src="<?php echo $_smarty_tpl->tpl_vars['b']->value['iconUrl'];?>
" />
        </a>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</center>
<div style="display:none;">
<div class="contactimporter_email_invite_cont">
<?php echo $_smarty_tpl->tpl_vars['eicmp']->value;?>

</div>
</div><?php }
}
