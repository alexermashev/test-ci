<?php
/* Smarty version 3.1.30, created on 2018-07-19 05:57:39
  from "/var/www/biyebiye/public_html/ow_plugins/matchmaking/views/controllers/admin_index.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5060935df1a0_73405181',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7ba438e2c39f943e2ba100a0751fa8fc36739a2f' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_plugins/matchmaking/views/controllers/admin_index.html',
      1 => 1531989433,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b5060935df1a0_73405181 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_cycle')) require_once '/var/www/biyebiye/public_html/ow_libraries/vendor/smarty/smarty/libs/plugins/function.cycle.php';
echo $_smarty_tpl->tpl_vars['menu']->value;?>


<p class="ow_small" style="padding-left: 12px; margin-bottom: 6px;"><?php echo smarty_function_text(array('key'=>"matchmaking+admin_rules_manage_match_questions"),$_smarty_tpl);?>
</p>
<table width="100%" class="ow_margin ow_table_2">
    <tbody>
    <tr class="ow_alt2">
        <th style="width: 33%;">
            <div style="overflow:hidden;"><?php echo smarty_function_text(array('key'=>"matchmaking+admin_rules_title_question"),$_smarty_tpl);?>
</div>
        </th>
        <th style="width: 33%;">
            <div style="overflow:hidden;"><?php echo smarty_function_text(array('key'=>"matchmaking+admin_rules_title_coefficient"),$_smarty_tpl);?>
</div>
        </th>
        <?php if ($_smarty_tpl->tpl_vars['accTypeLabel']->value) {?>
        <th style="width: 33%;">
            <div style="overflow:hidden;"><?php echo smarty_function_text(array('key'=>"matchmaking+admin_rules_title_properties"),$_smarty_tpl);?>
</div>
        </th>
        <?php }?>
    </tr>
    <!-- -------------------------------------TABLE CELLS---------------------------------------------- -->
    <tr class="ow_tr_delimiter">
        <td></td>
    </tr>
    <tr class="ow_tr_first">
        <th class="section_value ow_admin_profile_question_dnd_cursor" colspan="9">
            <div style="width:100%;overflow:hidden;margin:0 auto; position: relative;"><?php echo smarty_function_text(array('key'=>"matchmaking+admin_rules_manage_matched_questions"),$_smarty_tpl);?>
</div>
        </th>
    </tr>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['list']->value, 'match');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['match']->value) {
?>
    <tr class="<?php echo smarty_function_cycle(array('values'=>" ow_alt2, ow_alt1"),$_smarty_tpl);?>
 ow_hover_catch" <?php if ($_smarty_tpl->tpl_vars['match']->value['required'] == false) {?>onmouseover="$('span.del-cont', this).show();" onmouseout="$('span.del-cont', this).hide();"<?php }?>>
    <td width="33%"><div style="overflow:hidden;"><?php echo $_smarty_tpl->tpl_vars['match']->value['question_name'];?>
</div></td>
    <td width="33%" class="ns-hover-block">
        <div style="position: relative;">
        <?php echo $_smarty_tpl->tpl_vars['match']->value['edit_coefficient'];?>

        <div style="position:absolute; top: -1px; right: 0;">
            <span class="del-cont ow_hover ow_nowrap" style="display: none;">
                <a class="ow_lbutton ow_red section_delete_button" href="<?php echo $_smarty_tpl->tpl_vars['match']->value['delete_url'];?>
"
                   onclick="return confirm(OW.getLanguageText('matchmaking', 'confirm_delete_text')) ? true : false;"><?php echo smarty_function_text(array('key'=>'matchmaking+btn_label_remove'),$_smarty_tpl);?>
</a>
            </span>
        </div>
        </div>
    </td>
    <?php if ($_smarty_tpl->tpl_vars['accTypeLabel']->value) {?>
    <td style="width: 33%;" class="ow_alt1 ow_small"><?php echo $_smarty_tpl->tpl_vars['match']->value['acc_type_label'];?>
</td>
    <?php }?>
    </tr>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>


    <tr class="ow_tr_delimiter">
        <td></td>
    </tr>
    </tbody>
</table>

<div class="ow_open_match_section" id="matchQuestionFormBtn"><span><?php echo smarty_function_text(array('key'=>"matchmaking+admin_rules_create_match_question"),$_smarty_tpl);?>
</span>
    <div></div>
</div>
<div id="matchQuestionForm" class="ow_hidden">
<div class="ow_small" style="padding-left: 12px; margin-bottom: 6px;"><?php echo smarty_function_text(array('key'=>"matchmaking+admin_rules_manage_create_desc"),$_smarty_tpl);?>
</div>
<table class="ow_table_2">
    <tbody>
    <tr class="ow_tr_first">
        <th class="section_value ow_admin_profile_question_dnd_cursor" colspan="9"><div style="width:100%;overflow:hidden;margin:0 auto; position: relative;"><?php echo smarty_function_text(array('key'=>"matchmaking+admin_rules_manage_create_other"),$_smarty_tpl);?>
</div></th>
        </th>
    </tr>

    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['otherList']->value, 'item');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
?>
    <tr>
        <td style="width: 33%;" class="ow_alt1"><div style="overflow:hidden;"><?php echo $_smarty_tpl->tpl_vars['item']->value['question_name'];?>
</div></td>
        <td style="width: 33%;" class="ow_alt1">
            <?php echo $_smarty_tpl->tpl_vars['item']->value['create_coefficient'];?>

        </td>
        <?php if ($_smarty_tpl->tpl_vars['otherListAccTypeLabel']->value) {?>
        <td style="width: 33%;" class="ow_alt1 ow_small"><?php echo $_smarty_tpl->tpl_vars['item']->value['acc_type_label'];?>
</td>
        <?php }?>
    </tr>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>


    <tr class="question_tr no_question" style="display: none;">
        <td colspan="9"></td>
    </tr>

    <tr class="ow_tr_delimiter">
        <td></td>
    </tr>
    </tbody>
</table>
</div><?php }
}
