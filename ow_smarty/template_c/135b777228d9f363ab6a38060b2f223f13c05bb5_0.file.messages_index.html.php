<?php
/* Smarty version 3.1.30, created on 2018-07-21 14:58:56
  from "/var/www/biyebiye/public_html/ow_plugins/mailbox/views/controllers/messages_index.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b538270510ea6_04031787',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '135b777228d9f363ab6a38060b2f223f13c05bb5' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_plugins/mailbox/views/controllers/messages_index.html',
      1 => 1479204280,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b538270510ea6_04031787 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_decorator')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.decorator.php';
if ($_smarty_tpl->tpl_vars['mailModeEnabled']->value && $_smarty_tpl->tpl_vars['isAuthorizedSendMessage']->value) {?>
<div class="ow_content_menu_wrap clearfix">
    <div class="ow_right">
        <?php echo smarty_function_decorator(array('name'=>"button",'type'=>"button",'class'=>"ow_ic_add",'id'=>"newMessageBtn",'langLabel'=>'mailbox+label_btn_new_message'),$_smarty_tpl);?>

    </div>
</div>
<?php }?>
<div class="ow_mailbox_table ow_alt1 clearfix <?php if (!$_smarty_tpl->tpl_vars['mailModeEnabled']->value || !$_smarty_tpl->tpl_vars['chatModeEnabled']->value) {?>ow_mailbox_table_single<?php }?>" id="messagesContainerControl">
    <?php echo $_smarty_tpl->tpl_vars['conversationList']->value;?>

    <?php echo $_smarty_tpl->tpl_vars['conversationContainer']->value;?>

</div><?php }
}
