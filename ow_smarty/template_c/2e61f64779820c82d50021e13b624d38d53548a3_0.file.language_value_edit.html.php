<?php
/* Smarty version 3.1.30, created on 2018-07-19 05:58:21
  from "/var/www/biyebiye/public_html/ow_system_plugins/base/views/components/language_value_edit.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5060bd6c5dc9_19586062',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2e61f64779820c82d50021e13b624d38d53548a3' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_system_plugins/base/views/components/language_value_edit.html',
      1 => 1479204252,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b5060bd6c5dc9_19586062 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_block_decorator')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.block_decorator.php';
if (!is_callable('smarty_block_form')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_cycle')) require_once '/var/www/biyebiye/public_html/ow_libraries/vendor/smarty/smarty/libs/plugins/function.cycle.php';
if (!is_callable('smarty_function_input')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_submit')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.submit.php';
?>
<div class="ow_automargin">

	<?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('block_decorator', array('name'=>"box",'type'=>"empty"));
$_block_repeat1=true;
echo smarty_block_block_decorator(array('name'=>"box",'type'=>"empty"), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

	<?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('name'=>"lang-values-edit"));
$_block_repeat2=true;
echo smarty_block_form(array('name'=>"lang-values-edit"), null, $_smarty_tpl, $_block_repeat2);
while ($_block_repeat2) {
ob_start();
?>

		<table class="ow_table_1 ow_form ow_smallmargin">
			<tr class="ow_center ow_tr_first">
				<th>Language</th>
				<th>Translation</th>
			</tr>
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['langs']->value, 'lang', false, NULL, 'lang', array (
  'last' => true,
  'iteration' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['lang']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_lang']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_lang']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_lang']->value['iteration'] == $_smarty_tpl->tpl_vars['__smarty_foreach_lang']->value['total'];
?>
				<tr class="<?php echo smarty_function_cycle(array('values'=>"ow_alt1,ow_alt2"),$_smarty_tpl);?>
 <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_lang']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_lang']->value['last'] : null)) {?>ow_tr_last<?php }?>">
					<td class="ow_label"><?php echo $_smarty_tpl->tpl_vars['lang']->value->getLabel();?>
 (<?php echo $_smarty_tpl->tpl_vars['lang']->value->getTag();?>
)</td>
					<td class="ow_value"><?php echo smarty_function_input(array('name'=>"lang[".((string)$_smarty_tpl->tpl_vars['lang']->value->id)."][".((string)$_smarty_tpl->tpl_vars['prefix']->value)."][".((string)$_smarty_tpl->tpl_vars['key']->value)."]"),$_smarty_tpl);?>
</td>
				</tr>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

		</table>
			<div class="clearfix ow_smallmargin"><div class="ow_right">
					<?php echo smarty_function_submit(array('name'=>"submit"),$_smarty_tpl);?>

			</div></div>
	<?php $_block_repeat2=false;
echo smarty_block_form(array('name'=>"lang-values-edit"), ob_get_clean(), $_smarty_tpl, $_block_repeat2);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

	<?php $_block_repeat1=false;
echo smarty_block_block_decorator(array('name'=>"box",'type'=>"empty"), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>


</div><?php }
}
