<?php

class STRIPETRIAL_CLASS_EventHandler
{
    /**
     * Class instance
     *
     * @var STRIPETRIAL_CLASS_EventHandler
     */
    private static $classInstance;

    /**
     * Class constructor
     */
    private function __construct()
    {

    }

    /**
     * Get instance
     *
     * @return STRIPETRIAL_CLASS_EventHandler
     */
    public static function getInstance()
    {
        if (self::$classInstance === null) {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    public function init()
    {
        $requestHandler = OW::getRequestHandler();

        $requestHandler->addCatchAllRequestsExclude('base.email_verify', 'STRIPETRIAL_CTRL_Main');
        $requestHandler->addCatchAllRequestsExclude('base.email_verify', 'BILLINGSTRIPE_CTRL_Action');

        OW::getEventManager()->bind(OW_EventManager::ON_PLUGINS_INIT, [$this, 'afterInit']);
    }

    public function afterInit()
    {
        $em = OW::getEventManager();

        $em->bind($em::ON_USER_REGISTER, array($this, 'onAfterRegister'));
//        $em->bind($em::ON_AFTER_ROUTE, array($this, 'onAfterRegister'));
        $em->bind(OW_EventManager::ON_AFTER_USER_COMPLETE_PROFILE, array($this, 'onAfterUserCompleteProfile'));
        $em->bind('skmobileapp.get_translations', array($this, 'onGetTranslations'));

        $em->bind(BOL_BillingService::EVENT_ON_AFTER_DELIVER_SALE, array($this, 'onAfterDeliverSale'));

        //$em->bind('class.get_instance.BASE_CTRL_EmailVerify', array($this, 'onEmailVerify'), -100000);
    }

    public function onEmailVerify( OW_Event $event )
    {
        $user = OW::getUser();
        $trialService = STRIPETRIAL_BOL_Service::getInstance();

        /**
         * @var STRIPETRIAL_BOL_Trial $userTrialStatus
         */
        $userTrialStatus = $trialService->getStatusByUserId($user->getId());

        if ( !empty($userTrialStatus) && $userTrialStatus->status == 0 )
        {
            $trialService->setTrial(1, $user->getId());
            UTIL_Url::redirect(OW::getRouter()->urlForRoute(STRIPETRIAL_BOL_Service::PLUGIN_KEY . '_main_index'));
        }
    }

    public function onAfterRegister( OW_Event $event )
    {
        if ( !OW::getUser()->isAuthenticated() )
        {
            return;
        }

        $user = OW::getUser();

        if ( $user->isAdmin() )
        {
            return;
        }

        $params = $event->getParams();
        $userId = $params['userId'];

        $trialService = STRIPETRIAL_BOL_Service::getInstance();
        $trialService->setTrial(0, $userId);

        if ( OW::getApplication()->getContext() == OW::CONTEXT_DESKTOP && $params['method'] != 'facebook' )
        {
            $user = BOL_UserDao::getInstance()->findById($user->getId());

            OW::getFeedback()->info(OW::getLanguage()->text('base', 'join_successful_join'));

            if ( OW::getConfig()->getValue('base', 'confirm_email') )
            {
                BOL_EmailVerifyService::getInstance()->sendUserVerificationMail($user);
            }

            UTIL_Url::redirect(OW::getRouter()->urlForRoute(STRIPETRIAL_BOL_Service::PLUGIN_KEY . '_main_index'));
        }
    }

    public function onAfterUserCompleteProfile( OW_Event $event )
    {
        if ( !OW::getUser()->isAuthenticated() )
        {
            return;
        }

        $user = OW::getUser();

        if ( $user->isAdmin() )
        {
            return;
        }

        $params = $event->getParams();
        $userId = $params['userId'];

        if ( OW::getApplication()->getContext() == OW::CONTEXT_DESKTOP )
        {
            $trialService = STRIPETRIAL_BOL_Service::getInstance();

            /**
             * @var STRIPETRIAL_BOL_Trial $status
             */
            $status = $trialService->getStatusByUserId($userId);

            if (!empty($status) && $status->status == 0)
            {
                if ( empty($_POST) || $_POST['form_name'] != 'accountTypeForm' )
                {
                    UTIL_Url::redirect(OW::getRouter()->urlForRoute(STRIPETRIAL_BOL_Service::PLUGIN_KEY . '_main_index'));
                }
            }
        }
    }

    public function onAfterDeliverSale( OW_Event $event )
    {
        $sale = $event->getParams()['saleDbo'];

        $trialService = STRIPETRIAL_BOL_Service::getInstance();

        $user = BOL_UserService::getInstance()->findUserById($sale->userId);

        if ( empty($user) )
        {
            return;
        }

        $accType = $user->getAccountType();

        if ( mb_strtolower($sale->pluginKey) === 'membership' &&
            mb_strtolower($sale->entityKey) === 'membership_plan' &&
            $sale->entityId == $trialService->getMembershipPlanByAccountType($accType, 'number')
        )
        {
            $trialService->setTrial(2, $sale->userId);
        }
    }

    /**
     * Get translations to skmobileapp
     * @param OW_Event $event
     */
    public function onGetTranslations( OW_Event $event )
    {
        $langService = BOL_LanguageService::getInstance();
        $translations = [];

        $my_keys = $langService->findAllPrefixKeys($langService->findPrefixId(STRIPETRIAL_BOL_Service::PLUGIN_KEY));

        foreach ( $my_keys as $item )
        {
            $translations[$item->key] = OW::getLanguage()->text(STRIPETRIAL_BOL_Service::PLUGIN_KEY, $item->key);
        }

        $event->add(STRIPETRIAL_BOL_Service::PLUGIN_KEY, $translations);
    }
}