<?php
/* Smarty version 3.1.30, created on 2018-07-26 00:33:40
  from "/var/www/biyebiye/public_html/ow_plugins/billing_stripe/views/controllers/admin_index.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b594f24829c28_07930029',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e464907e3f1db7062b123f1005c87aad8f9256bb' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_plugins/billing_stripe/views/controllers/admin_index.html',
      1 => 1532322022,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b594f24829c28_07930029 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_form')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_cycle')) require_once '/var/www/biyebiye/public_html/ow_libraries/vendor/smarty/smarty/libs/plugins/function.cycle.php';
if (!is_callable('smarty_function_label')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.label.php';
if (!is_callable('smarty_function_input')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_error')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.error.php';
if (!is_callable('smarty_function_submit')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.submit.php';
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('name'=>'settings-form'));
$_block_repeat1=true;
echo smarty_block_form(array('name'=>'settings-form'), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>


<img src="<?php echo $_smarty_tpl->tpl_vars['logoUrl']->value;?>
" />

<div class="ow_superwide">
<table class="ow_table_3">
    <tr class="ow_tr_first">
        <td class="ow_label" style="width: 30%"><?php echo smarty_function_text(array('key'=>'billingstripe+recurring_supported'),$_smarty_tpl);?>
</td>
        <td class="ow_value"><?php if ($_smarty_tpl->tpl_vars['gateway']->value->recurring) {
echo smarty_function_text(array('key'=>'billingstripe+available'),$_smarty_tpl);
}?></td>
    </tr>
    
    <tr class="ow_tr_last">
        <td class="ow_label"><?php echo smarty_function_text(array('key'=>'billingstripe+supported_currencies'),$_smarty_tpl);?>
</td>
        <td class="ow_value"><?php echo $_smarty_tpl->tpl_vars['gateway']->value->getCurrenciesString();?>
</td>
    </tr>
</table>
</div>

<?php if (!$_smarty_tpl->tpl_vars['currSupported']->value) {?>
    <div class="ow_anno ow_std_margin ow_nocontent"><?php echo smarty_function_text(array('key'=>"base+billing_currency_not_supported",'currency'=>$_smarty_tpl->tpl_vars['activeCurrency']->value),$_smarty_tpl);?>
</div>
<?php }?>

<table class="ow_table_1 ow_form">
    <tr>
        <th class="ow_name ow_txtleft" colspan="3">
            <span class="ow_section_icon ow_ic_gear_wheel"><?php echo smarty_function_text(array('key'=>'billingstripe+account_settings'),$_smarty_tpl);?>
</span>
        </th>
    </tr>
    <tr class="<?php echo smarty_function_cycle(array('values'=>'ow_alt1, ow_alt2'),$_smarty_tpl);?>
">
        <td class="ow_label"><?php echo smarty_function_label(array('name'=>'testSK'),$_smarty_tpl);?>
</td>
        <td class="ow_value">
            <?php echo smarty_function_input(array('name'=>'testSK'),$_smarty_tpl);?>
 <?php echo smarty_function_error(array('name'=>'testSK'),$_smarty_tpl);?>

        </td>
        <td class="ow_desc ow_small"></td>
    </tr>
    <tr class="<?php echo smarty_function_cycle(array('values'=>'ow_alt1, ow_alt2'),$_smarty_tpl);?>
">
        <td class="ow_label"><?php echo smarty_function_label(array('name'=>'testPK'),$_smarty_tpl);?>
</td>
        <td class="ow_value">
            <?php echo smarty_function_input(array('name'=>'testPK'),$_smarty_tpl);?>
 <?php echo smarty_function_error(array('name'=>'testPK'),$_smarty_tpl);?>

        </td>
        <td class="ow_desc ow_small"></td>
    </tr>
    <tr class="<?php echo smarty_function_cycle(array('values'=>'ow_alt1, ow_alt2'),$_smarty_tpl);?>
">
        <td class="ow_label"><?php echo smarty_function_label(array('name'=>'liveSK'),$_smarty_tpl);?>
</td>
        <td class="ow_value">
            <?php echo smarty_function_input(array('name'=>'liveSK'),$_smarty_tpl);?>
 <?php echo smarty_function_error(array('name'=>'liveSK'),$_smarty_tpl);?>

        </td>
        <td class="ow_desc ow_small"></td>
    </tr>
    <tr class="<?php echo smarty_function_cycle(array('values'=>'ow_alt1, ow_alt2'),$_smarty_tpl);?>
">
        <td class="ow_label"><?php echo smarty_function_label(array('name'=>'livePK'),$_smarty_tpl);?>
</td>
        <td class="ow_value">
            <?php echo smarty_function_input(array('name'=>'livePK'),$_smarty_tpl);?>
 <?php echo smarty_function_error(array('name'=>'livePK'),$_smarty_tpl);?>

        </td>
        <td class="ow_desc ow_small"></td>
    </tr>
    <tr class="<?php echo smarty_function_cycle(array('values'=>'ow_alt1, ow_alt2'),$_smarty_tpl);?>
">
        <td class="ow_label"><?php echo smarty_function_label(array('name'=>'sandboxMode'),$_smarty_tpl);?>
</td>
        <td class="ow_value">
            <?php echo smarty_function_input(array('name'=>'sandboxMode'),$_smarty_tpl);?>
 <?php echo smarty_function_error(array('name'=>'sandboxMode'),$_smarty_tpl);?>

        </td>
        <td class="ow_desc ow_small"></td>
    </tr>
    <tr class="<?php echo smarty_function_cycle(array('values'=>'ow_alt1, ow_alt2'),$_smarty_tpl);?>
">
        <td class="ow_label"><?php echo smarty_function_label(array('name'=>'requireData'),$_smarty_tpl);?>
</td>
        <td class="ow_value">
            <?php echo smarty_function_input(array('name'=>'requireData'),$_smarty_tpl);?>
 <?php echo smarty_function_error(array('name'=>'requireData'),$_smarty_tpl);?>

        </td>
        <td class="ow_desc ow_small"></td>
    </tr>
</table>
<div class="clearfix ow_submit ow_stdmargin">
	<div class="ow_right"><?php echo smarty_function_submit(array('name'=>'save','class'=>'ow_ic_save'),$_smarty_tpl);?>
</div>
</div>
<?php $_block_repeat1=false;
echo smarty_block_form(array('name'=>'settings-form'), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);
}
}
