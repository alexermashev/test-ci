<?php

class VIEWPROFILE_MCLASS_EventHandler
{
    /**
     * @var VIEWPROFILE_BOL_Service
     */
    private $service;

    /**
     * Class instance
     *
     * @var VIEWPROFILE_CLASS_EventHandler
     */
    private static $classInstance;

    /**
     * Class constructor
     */
    private function __construct()
    {
        $this->service = VIEWPROFILE_BOL_Service::getInstance();
    }

    /**
     * Get instance
     *
     * @return VIEWPROFILE_CLASS_EventHandler
     */
    public static function getInstance()
    {
        if (self::$classInstance === null) {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    public function init()
    {
        OW::getEventManager()->bind(OW_EventManager::ON_PLUGINS_INIT, [$this, 'afterInit']);
    }

    public function afterInit()
    {
        $em = OW::getEventManager();

        $em->bind($em::ON_USER_REGISTER, array($this, 'setLastTimeInEdit'));
        $em->bind('base.event.on_get_empty_questions_list', array($this, 'onGetEmptyQuestionsList'));
        $em->bind('class.get_instance.BASE_CMP_UserViewSection', array($this, 'onUserViewSection'));
//        $em->bind('base.questions_field_get_value', array($this, 'onQuestionsFieldGetValue'));
        $em->bind('skmobileapp.get_translations', array($this, 'onGetTranslations'));
//        $em->bind('base.get_user_view_questions', array($this, 'onGetUserViewQuestions'));



        $em->bind(OW_EventManager::ON_AFTER_ROUTE, array($this, 'onAfterRoute'), -999);

        $em->bind('skmobileapp.formatted_users_data', array($this, 'onFormattedUsersData'));
    }

    /**
     * On after route
     */
    public function onAfterRoute( $event )
    {
        if ( OW::getRequest()->isAjax() )
        {
            return;
        }

        $route = OW::getRouter()->route();

        if ( OW::getApplication()->getContext() == OW_Application::CONTEXT_MOBILE
            && $route['controller'] == 'VIEWPROFILE_CTRL_Main' && $route['action'] == 'opinion' )
        {
            if ( OW::getRequest()->isPost() && isset($_POST['form_name']) && $_POST['form_name'] == 'opinionForm' )
            {
                VIEWPROFILE_BOL_Service::getInstance()->saveOpinion($_POST);
            }

            $link = '?' . http_build_query($_GET);

            OW::getApplication()->redirect(OW::getRouter()->urlForRoute(VIEWPROFILE_BOL_Service::PLUGIN_KEY . '_main_opinion') . $link, OW::CONTEXT_DESKTOP);
        }
    }

    /**
     * Visit a edit profile
     * @param OW_Event $event
     */
    public function setLastTimeInEdit( OW_Event $event )
    {
        $params = $event->getParams();

        $this->service->setLastTimeInEdit(0, (int) $params['userId']);
    }

    /**
     * Return empty array if don't spent a week
     * @param OW_Event $event
     */
    public function onGetEmptyQuestionsList( OW_Event $event )
    {
        $data = $event->getData();
        $params = $event->getParams();
        $questionsCount = count($data);

        if ( $questionsCount > 0 )
        {
            $userId = $params['userId'];
            $lastTime = $this->service->findLastVisitByUserId($userId);
            $questionsEditStamp = OW::getConfig()->getValue('base', 'profile_question_edit_stamp');

            if ( time() - $lastTime->dateTime < VIEWPROFILE_BOL_Service::DATE_FOR_REDIRECT && $questionsEditStamp < (int) $lastTime->completeTime )
            {
                $event->setData([]);
            }
        }
    }

    /**
     * On user view section (for email notification)
     * @param OW_Event $event
     */
    public function onUserViewSection( OW_Event $event )
    {
        $handlerAtr = OW::getRequestHandler()->getHandlerAttributes();

        if ( ( OW::getRequest()->isPost() && isset($_POST['form_name']) || $_POST['form_name'] == 'sendingForm' )
            || $handlerAtr['controller'] == 'SKMOBILEAPP_MCTRL_Api'
        )
        {
            $params = $event->getParams();
            $event->setData(OW::getClassInstanceArray('VIEWPROFILE_CMP_UserViewSection', $params['arguments']));
        }
    }

    /**
     * Replace empty question's value by default
     * @param OW_Event $event
     */
    public function onQuestionsFieldGetValue( OW_Event $event )
    {
        $params = $event->getParams();
        $match = explode('_', $params['fieldName']);

        if ( $params['value'] == null && $match[0] != 'match' )
        {
            $event->setData(OW::getLanguage()->text('viewprofile', 'label_no_question_value'));
        }
    }

    /**
     * Get translations to skmobileapp
     * @param OW_Event $event
     */
    public function onGetTranslations( OW_Event $event )
    {
        $langService = BOL_LanguageService::getInstance();
        $translations = [];

        $my_keys = $langService->findAllPrefixKeys($langService->findPrefixId(VIEWPROFILE_BOL_Service::PLUGIN_KEY));

        foreach ($my_keys as $item) {
            $translations[$item->key] = OW::getLanguage()->text(VIEWPROFILE_BOL_Service::PLUGIN_KEY, $item->key);
        }

        $event->add(VIEWPROFILE_BOL_Service::PLUGIN_KEY, $translations);
    }

    /**
     * Return questions for free or VIP user
     * @param OW_Event $event
     */
    public function onGetUserViewQuestions( OW_Event $event )
    {
        $params = $event->getParams();
        $authService = BOL_AuthorizationService::getInstance();
        $userId = $params['userId'];
        $userId = OW::getUser()->getId();

        if ( $params['adminMode'] == true )
        {
            return;
        }

        if ( $authService->isActionAuthorizedForUser($userId, $authService::ADMIN_GROUP_NAME) )
        {
            return;
        }

        $isActionViewProfile = $authService->isActionAuthorizedForUser($userId, 'viewprofile', 'view_profile');

        if ( $isActionViewProfile )
        {
            return;
        }

        $data = $event->getData();
        $viewQuestions = VIEWPROFILE_BOL_Service::getFreeQuestions();
        $newQuestions = [];

        foreach ( $data as $question )
        {
            if ( in_array($question['name'], $viewQuestions) )
            {
                $newQuestions[] = $question;
            }
        }

        $event->setData($newQuestions);
    }

    public function onFormattedUsersData( OW_Event $event )
    {
        $data = $event->getData();

        $ids = [];
        foreach ( $data as $user )
        {
            $ids[] = $user['id'];
        }

        if ( count($ids) > 1 ) return;

        $newData = [];
        foreach ( $data as $user )
        {
            $countRequiredQuestions = VIEWPROFILE_BOL_Service::getInstance()->getCountEmptyRequiredQuestions($user['id']);
            $user['isAllRequired'] = $countRequiredQuestions == 0 ? false : true;

            $newData[] = $user;
        }

        $event->setData($newData);
    }
}