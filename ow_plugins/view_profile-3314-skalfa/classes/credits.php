<?php

class VIEWPROFILE_CLASS_Credits
{
    /**
     * Actions
     *
     * @var array
     */
    private $actions;

    /**
     * Auth actions
     *
     * @var array
     */
    private $authActions = [];
    
    public function __construct()
    {
        $this->actions[] = array('pluginKey' => 'viewprofile', 'action' => 'send_profile', 'amount' => 0);

        $this->authActions['send_profile'] = 'send_profile';
    }
    
    public function bindCreditActionsCollect( BASE_CLASS_EventCollector $e )
    {
        foreach ( $this->actions as $action )
        {
            $e->add($action);
        }        
    }
    
    public function triggerCreditActionsAdd()
    {
        $e = new BASE_CLASS_EventCollector('usercredits.action_add');
        
        foreach ( $this->actions as $action )
        {
            $e->add($action);
        }

        OW::getEventManager()->trigger($e);
    }
}