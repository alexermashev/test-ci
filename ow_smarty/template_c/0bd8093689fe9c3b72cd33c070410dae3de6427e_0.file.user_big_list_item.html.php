<?php
/* Smarty version 3.1.30, created on 2018-07-19 07:04:25
  from "/var/www/biyebiye/public_html/ow_plugins/skadate/decorators/user_big_list_item.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5070394bdf19_85430543',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0bd8093689fe9c3b72cd33c070410dae3de6427e' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_plugins/skadate/decorators/user_big_list_item.html',
      1 => 1479204282,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b5070394bdf19_85430543 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_url_for_route')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.url_for_route.php';
?>
<div class="ow_ulist_big_item">
    <div class="ow_ulist_big_item_cont">
        <div style="background-image: url('<?php echo $_smarty_tpl->tpl_vars['data']->value['avatar'];?>
');" class="ow_ulist_big_avatar">
            <?php if (!empty($_smarty_tpl->tpl_vars['data']->value['isMarked'])) {?>
                <div class="ow_ic_bookmark ow_bookmark_icon_ulist"></div>
            <?php }?>
            
            <?php if (!empty($_smarty_tpl->tpl_vars['data']->value['online'])) {?>
                <div class="ow_miniic_live"><span class="ow_live_on"></span></div>
            <?php }?>
            
            <a href="<?php echo smarty_function_url_for_route(array('for'=>"base_user_profile:[username=>".((string)$_smarty_tpl->tpl_vars['data']->value['username'])."]"),$_smarty_tpl);?>
" class="ow_ulist_big_avatar_shadow"></a>
            
            <?php if (!empty($_smarty_tpl->tpl_vars['data']->value['avatarAction'])) {?>
                <a <?php if (!empty($_smarty_tpl->tpl_vars['data']->value['avatarAction']['userId'])) {?>data-user-id="<?php echo $_smarty_tpl->tpl_vars['data']->value['avatarAction']['userId'];?>
" <?php }?>class="ow_ulist_big_avatar_bookmark" href="<?php echo $_smarty_tpl->tpl_vars['data']->value['avatarAction']['href'];?>
"><?php echo $_smarty_tpl->tpl_vars['data']->value['avatarAction']['label'];?>
</a>
            <?php }?>
        </div>
        <div class="ow_ulist_big_info">
            <div class="clearfix">
                <a class="ow_ulist_big_info_name ow_small" href="<?php echo smarty_function_url_for_route(array('for'=>"base_user_profile:[username=>".((string)$_smarty_tpl->tpl_vars['data']->value['username'])."]"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['data']->value['displayName'];?>
</a>
            </div>
            <?php echo $_smarty_tpl->tpl_vars['data']->value['fields'];?>

            <?php if (!empty($_smarty_tpl->tpl_vars['data']->value['activity'])) {?>
                <?php echo $_smarty_tpl->tpl_vars['data']->value['activity'];?>

            <?php }?>
        </div> 
    </div>
</div>
<?php }
}
