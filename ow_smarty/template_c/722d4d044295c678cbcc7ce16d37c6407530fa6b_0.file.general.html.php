<?php
/* Smarty version 3.1.30, created on 2018-07-19 04:47:47
  from "/var/www/biyebiye/public_html/ow_themes/trend/master_pages/general.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b505033afca42_68587619',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '722d4d044295c678cbcc7ce16d37c6407530fa6b' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_themes/trend/master_pages/general.html',
      1 => 1479204282,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b505033afca42_68587619 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_component')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.component.php';
if (!is_callable('smarty_function_add_content')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.add_content.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_decorator')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.decorator.php';
?>
<div class="ow_page_wrap_inner">
	<div class="ow_page_padding">
		<div class="ow_site_panel">
			<?php echo smarty_function_component(array('class'=>'BASE_CMP_Console'),$_smarty_tpl);?>

		</div>
		<div class="ow_header ow_header_general">
			<div class="sitelogo_and_join_btn">
				<a href="<?php echo $_smarty_tpl->tpl_vars['siteUrl']->value;?>
" class="sitelogo"></a>
			</div>
		</div>
		<div class="ow_menu_wrap"><?php echo $_smarty_tpl->tpl_vars['main_menu']->value;?>
</div>
		<div class="ow_page_container">
			
			<div class="ow_canvas">
				<div class="ow_page ow_bg_color clearfix">
					<?php if (!empty($_smarty_tpl->tpl_vars['heading']->value)) {?><h1 class="ow_stdmargin <?php echo $_smarty_tpl->tpl_vars['heading_icon_class']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['heading']->value;?>
</h1><?php }?>
					<div class="ow_content">
						<?php echo smarty_function_add_content(array('key'=>'base.add_page_top_content'),$_smarty_tpl);?>

						<?php echo $_smarty_tpl->tpl_vars['content']->value;?>

						<?php echo smarty_function_add_content(array('key'=>'base.add_page_bottom_content'),$_smarty_tpl);?>

					</div>
					<div class="ow_sidebar">
						<?php echo smarty_function_component(array('class'=>"BASE_CMP_Sidebar"),$_smarty_tpl);?>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ow_footer">
	<div class="ow_canvas">
		<div class="ow_page clearfix">
			<?php echo $_smarty_tpl->tpl_vars['bottom_menu']->value;?>

			<div class="ow_copyright">
				<?php echo smarty_function_text(array('key'=>'base+copyright'),$_smarty_tpl);?>

			</div>
			<div style="float:right;">
				<?php echo $_smarty_tpl->tpl_vars['bottomPoweredByLink']->value;?>

			</div>
		</div>
	</div>
</div>    
<?php echo smarty_function_decorator(array('name'=>'floatbox'),$_smarty_tpl);
}
}
