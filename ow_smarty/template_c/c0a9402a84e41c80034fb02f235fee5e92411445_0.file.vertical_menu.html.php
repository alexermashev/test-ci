<?php
/* Smarty version 3.1.30, created on 2018-07-19 06:55:09
  from "/var/www/biyebiye/public_html/ow_system_plugins/base/views/components/vertical_menu.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b506e0d4acdf2_80499374',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c0a9402a84e41c80034fb02f235fee5e92411445' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_system_plugins/base/views/components/vertical_menu.html',
      1 => 1479204252,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b506e0d4acdf2_80499374 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="ow_vertical_nav <?php echo $_smarty_tpl->tpl_vars['class']->value;?>
">
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value, 'item');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
?>
    <a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['url'];?>
" <?php if ($_smarty_tpl->tpl_vars['item']->value['new_window']) {?> target="_blank"<?php }?> class="ow_vertical_nav_item clearfix <?php if (!empty($_smarty_tpl->tpl_vars['item']->value['active'])) {?>selected<?php }?>">
        <span><?php echo $_smarty_tpl->tpl_vars['item']->value['label'];?>
</span>
        <?php if (isset($_smarty_tpl->tpl_vars['item']->value['number'])) {?>
        <span class="ow_count_wrap">
            <span class="ow_count_bg">
                <span class="ow_count"><?php echo $_smarty_tpl->tpl_vars['item']->value['number'];?>
</span>
            </span>
        </span>
        <?php }?>
    </a>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</div><?php }
}
