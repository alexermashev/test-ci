<?php
/* Smarty version 3.1.30, created on 2019-01-22 05:17:53
  from "/Users/esase/Sites/8418/ow_system_plugins/base/views/components/profile_action_toolbar.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c46edd1b0f770_40542941',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e447cf6582ba6aff66a860e28efacac6196c4bc8' => 
    array (
      0 => '/Users/esase/Sites/8418/ow_system_plugins/base/views/components/profile_action_toolbar.html',
      1 => 1547792051,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c46edd1b0f770_40542941 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="ow_profile_gallery_action_toolbar ow_profile_action_toolbar_wrap clearfix ow_stdmargin">
    <ul class="ow_bl ow_profile_action_toolbar clearfix ow_small ow_left">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['toolbar']->value, 'action');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['action']->value) {
?>
            <?php if (isset($_smarty_tpl->tpl_vars['action']->value['html'])) {?>
                <?php echo $_smarty_tpl->tpl_vars['action']->value['html'];?>

            <?php } elseif (isset($_smarty_tpl->tpl_vars['action']->value['extraLabel'])) {?>
                <li>
                    <?php echo $_smarty_tpl->tpl_vars['action']->value['extraLabel'];?>

                </li>
            <?php } else { ?>
                <li>
                    <a <?php echo $_smarty_tpl->tpl_vars['action']->value['attrs'];?>
 >
                        <?php echo $_smarty_tpl->tpl_vars['action']->value['label'];?>

                    </a>
                </li>
            <?php }?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

    </ul>

    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['groups']->value, 'group');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['group']->value) {
?>
        <?php echo $_smarty_tpl->tpl_vars['group']->value;?>

    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>


    <?php echo $_smarty_tpl->tpl_vars['cmpsMarkup']->value;?>

</div>
<?php }
}
