<?php
/* Smarty version 3.1.30, created on 2019-01-18 01:16:51
  from "/Users/esase/Sites/8418/ow_system_plugins/base/views/components/bottom_menu.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c416f53843c14_95927067',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1431a9d0c0be1bb48701f2af0042c23418e37dd9' => 
    array (
      0 => '/Users/esase/Sites/8418/ow_system_plugins/base/views/components/bottom_menu.html',
      1 => 1547792051,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c416f53843c14_95927067 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="ow_footer_menu">
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value, 'item', false, NULL, 'bottom_menu', array (
  'last' => true,
  'iteration' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_bottom_menu']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_bottom_menu']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_bottom_menu']->value['iteration'] == $_smarty_tpl->tpl_vars['__smarty_foreach_bottom_menu']->value['total'];
?>
	<a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['url'];?>
"<?php if ($_smarty_tpl->tpl_vars['item']->value['active']) {?> class="active"<?php }
if ($_smarty_tpl->tpl_vars['item']->value['new_window']) {?> target="_blank"<?php }?>><?php echo $_smarty_tpl->tpl_vars['item']->value['label'];?>
</a><?php if (!(isset($_smarty_tpl->tpl_vars['__smarty_foreach_bottom_menu']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_bottom_menu']->value['last'] : null)) {?> | <?php }
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</div><?php }
}
