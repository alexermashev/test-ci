<?php
/* Smarty version 3.1.30, created on 2019-01-27 22:58:38
  from "/Users/esase/Sites/8418/ow_system_plugins/base/views/controllers/edit_index.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c4e7dee3a2b24_43576852',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bad7a2a93ec6f6c94ae05540201be09ac94b9db7' => 
    array (
      0 => '/Users/esase/Sites/8418/ow_system_plugins/base/views/controllers/edit_index.html',
      1 => 1547792051,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c4e7dee3a2b24_43576852 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_style')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/block.style.php';
if (!is_callable('smarty_block_block_decorator')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/block.block_decorator.php';
if (!is_callable('smarty_function_text')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_block_form')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_label')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.label.php';
if (!is_callable('smarty_function_input')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_error')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.error.php';
if (!is_callable('smarty_function_cycle')) require_once '/Users/esase/Sites/8418/ow_libraries/vendor/smarty/smarty/libs/plugins/function.cycle.php';
if (!is_callable('smarty_function_question_description_lang')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.question_description_lang.php';
if (!is_callable('smarty_function_decorator')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.decorator.php';
if (!is_callable('smarty_function_submit')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.submit.php';
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('style', array());
$_block_repeat1=true;
echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

    .anno_padding_left {
        padding-left:45px;
    }
<?php $_block_repeat1=false;
echo smarty_block_style(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>


<?php echo '<script'; ?>
 language="javascript" type="text/javascript">
    $(function(){
        $(".unregister_profile_button").click(
            function() { window.location = "<?php echo $_smarty_tpl->tpl_vars['unregisterProfileUrl']->value;?>
" }
        );
   });
<?php echo '</script'; ?>
>


<?php if (!empty($_smarty_tpl->tpl_vars['changePassword']->value)) {?>
    <?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('block_decorator', array('name'=>"box",'type'=>"empty",'addClass'=>"ow_superwide ow_automargin"));
$_block_repeat1=true;
echo smarty_block_block_decorator(array('name'=>"box",'type'=>"empty",'addClass'=>"ow_superwide ow_automargin"), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

        <div class="clearfix ow_stdmargin"><div class="ow_right"><?php echo $_smarty_tpl->tpl_vars['changePassword']->value;?>
</div></div>
    <?php $_block_repeat1=false;
echo smarty_block_block_decorator(array('name'=>"box",'type'=>"empty",'addClass'=>"ow_superwide ow_automargin"), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

<?php }?>

<?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('block_decorator', array('name'=>"box",'type'=>"empty",'addClass'=>"ow_superwide ow_automargin"));
$_block_repeat1=true;
echo smarty_block_block_decorator(array('name'=>"box",'type'=>"empty",'addClass'=>"ow_superwide ow_automargin"), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

<?php if (isset($_smarty_tpl->tpl_vars['editSynchronizeHook']->value)) {?>
    <?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('block_decorator', array('name'=>"box",'addClass'=>"ow_center",'iconClass'=>'ow_ic_update','langLabel'=>'base+edit_remote_field_synchronize_title','style'=>"overflow:hidden;"));
$_block_repeat2=true;
echo smarty_block_block_decorator(array('name'=>"box",'addClass'=>"ow_center",'iconClass'=>'ow_ic_update','langLabel'=>'base+edit_remote_field_synchronize_title','style'=>"overflow:hidden;"), null, $_smarty_tpl, $_block_repeat2);
while ($_block_repeat2) {
ob_start();
?>

       <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['editSynchronizeHook']->value, 'item');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
?>
          <?php echo $_smarty_tpl->tpl_vars['item']->value;?>

       <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

    <?php $_block_repeat2=false;
echo smarty_block_block_decorator(array('name'=>"box",'addClass'=>"ow_center",'iconClass'=>'ow_ic_update','langLabel'=>'base+edit_remote_field_synchronize_title','style'=>"overflow:hidden;"), ob_get_clean(), $_smarty_tpl, $_block_repeat2);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

    <?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('block_decorator', array('name'=>"box",'type'=>"empty",'addClass'=>"ow_center",'style'=>"padding:15px;"));
$_block_repeat2=true;
echo smarty_block_block_decorator(array('name'=>"box",'type'=>"empty",'addClass'=>"ow_center",'style'=>"padding:15px;"), null, $_smarty_tpl, $_block_repeat2);
while ($_block_repeat2) {
ob_start();
?>

        <?php echo smarty_function_text(array('key'=>"base+join_or"),$_smarty_tpl);?>

    <?php $_block_repeat2=false;
echo smarty_block_block_decorator(array('name'=>"box",'type'=>"empty",'addClass'=>"ow_center",'style'=>"padding:15px;"), ob_get_clean(), $_smarty_tpl, $_block_repeat2);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

<?php }?>

<?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('name'=>'editForm'));
$_block_repeat2=true;
echo smarty_block_form(array('name'=>'editForm'), null, $_smarty_tpl, $_block_repeat2);
while ($_block_repeat2) {
ob_start();
?>

    <table class="ow_table_1 ow_form ow_stdmargin">
        <?php if ($_smarty_tpl->tpl_vars['displayAccountType']->value) {?>
            <tr class="ow_alt1 ow_tr_first">
                <td class="ow_label">
                    <?php echo smarty_function_label(array('name'=>'accountType'),$_smarty_tpl);?>

                </td>
                <td class="ow_value">
                    <?php echo smarty_function_input(array('name'=>'accountType'),$_smarty_tpl);?>

                    <div style="height:1px;"></div>
                    <?php echo smarty_function_error(array('name'=>'accountType'),$_smarty_tpl);?>

                </td>
                <td class="ow_desc ow_small">

                </td>
            </tr>
        <?php }?>
        <tr class="ow_tr_delimiter"><td></td></tr>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['questionArray']->value, 'questions', false, 'section', 'question', array (
  'last' => true,
  'first' => true,
  'index' => true,
  'iteration' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['section']->value => $_smarty_tpl->tpl_vars['questions']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_question']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_question']->value['index']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_question']->value['first'] = !$_smarty_tpl->tpl_vars['__smarty_foreach_question']->value['index'];
$_smarty_tpl->tpl_vars['__smarty_foreach_question']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_question']->value['iteration'] == $_smarty_tpl->tpl_vars['__smarty_foreach_question']->value['total'];
?>
            <?php if (!empty($_smarty_tpl->tpl_vars['section']->value)) {?>
                <tr class="ow_tr_first"><th colspan="3"><?php echo smarty_function_text(array('key'=>"base+questions_section_".((string)$_smarty_tpl->tpl_vars['section']->value)."_label"),$_smarty_tpl);?>
</th></tr>
            <?php }?>
            
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['questions']->value, 'question', false, NULL, 'question', array (
  'last' => true,
  'first' => true,
  'index' => true,
  'iteration' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['question']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_question']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_question']->value['index']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_question']->value['first'] = !$_smarty_tpl->tpl_vars['__smarty_foreach_question']->value['index'];
$_smarty_tpl->tpl_vars['__smarty_foreach_question']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_question']->value['iteration'] == $_smarty_tpl->tpl_vars['__smarty_foreach_question']->value['total'];
?>
                <?php smarty_function_cycle(array('assign'=>'alt','name'=>$_smarty_tpl->tpl_vars['section']->value,'values'=>'ow_alt1,ow_alt2'),$_smarty_tpl);?>

                <tr class=" <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_question']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_question']->value['last'] : null)) {?>ow_tr_last<?php }?> <?php if (!empty($_smarty_tpl->tpl_vars['changeList']->value[$_smarty_tpl->tpl_vars['question']->value['name']]) && $_smarty_tpl->tpl_vars['adminMode']->value) {?> ow_premoderation_high <?php }?>">
                    <td class="<?php echo $_smarty_tpl->tpl_vars['alt']->value;?>
 ow_label">
                        <?php echo smarty_function_label(array('name'=>$_smarty_tpl->tpl_vars['question']->value['name']),$_smarty_tpl);?>

                    </td>
                    <td class="<?php echo $_smarty_tpl->tpl_vars['alt']->value;?>
 ow_value">
                        <?php echo smarty_function_input(array('name'=>$_smarty_tpl->tpl_vars['question']->value['name']),$_smarty_tpl);?>

                        <div style="height:1px;"></div>
                        <?php echo smarty_function_error(array('name'=>$_smarty_tpl->tpl_vars['question']->value['name']),$_smarty_tpl);?>

                    </td>
                    <td class="<?php echo $_smarty_tpl->tpl_vars['alt']->value;?>
 ow_desc ow_small">
                        <?php echo smarty_function_question_description_lang(array('name'=>$_smarty_tpl->tpl_vars['question']->value['name']),$_smarty_tpl);?>

                    </td>
                </tr>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

            <?php if (!empty($_smarty_tpl->tpl_vars['section']->value)) {
if (!(isset($_smarty_tpl->tpl_vars['__smarty_foreach_question']->value['first']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_question']->value['first'] : null)) {?>
            <tr class="ow_tr_delimiter"><td></td></tr>
            <?php }?>
            <?php }?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

        <tr class="ow_tr_delimiter"><td></td></tr>
        <tr class="ow_tr_first">
            <th colspan="3"><?php echo smarty_function_text(array('key'=>'base+questions_section_user_photo_label'),$_smarty_tpl);?>
</th>
        </tr>
        <tr>
            <td class="ow_alt1 ow_label">
                <?php echo smarty_function_label(array('name'=>'avatar'),$_smarty_tpl);?>

            </td>
            <td class="ow_alt1 ow_value">
                <?php echo smarty_function_input(array('name'=>'avatar'),$_smarty_tpl);?>

                <?php echo smarty_function_error(array('name'=>'avatar'),$_smarty_tpl);?>

            </td>
            <td class="ow_alt1 ow_desc ow_small"></td>
        </tr>
    </table>

    <?php if (!$_smarty_tpl->tpl_vars['adminMode']->value && !$_smarty_tpl->tpl_vars['isAdmin']->value && $_smarty_tpl->tpl_vars['approveEnabled']->value) {?>
        <div class="ow_anno ow_std_margin anno_padding_left"><?php echo smarty_function_text(array('key'=>"base+edit_profile_warning"),$_smarty_tpl);?>
</div>
    <?php }?>

	<div class="clearfix ow_stdmargin<?php if (!$_smarty_tpl->tpl_vars['isAdmin']->value) {?> ow_btn_delimiter<?php }?>">
           <?php if ($_smarty_tpl->tpl_vars['adminMode']->value) {?>
                <?php if (!$_smarty_tpl->tpl_vars['isUserApproved']->value && !$_smarty_tpl->tpl_vars['isEditedUserModerator']->value) {?>
                    <?php echo smarty_function_decorator(array('name'=>"button",'class'=>"delete_user_by_moderator ow_ic_delete ow_red ow_negative",'langLabel'=>'base+delete_profile'),$_smarty_tpl);?>

                <?php }?>

                <div class="ow_right">
                    <?php if (!$_smarty_tpl->tpl_vars['isUserApproved']->value) {?>
                        <?php echo smarty_function_decorator(array('name'=>"button",'class'=>"write_message_button ow_green ow_positive",'langLabel'=>'base+write_message'),$_smarty_tpl);?>

                        <?php echo smarty_function_submit(array('name'=>'saveAndApprove'),$_smarty_tpl);?>

                    <?php } else { ?>
                        <?php echo smarty_function_submit(array('name'=>'editSubmit'),$_smarty_tpl);?>

                    <?php }?>
                </div>
           <?php } else { ?>
                 <?php if (!$_smarty_tpl->tpl_vars['isAdmin']->value) {?>
                    <?php echo smarty_function_decorator(array('name'=>"button",'class'=>"unregister_profile_button ow_ic_delete ow_red ow_negative",'langLabel'=>'base+delete_profile'),$_smarty_tpl);?>

                 <?php }?>
                 <div class="ow_right">
                    <?php echo smarty_function_submit(array('name'=>'editSubmit'),$_smarty_tpl);?>

                 </div>
		   <?php }?>

    </div>
<?php $_block_repeat2=false;
echo smarty_block_form(array('name'=>'editForm'), ob_get_clean(), $_smarty_tpl, $_block_repeat2);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

<?php $_block_repeat1=false;
echo smarty_block_block_decorator(array('name'=>"box",'type'=>"empty",'addClass'=>"ow_superwide ow_automargin"), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

<?php }
}
