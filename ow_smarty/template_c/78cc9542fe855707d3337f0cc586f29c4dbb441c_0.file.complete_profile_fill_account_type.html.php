<?php
/* Smarty version 3.1.30, created on 2019-01-21 06:10:36
  from "/Users/esase/Sites/8418/ow_system_plugins/base/views/controllers/complete_profile_fill_account_type.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c45a8acc7d268_15001398',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '78cc9542fe855707d3337f0cc586f29c4dbb441c' => 
    array (
      0 => '/Users/esase/Sites/8418/ow_system_plugins/base/views/controllers/complete_profile_fill_account_type.html',
      1 => 1547792051,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c45a8acc7d268_15001398 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_block_decorator')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/block.block_decorator.php';
if (!is_callable('smarty_block_form')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_label')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.label.php';
if (!is_callable('smarty_function_input')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_error')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.error.php';
if (!is_callable('smarty_function_question_description_lang')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.question_description_lang.php';
if (!is_callable('smarty_function_submit')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.submit.php';
?>

<?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('block_decorator', array('name'=>"box",'type'=>"empty",'addClass'=>"ow_superwide ow_automargin"));
$_block_repeat1=true;
echo smarty_block_block_decorator(array('name'=>"box",'type'=>"empty",'addClass'=>"ow_superwide ow_automargin"), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

    <?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('block_decorator', array('name'=>"box_cap",'type'=>"empty",'addClass'=>"ow_center",'style'=>"padding:15px;",'langLabel'=>"base+required_profile_questions"));
$_block_repeat2=true;
echo smarty_block_block_decorator(array('name'=>"box_cap",'type'=>"empty",'addClass'=>"ow_center",'style'=>"padding:15px;",'langLabel'=>"base+required_profile_questions"), null, $_smarty_tpl, $_block_repeat2);
while ($_block_repeat2) {
ob_start();
?>

    <?php $_block_repeat2=false;
echo smarty_block_block_decorator(array('name'=>"box_cap",'type'=>"empty",'addClass'=>"ow_center",'style'=>"padding:15px;",'langLabel'=>"base+required_profile_questions"), ob_get_clean(), $_smarty_tpl, $_block_repeat2);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>


    <?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('name'=>'accountTypeForm'));
$_block_repeat2=true;
echo smarty_block_form(array('name'=>'accountTypeForm'), null, $_smarty_tpl, $_block_repeat2);
while ($_block_repeat2) {
ob_start();
?>

        <table class="ow_table_1 ow_form ow_stdmargin">
            <tr class=" ow_tr_first ow_tr_last">
                <td class="ow_alt1 ow_label">
                    <?php echo smarty_function_label(array('name'=>'accountType'),$_smarty_tpl);?>

                </td>
                <td class="ow_alt1 ow_value">
                    <?php echo smarty_function_input(array('name'=>'accountType'),$_smarty_tpl);?>

                    <div style="height:1px;"></div>
                    <?php echo smarty_function_error(array('name'=>'accountType'),$_smarty_tpl);?>

                </td>
                <td class="ow_alt1 ow_desc ow_small">
                    <?php echo smarty_function_question_description_lang(array('name'=>'accountType'),$_smarty_tpl);?>

                </td>
            </tr>
        </table>
        <div class="clearfix ow_stdmargin">
           <div class="ow_right">
               <?php echo smarty_function_submit(array('name'=>'submit'),$_smarty_tpl);?>

           </div>
        </div>
    <?php $_block_repeat2=false;
echo smarty_block_form(array('name'=>'accountTypeForm'), ob_get_clean(), $_smarty_tpl, $_block_repeat2);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

<?php $_block_repeat1=false;
echo smarty_block_block_decorator(array('name'=>"box",'type'=>"empty",'addClass'=>"ow_superwide ow_automargin"), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);
}
}
