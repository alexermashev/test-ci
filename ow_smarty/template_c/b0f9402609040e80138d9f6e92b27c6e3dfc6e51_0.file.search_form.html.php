<?php
/* Smarty version 3.1.30, created on 2019-01-28 03:07:31
  from "/Users/esase/Sites/8418/ow_plugins/usearch/views/controllers/search_form.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c4eb8437dad74_24954865',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b0f9402609040e80138d9f6e92b27c6e3dfc6e51' => 
    array (
      0 => '/Users/esase/Sites/8418/ow_plugins/usearch/views/controllers/search_form.html',
      1 => 1547792046,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c4eb8437dad74_24954865 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_script')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/block.script.php';
if (!is_callable('smarty_block_form')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_text')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_cycle')) require_once '/Users/esase/Sites/8418/ow_libraries/vendor/smarty/smarty/libs/plugins/function.cycle.php';
if (!is_callable('smarty_function_label')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.label.php';
if (!is_callable('smarty_function_input')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_error')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.error.php';
if (!is_callable('smarty_function_submit')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.submit.php';
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('script', array());
$_block_repeat1=true;
echo smarty_block_script(array(), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>


$(function(){
    $("form[name='MainSearchForm'] [name='match_sex']").change(
        function(){ OW.trigger("usearch.lookin_for_changed", $(this).val()); }
    );
});

<?php $_block_repeat1=false;
echo smarty_block_script(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>


<?php if (isset($_smarty_tpl->tpl_vars['menu']->value)) {
echo $_smarty_tpl->tpl_vars['menu']->value;
}?>

<?php if (isset($_smarty_tpl->tpl_vars['authMessage']->value)) {?>
<div class="ow_anno ow_std_margin ow_nocontent"><?php echo $_smarty_tpl->tpl_vars['authMessage']->value;?>
</div>
<?php } else { ?>
<div class="clearfix">
    <div class="ow_superwide ow_automargin">
        <?php if ($_smarty_tpl->tpl_vars['usernameSearchEnabled']->value == true) {?>
            <?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('name'=>'UsernameSearchForm'));
$_block_repeat1=true;
echo smarty_block_form(array('name'=>'UsernameSearchForm'), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

            <h3 class="ow_std_margin"><?php echo smarty_function_text(array('key'=>"usearch+search_by_username"),$_smarty_tpl);?>
</h3>
            <table class="ow_table_1 ow_form">
                <tr class=" ow_tr_first ow_question_tr ow_tr_last">
                    <?php smarty_function_cycle(array('assign'=>'alt','values'=>'ow_alt1,ow_alt2'),$_smarty_tpl);?>

                    <td class="<?php echo $_smarty_tpl->tpl_vars['alt']->value;?>
 ow_label">
                        <?php echo smarty_function_label(array('name'=>'username'),$_smarty_tpl);?>

                    </td>
                    <td class="<?php echo $_smarty_tpl->tpl_vars['alt']->value;?>
 ow_value">
                        <?php echo smarty_function_input(array('name'=>'username'),$_smarty_tpl);?>

                        <div style="height:1px;"></div>
                        <?php echo smarty_function_error(array('name'=>'username'),$_smarty_tpl);?>

                    </td>
                </tr>
            </table>
            <div class="clearfix userach_submit_button">
                <div class="ow_right">
                    <?php echo smarty_function_submit(array('name'=>'SearchFormSubmit'),$_smarty_tpl);?>

                </div>
            </div>
            <?php $_block_repeat1=false;
echo smarty_block_form(array('name'=>'UsernameSearchForm'), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

        <?php }?>
        <?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('name'=>'MainSearchForm'));
$_block_repeat1=true;
echo smarty_block_form(array('name'=>'MainSearchForm'), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

        <?php if ($_smarty_tpl->tpl_vars['usernameSearchEnabled']->value == true) {?>
            <h3 class="ow_std_margin"><?php echo smarty_function_text(array('key'=>"usearch+advanced_search"),$_smarty_tpl);?>
</h3>
        <?php }?>
        <table class="ow_table_1 ow_form">
            <?php if ($_smarty_tpl->tpl_vars['displayAccountType']->value == true) {?>
                <?php if (!empty($_smarty_tpl->tpl_vars['displayGender']->value)) {?>
                    <tr class=" ow_tr_first ow_question_tr ow_tr_last">
                        <?php smarty_function_cycle(array('assign'=>'alt','values'=>'ow_alt1,ow_alt2'),$_smarty_tpl);?>

                        <td class="<?php echo $_smarty_tpl->tpl_vars['alt']->value;?>
 ow_label">
                            <?php echo smarty_function_label(array('name'=>'sex'),$_smarty_tpl);?>

                        </td>
                        <td class="<?php echo $_smarty_tpl->tpl_vars['alt']->value;?>
 ow_value">
                            <?php echo smarty_function_input(array('name'=>'sex'),$_smarty_tpl);?>

                            <div style="height:1px;"></div>
                            <?php echo smarty_function_error(array('name'=>'sex'),$_smarty_tpl);?>

                        </td>
                    </tr>
                <?php }?>
                <tr class="ow_tr_first ow_question_tr">
                    <?php smarty_function_cycle(array('assign'=>'alt','values'=>'ow_alt1,ow_alt2'),$_smarty_tpl);?>

                    <td class="<?php echo $_smarty_tpl->tpl_vars['alt']->value;?>
 ow_label">
                        <?php echo smarty_function_label(array('name'=>'match_sex'),$_smarty_tpl);?>

                    </td>
                    <td class="<?php echo $_smarty_tpl->tpl_vars['alt']->value;?>
 ow_value">
                        <?php echo smarty_function_input(array('name'=>'match_sex'),$_smarty_tpl);?>

                        <div style="height:1px;"></div>
                        <?php echo smarty_function_error(array('name'=>'match_sex'),$_smarty_tpl);?>

                    </td>
                </tr>
                
                <tr class="ow_question_tr">
                    <?php smarty_function_cycle(array('assign'=>'alt','values'=>'ow_alt1,ow_alt2'),$_smarty_tpl);?>

                    <td class="<?php echo $_smarty_tpl->tpl_vars['alt']->value;?>
 ow_label">
                        <?php echo smarty_function_label(array('name'=>'online'),$_smarty_tpl);?>

                    </td>
                    <td class="<?php echo $_smarty_tpl->tpl_vars['alt']->value;?>
 ow_value">
                        <?php echo smarty_function_input(array('name'=>'online'),$_smarty_tpl);?>

                        <div style="height:1px;"></div>
                        <?php echo smarty_function_error(array('name'=>'online'),$_smarty_tpl);?>

                    </td>
                </tr>
                
                <tr class="ow_tr_last ow_question_tr <?php echo $_smarty_tpl->tpl_vars['question_prefix']->value;?>
with_photo">
                    <?php smarty_function_cycle(array('assign'=>'alt','values'=>'ow_alt1,ow_alt2'),$_smarty_tpl);?>

                    <td class="<?php echo $_smarty_tpl->tpl_vars['alt']->value;?>
 ow_label">
                        <?php echo smarty_function_label(array('name'=>'with_photo'),$_smarty_tpl);?>

                    </td>
                    <td class="<?php echo $_smarty_tpl->tpl_vars['alt']->value;?>
 ow_value">
                        <?php echo smarty_function_input(array('name'=>'with_photo'),$_smarty_tpl);?>

                        <div style="height:1px;"></div>
                        <?php echo smarty_function_error(array('name'=>'with_photo'),$_smarty_tpl);?>

                    </td>
                </tr>
            <?php }?>
            </table>
            <div class="ow_preloader ow_std_margin usearch_preloader" style="display:none;"></div>
            <div class="questions_div usearch_transition">
                <table class="ow_table_1 ow_form">
                <?php if (!empty($_smarty_tpl->tpl_vars['questionList']->value)) {?>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['questionList']->value, 'questions', false, 'section');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['section']->value => $_smarty_tpl->tpl_vars['questions']->value) {
?>
                    <?php if (!empty($_smarty_tpl->tpl_vars['section']->value)) {?>
                    <tr class="<?php echo $_smarty_tpl->tpl_vars['section_prefix']->value;
echo $_smarty_tpl->tpl_vars['section']->value;?>
 ow_tr_first <?php if (empty($_smarty_tpl->tpl_vars['visibilityList']->value['sections'][$_smarty_tpl->tpl_vars['section']->value])) {?>ow_hidden<?php }?>" ><th colspan="3"><?php echo smarty_function_text(array('key'=>"base+questions_section_".((string)$_smarty_tpl->tpl_vars['section']->value)."_label"),$_smarty_tpl);?>
</th></tr>
                    <?php }?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['questions']->value, 'question', false, NULL, 'question', array (
  'last' => true,
  'iteration' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['question']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_question']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_question']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_question']->value['iteration'] == $_smarty_tpl->tpl_vars['__smarty_foreach_question']->value['total'];
?>
                        <tr class="<?php echo $_smarty_tpl->tpl_vars['question_prefix']->value;
echo $_smarty_tpl->tpl_vars['question']->value['name'];?>
 ow_question_tr <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_question']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_question']->value['last'] : null)) {?>ow_tr_last<?php }?> <?php if (empty($_smarty_tpl->tpl_vars['visibilityList']->value['questions'][$_smarty_tpl->tpl_vars['question']->value['name']])) {?>ow_hidden<?php }?>">
                            <?php smarty_function_cycle(array('assign'=>'alt','values'=>'ow_alt1,ow_alt2'),$_smarty_tpl);?>

                            <td class="<?php echo $_smarty_tpl->tpl_vars['alt']->value;?>
 ow_label">
                                <?php echo smarty_function_label(array('name'=>$_smarty_tpl->tpl_vars['question']->value['name']),$_smarty_tpl);?>

                            </td>
                            <td class="<?php echo $_smarty_tpl->tpl_vars['alt']->value;?>
 ow_value">
                                <?php echo smarty_function_input(array('name'=>$_smarty_tpl->tpl_vars['question']->value['name']),$_smarty_tpl);?>

                                <div style="height:1px;"></div>
                                <?php echo smarty_function_error(array('name'=>$_smarty_tpl->tpl_vars['question']->value['name']),$_smarty_tpl);?>

                            </td>
                        </tr>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                    <tr class="<?php echo $_smarty_tpl->tpl_vars['section_prefix']->value;
echo $_smarty_tpl->tpl_vars['section']->value;?>
 ow_tr_delimiter <?php if (empty($_smarty_tpl->tpl_vars['visibilityList']->value['sections'][$_smarty_tpl->tpl_vars['section']->value])) {?>ow_hidden<?php }?>"><td></td></tr>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                <?php }?>
                
            </table>
            <div class="clearfix userach_submit_button">
                <div class="ow_right">
                    <?php echo smarty_function_submit(array('name'=>'SearchFormSubmit'),$_smarty_tpl);?>

                </div>
            </div>
            </div>
        <?php $_block_repeat1=false;
echo smarty_block_form(array('name'=>'MainSearchForm'), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

    </div>
</div>
<?php }
}
}
