<?php
/* Smarty version 3.1.30, created on 2018-07-19 06:55:09
  from "/var/www/biyebiye/public_html/ow_plugins/premoderation/views/controllers/moderation_approve.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b506e0d5216e1_02281359',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'db45f77e7002519ad39895678d5b9fc4b6fbf982' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_plugins/premoderation/views/controllers/moderation_approve.html',
      1 => 1479204282,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b506e0d5216e1_02281359 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_style')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.style.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_decorator')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.decorator.php';
echo '<script'; ?>
 type="text/javascript">

function MODERATION_ApproveInit( uniqId, options )
{
    var c = $("#" + uniqId);
    
    $("[data-checkall]", c).click(function() {
        $("[data-item]", c).prop("checked", this.checked);
    });

    $("[data-command]", c).click(function() {
        var node = $(this),
            command = node.data("command"),
            contentLabel = node.data("content"),
            action = command.split(".")[0],
            itemKey = node.data("item"),
            form = $("#" + uniqId + "-form");
            
        var count = c.find("[data-item]:checked").length;
        
        if ( !contentLabel && !count ) {
            alert(OW.getLanguageText("base", "moderation_no_items_warning"));
            
            return false;
        }
        
        var deleteConfirmMsg = contentLabel
            ? OW.getLanguageText("base", "moderation_delete_confirmation", { "content": contentLabel })
            : OW.getLanguageText("base", "moderation_delete_multiple_confirmation", { "content": options.groupLabel, "count": count });

        if ( action === "delete" && !confirm(deleteConfirmMsg) ) {
            return false;
        }

        $(form.get(0)["command"]).val(command);
        $(form.get(0)["item"]).val(itemKey);

        form.submit();
        
        return false;
    });
    
    
    (function() {
        
        var OFFSET = 40;
        var stickers = [];
        
        function stick( sticker ) {
            sticker.addClass("ow_moderation_sticked");
        };
        
        function unstick( sticker ) {
            sticker.removeClass("ow_moderation_sticked");
        };
        
        $(document).on("scroll", function() {
            var top = $(document).scrollTop();
            $.each(stickers, function(i, sticker) {
                if ( sticker.top - top <= OFFSET ) {
                    stick(sticker.el);
                } else {
                    unstick(sticker.el);
                }
            });
        });
        
        $(".ow_moderation_sticky").each(function() {
            var sticker = {};
            sticker.el = $(this);
            sticker.top = sticker.el.position().top;
                        
            stickers.push(sticker);
        });
    })();
    
    
}

<?php echo '</script'; ?>
>

<?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('style', array());
$_block_repeat1=true;
echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>


.ow_moderation_sticked {
    position: fixed;
    top: 40px;
}

<?php $_block_repeat1=false;
echo smarty_block_style(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>


<?php if (!empty($_smarty_tpl->tpl_vars['menu']->value)) {
echo $_smarty_tpl->tpl_vars['menu']->value;
}?>

<div class="ow_moderation_wrap clearfix" id="<?php echo $_smarty_tpl->tpl_vars['uniqId']->value;?>
">
    <div class="ow_moderation_sticky">
        <div class="ow_smallmargin">
            <?php echo $_smarty_tpl->tpl_vars['contentMenu']->value;?>

        </div>
        <table class="ow_table_2">
            <tbody><tr class="ow_tr_first ow_tr_last ow_alt1">
                <td class="ow_txtleft" style="border-right: none;">
                    <input type="checkbox" data-checkall="">
                </td>
                <td class="ow_txtleft" style="border-right: none;">
                    <span><?php echo smarty_function_text(array('key'=>'base+check_all_to'),$_smarty_tpl);?>
</span>
                </td>
                <td>
                    <div class="ow_moderation_label_bnts ow_left">
                        <?php if ($_smarty_tpl->tpl_vars['actions']->value['approve']) {?>
                            <a data-command="approve.multiple" class="ow_lbutton ow_smallmargin ow_green" href="javascript://"><?php echo smarty_function_text(array('key'=>'base+approve'),$_smarty_tpl);?>
</a>
                            <br />
                        <?php }?>
                        <a data-command="delete.multiple" class="ow_lbutton ow_red" href="javascript://"><?php echo smarty_function_text(array('key'=>'base+delete'),$_smarty_tpl);?>
</a>
                    </div>
                </td>
            </tr>
        </tbody></table>
    </div>
    <div class="ow_left" style="width: 100%;">

        <form action="<?php echo $_smarty_tpl->tpl_vars['responderUrl']->value;?>
" method="post" id="<?php echo $_smarty_tpl->tpl_vars['uniqId']->value;?>
-form">
            <input type="hidden" name="command" />
            <input type="hidden" name="item" />
            
        <table class="ow_table_2 ow_margin ow_moderation_table">
        <tbody>
            <tr class="ow_tr_first">
                <th><?php echo $_smarty_tpl->tpl_vars['group']->value['label'];?>
</th>
                <th><?php echo smarty_function_text(array('key'=>"base+moderation_action"),$_smarty_tpl);?>
</th>
            </tr>
                
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['items']->value, 'item');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
?>
            <tr class="ow_alt1">
                <td>
                    <div class="clearfix ow_moderation_content_wrap">
                        <input type="checkbox" class="ow_left" data-item="<?php echo $_smarty_tpl->tpl_vars['item']->value['entityType'];?>
:<?php echo $_smarty_tpl->tpl_vars['item']->value['entityId'];?>
" name="items[]" value="<?php echo $_smarty_tpl->tpl_vars['item']->value['entityType'];?>
:<?php echo $_smarty_tpl->tpl_vars['item']->value['entityId'];?>
">
                       <div class="ow_user_list_picture">
                           <?php echo smarty_function_decorator(array('name'=>"avatar_item",'data'=>$_smarty_tpl->tpl_vars['item']->value['avatar']),$_smarty_tpl);?>

                        </div>
                        <div class="ow_user_list_data">
                            <div class="ow_moderation_string ow_txtleft ow_small ow_smallmargin">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['avatar']['url'];?>
"><b><?php echo $_smarty_tpl->tpl_vars['item']->value['avatar']['title'];?>
</b></a>
                                <?php echo $_smarty_tpl->tpl_vars['item']->value['string'];?>

                            </div>
                            <div class="ow_moderation_content ow_txtleft">
                                <?php echo $_smarty_tpl->tpl_vars['item']->value['content'];?>

                            </div>      

                        </div>
                    </div>
                    <div class="ow_newsfeed_btns ow_small ow_remark clearfix">
                        <span class="ow_nowrap create_time ow_newsfeed_date ow_small" style="line-height: 14px;"><?php echo smarty_function_text(array('key'=>"moderation+approve_".((string)$_smarty_tpl->tpl_vars['item']->value['reason'])."_time",'time'=>$_smarty_tpl->tpl_vars['item']->value['time']),$_smarty_tpl);?>
</span>
                    </div>
                </td>
                <td class="ow_small">
                    <div class="ow_moderation_label_bnts">
                        <?php if ($_smarty_tpl->tpl_vars['actions']->value['approve']) {?>
                        <a data-command="approve.single" data-item="<?php echo $_smarty_tpl->tpl_vars['item']->value['entityType'];?>
:<?php echo $_smarty_tpl->tpl_vars['item']->value['entityId'];?>
" data-content="<?php echo strip_tags($_smarty_tpl->tpl_vars['item']->value['contentLabel']);?>
" class="ow_lbutton ow_smallmargin ow_green" href="javascript://"><?php echo smarty_function_text(array('key'=>'base+approve'),$_smarty_tpl);?>
</a>
                        <br />
                        <?php }?>
                        <a data-command="delete.single" data-item="<?php echo $_smarty_tpl->tpl_vars['item']->value['entityType'];?>
:<?php echo $_smarty_tpl->tpl_vars['item']->value['entityId'];?>
" data-content="<?php echo strip_tags($_smarty_tpl->tpl_vars['item']->value['contentLabel']);?>
" class="ow_lbutton ow_red" href="javascript://"><?php echo smarty_function_text(array('key'=>'base+delete'),$_smarty_tpl);?>
</a>
                    </div>
                </td>
            </tr>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

        </tbody>
        </table>
            
        </form>

        <div class="ow_smallmargin">
            <?php echo $_smarty_tpl->tpl_vars['paging']->value;?>

        </div>

    </div>
</div><?php }
}
