<?php
/* Smarty version 3.1.30, created on 2018-07-26 00:53:42
  from "/var/www/biyebiye/public_html/ow_system_plugins/admin/views/controllers/finance_index.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5953d63d6307_42935118',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cd283b7b036685147b94b6b44d1e197cfd02f4fd' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_system_plugins/admin/views/controllers/finance_index.html',
      1 => 1479204252,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b5953d63d6307_42935118 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_cycle')) require_once '/var/www/biyebiye/public_html/ow_libraries/vendor/smarty/smarty/libs/plugins/function.cycle.php';
if (!is_callable('smarty_function_user_link')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.user_link.php';
if (!is_callable('smarty_function_format_date')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.format_date.php';
?>

<?php echo $_smarty_tpl->tpl_vars['paging']->value;?>


<table class="ow_table_2">
    <tr class="ow_tr_first">
        <th class="ow_nowrap"><?php echo smarty_function_text(array('key'=>'base+billing_transaction_id'),$_smarty_tpl);?>
</th>
        <th><?php echo smarty_function_text(array('key'=>'base+billing_gateway'),$_smarty_tpl);?>
</th>
        <th><?php echo smarty_function_text(array('key'=>'admin+plugin'),$_smarty_tpl);?>
</th>
        <th><?php echo smarty_function_text(array('key'=>'base+billing_details'),$_smarty_tpl);?>
</th>
        <th><?php echo smarty_function_text(array('key'=>'base+billing_amount'),$_smarty_tpl);?>
</th>
        <th><?php echo smarty_function_text(array('key'=>'admin+currency'),$_smarty_tpl);?>
</th>
        <th><?php echo smarty_function_text(array('key'=>'admin+user'),$_smarty_tpl);?>
</th>
        <th><?php echo smarty_function_text(array('key'=>'base+time'),$_smarty_tpl);?>
</th>
    </tr>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['list']->value, 'sale', false, NULL, 'sale', array (
  'last' => true,
  'iteration' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['sale']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_sale']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_sale']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_sale']->value['iteration'] == $_smarty_tpl->tpl_vars['__smarty_foreach_sale']->value['total'];
?>
    <tr class="ow_alt<?php echo smarty_function_cycle(array('values'=>'1,2'),$_smarty_tpl);?>
 <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_sale']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_sale']->value['last'] : null)) {?>ow_tr_last<?php }?>">
        <td><?php echo $_smarty_tpl->tpl_vars['sale']->value['transactionUid'];?>
</td>
        <td>
            <?php if ($_smarty_tpl->tpl_vars['sale']->value['gatewayKey'] != '') {?>
                <?php echo smarty_function_text(array('key'=>((string)$_smarty_tpl->tpl_vars['sale']->value['gatewayKey'])."+gateway_title"),$_smarty_tpl);?>

            <?php } else { ?>
                <span class="ow_small ow_remark"><?php echo smarty_function_text(array('key'=>'base+billing_gateway_unavailable'),$_smarty_tpl);?>
</span>
            <?php }?>
        </td>
        <td><?php echo $_smarty_tpl->tpl_vars['sale']->value['pluginTitle'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['sale']->value['entityDescription'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['sale']->value['totalAmount'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['sale']->value['currency'];?>
</td>
        <td>
            <?php if ($_smarty_tpl->tpl_vars['sale']->value['userId'] != null) {?>
                <?php echo smarty_function_user_link(array('username'=>$_smarty_tpl->tpl_vars['userNames']->value[$_smarty_tpl->tpl_vars['sale']->value['userId']],'name'=>$_smarty_tpl->tpl_vars['displayNames']->value[$_smarty_tpl->tpl_vars['sale']->value['userId']]),$_smarty_tpl);?>

            <?php } else { ?>
                <span class="ow_small ow_remark">-</span>
            <?php }?>
        </td>
        <td class="ow_small"><?php echo smarty_function_format_date(array('timestamp'=>$_smarty_tpl->tpl_vars['sale']->value['timeStamp']),$_smarty_tpl);?>
</td>
    </tr>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</table>

<?php echo $_smarty_tpl->tpl_vars['paging']->value;?>


<div class="ow_supernarrow ow_automargin">
<table class="ow_table_3">
<tr class="ow_tr_first"><th class="ow_label" style="text-align: center" colspan="2"><b><?php echo smarty_function_text(array('key'=>'base+billing_statistics'),$_smarty_tpl);?>
</b></th></tr>
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['stats']->value, 'sum', false, 'cur');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['cur']->value => $_smarty_tpl->tpl_vars['sum']->value) {
?>
    <tr>
        <td class="ow_label"><?php echo $_smarty_tpl->tpl_vars['cur']->value;?>
</td>
        <td class="ow_value"><?php echo $_smarty_tpl->tpl_vars['sum']->value;?>
</td>
    </tr>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</table>
</div><?php }
}
