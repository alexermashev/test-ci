<?php
/* Smarty version 3.1.30, created on 2019-01-18 01:16:51
  from "/Users/esase/Sites/8418/ow_plugins/usearch/views/components/quick_search.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c416f53cd77f3_26488575',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '571b9957092abf8aafbcc9f1bbf9216caccd452a' => 
    array (
      0 => '/Users/esase/Sites/8418/ow_plugins/usearch/views/components/quick_search.html',
      1 => 1547792046,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c416f53cd77f3_26488575 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_style')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/block.style.php';
if (!is_callable('smarty_block_form')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_label')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.label.php';
if (!is_callable('smarty_function_input')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_text')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_error')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.error.php';
if (!is_callable('smarty_function_submit')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.submit.php';
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('style', array());
$_block_repeat1=true;
echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>



    .ow_qs_header { 
        padding-bottom: 20px; 
    }

    .ow_qs_field { 
        padding-bottom: 15px; 
    }

    .ow_qs_label { 
        padding: 0 10px 0 10px; 
    }

    .ow_qs_field input[type="text"] {
        max-width: 70%;
    }

    .ow_qs_field input.ow_googlelocation_search_location
    {
        max-width:100%;
    }

   .ow_qs_field .ow_qs_miles {
        width: 50px;
        text-align: center;
    }

     .ow_qs_field .ow_qs_locationstr { 
        width: 150px; 
    }

    form .ow_googlelocation_search_distance { 
        text-align: center; 
    }

    .ow_qs_btn .ow_qs_label {
        padding: 6px 10px 0px;
        display: inline-block;
    }

<?php $_block_repeat1=false;
echo smarty_block_style(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>


<div class="ow_qs">
    <?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('name'=>'QuickSearchForm'));
$_block_repeat1=true;
echo smarty_block_form(array('name'=>'QuickSearchForm'), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>


    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['questionList']->value, 'question');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['question']->value) {
?>

        <?php if ($_smarty_tpl->tpl_vars['question']->value->name == 'match_sex') {?>
            <?php if (!empty($_smarty_tpl->tpl_vars['form']->value->displayAccountType)) {?>
                <div class="ow_qs_field ow_qs_presentation_<?php echo $_smarty_tpl->tpl_vars['question']->value->presentation;?>
">
                    <span class="ow_qs_label"><?php echo smarty_function_label(array('name'=>$_smarty_tpl->tpl_vars['question']->value->name),$_smarty_tpl);?>
</span>
                    <span class="ow_qs_value"><?php echo smarty_function_input(array('name'=>$_smarty_tpl->tpl_vars['question']->value->name),$_smarty_tpl);?>
</span>
                </div>
            <?php }?>
        <?php } elseif ($_smarty_tpl->tpl_vars['question']->value->name == 'sex') {?>
            <?php if (!empty($_smarty_tpl->tpl_vars['form']->value->displayAccountType)) {?>
                <div class="ow_qs_field ow_qs_presentation_<?php echo $_smarty_tpl->tpl_vars['question']->value->presentation;?>
">
                    <span class="ow_qs_label"><?php echo smarty_function_label(array('name'=>$_smarty_tpl->tpl_vars['question']->value->name),$_smarty_tpl);?>
</span>
                    <span class="ow_qs_value"><?php echo smarty_function_input(array('name'=>$_smarty_tpl->tpl_vars['question']->value->name),$_smarty_tpl);?>
</span>
                </div>
            <?php }?>
        <?php } elseif ($_smarty_tpl->tpl_vars['question']->value->name == 'birthdate') {?>
             <div class="ow_qs_field ow_qs_presentation_<?php echo $_smarty_tpl->tpl_vars['question']->value->presentation;?>
">
                <span class="ow_qs_label"><label><?php echo smarty_function_text(array('key'=>'usearch+age'),$_smarty_tpl);?>
</label></span>
                <span class="ow_qs_value"><?php echo smarty_function_input(array('name'=>'birthdate'),$_smarty_tpl);?>
</span>
                <div><?php echo smarty_function_error(array('name'=>'birthdate'),$_smarty_tpl);?>
</div>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['question']->value->name == 'googlemap_location') {?>
            <div class="ow_qs_field ow_qs_presentation_location">
                <?php echo smarty_function_input(array('name'=>'googlemap_location'),$_smarty_tpl);?>

                <?php echo smarty_function_error(array('name'=>'googlemap_location'),$_smarty_tpl);?>

            </div>
        <?php } else { ?>
            <div class="ow_qs_field ow_qs_presentation_<?php echo $_smarty_tpl->tpl_vars['question']->value->presentation;?>
">
                <span class="ow_qs_label"><?php echo smarty_function_label(array('name'=>$_smarty_tpl->tpl_vars['question']->value->name),$_smarty_tpl);
if ($_smarty_tpl->tpl_vars['question']->value->presentation == "checkbox") {?><span class="ow_qs_checkbox_fake"></span><?php }?></span>
                <span class="ow_qs_value"><?php echo smarty_function_input(array('name'=>$_smarty_tpl->tpl_vars['question']->value->name),$_smarty_tpl);?>
</span>
            </div>
        <?php }?>

    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

    
     <?php if ($_smarty_tpl->tpl_vars['form']->value->getElement('online')) {?>
        <div class="ow_qs_field ow_qs_presentation_checkbox" style="width:50%; float:left;">
            <span class="ow_qs_value"><?php echo smarty_function_input(array('name'=>'online'),$_smarty_tpl);?>
<span class="ow_qs_checkbox_fake"></span></span>
            <span class="ow_qs_label"><?php echo smarty_function_label(array('name'=>'online'),$_smarty_tpl);?>
</span>
        </div>
    <?php }?>
    
    <?php if ($_smarty_tpl->tpl_vars['form']->value->getElement('with_photo')) {?>
        <div class="ow_qs_field ow_qs_presentation_checkbox" style="width:50%; float:left;">
            <span class="ow_qs_value"><?php echo smarty_function_input(array('name'=>'with_photo'),$_smarty_tpl);?>
<span class="ow_qs_checkbox_fake"></span></span>
            <span class="ow_qs_label"><?php echo smarty_function_label(array('name'=>'with_photo'),$_smarty_tpl);?>
</span>
        </div>
    <?php }?>

    <div class="clearfix ow_stdmargin ow_qs_btn">
        <?php echo smarty_function_submit(array('name'=>'search'),$_smarty_tpl);?>


        <span class="ow_qs_label">
            <a href="<?php echo $_smarty_tpl->tpl_vars['advancedUrl']->value;?>
" class="ow_nowrap"><?php echo smarty_function_text(array('key'=>'usearch+advanced_search'),$_smarty_tpl);?>
</a>
        </span>
    </div>
    <?php $_block_repeat1=false;
echo smarty_block_form(array('name'=>'QuickSearchForm'), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

</div><?php }
}
