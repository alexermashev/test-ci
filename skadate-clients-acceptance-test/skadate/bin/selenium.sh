#!/bin/bash

unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGw;;
    *)          machine="UNKNOWN:${unameOut}"
esac

if [ ${machine} = "Mac" ]; then
    java -Dwebdriver.chrome.driver=./bin/chromedriver_mac -jar ./bin/selenium-server-standalone-3.141.59.jar -port 4444
else
    java -Dwebdriver.chrome.driver=./bin/chromedriver_linux -jar ./bin/selenium-server-standalone-3.141.59.jar -port 4444
fi
