<?php

class VIEWPROFILE_CMP_UserViewWidget extends BASE_CMP_UserViewWidget
{
    const USER_VIEW_PRESENTATION_TABS = 'tabs';

    const USER_VIEW_PRESENTATION_TABLE = 'table';

    public function __construct( BASE_CLASS_WidgetParameter $params )
    {
        parent::__construct($params);

        $isButton = true;
        $error = false;

        $status = BOL_AuthorizationService::getInstance()->getActionStatus('viewprofile', 'view_profile');

        if ( $status['status'] == BOL_AuthorizationService::STATUS_PROMOTED )
        {
            $script = '$({$link}).click(function(){
                window.OW.error({$message});
            });';

            $script = UTIL_JsGenerator::composeJsString($script, array('link' => '#btn-viewprofile', 'message' => $status['msg'] ));
            OW::getDocument()->addOnloadScript($script);

            $error = true;
        }

        if ( !$error )
        {
            $userId = $params->additionalParamList['entityId'];
            $countRequiredQuestions = VIEWPROFILE_BOL_Service::getInstance()->getCountEmptyRequiredQuestions($userId);

            if ( $countRequiredQuestions == 0 )
            {
                $isButton = false;
            }
            else
            {
                $script = '$({$link}).click(function() {
                    location.href = "?view_profile";
                });';

                $script = UTIL_JsGenerator::composeJsString($script, array('link' => '#btn-viewprofile' ));
                OW::getDocument()->addOnloadScript($script);
            }
        }

        $isActionViewProfile = OW::getUser()->isAuthorized('viewprofile', 'view_profile');

        $isViewProfile = true;

        if ( $isActionViewProfile && isset($_GET['view_profile']) )
        {
            $isViewProfile = false;
        };

        $this->assign('isViewProfile', $isViewProfile);
        $this->assign('isButton', $isButton);

        $template = OW::getPluginManager()->getPlugin('viewprofile')->getCmpViewDir() . 'user_view_widget_table.html';

        $this->setTemplate($template);
    }
}