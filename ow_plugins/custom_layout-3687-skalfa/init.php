<?php

CUSTOMLAYOUT_CLASS_EventHandler::getInstance()->init();

$plugin = OW::getPluginManager()->getPlugin('customlayout');

$key = strtoupper($plugin->getKey());

OW::getRouter()->addRoute(new OW_Route('customlayout.uninstall', 'customlayout/uninstall', 'CUSTOMLAYOUT_CTRL_Admin', 'uninstall'));