<?php
/* Smarty version 3.1.30, created on 2018-07-19 06:02:29
  from "/var/www/biyebiye/public_html/ow_system_plugins/base/views/components/profile_action_toolbar.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5061b53e8eb8_89013120',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0e0bb6398916ab14ace450f5b412f329ddf2f86d' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_system_plugins/base/views/components/profile_action_toolbar.html',
      1 => 1479204252,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b5061b53e8eb8_89013120 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="ow_profile_gallery_action_toolbar ow_profile_action_toolbar_wrap clearfix ow_stdmargin">
    <ul class="ow_bl ow_profile_action_toolbar clearfix ow_small ow_left">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['toolbar']->value, 'action');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['action']->value) {
?>
            <?php if (isset($_smarty_tpl->tpl_vars['action']->value['html'])) {?>
                <?php echo $_smarty_tpl->tpl_vars['action']->value['html'];?>

            <?php } elseif (isset($_smarty_tpl->tpl_vars['action']->value['extraLabel'])) {?>
                <li>
                    <?php echo $_smarty_tpl->tpl_vars['action']->value['extraLabel'];?>

                </li>
            <?php } else { ?>
                <li>
                    <a <?php echo $_smarty_tpl->tpl_vars['action']->value['attrs'];?>
 >
                        <?php echo $_smarty_tpl->tpl_vars['action']->value['label'];?>

                    </a>
                </li>
            <?php }?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

    </ul>

    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['groups']->value, 'group');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['group']->value) {
?>
        <?php echo $_smarty_tpl->tpl_vars['group']->value;?>

    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>


    <?php echo $_smarty_tpl->tpl_vars['cmpsMarkup']->value;?>

</div>
<?php }
}
