<?php
/* Smarty version 3.1.30, created on 2018-07-19 06:02:29
  from "/var/www/biyebiye/public_html/ow_plugins/photo/views/components/user_photo_albums_widget.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5061b54a46e0_20791376',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'dc01edfcc89a9c7950be4750833cdbc423b122a7' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_plugins/photo/views/components/user_photo_albums_widget.html',
      1 => 1479204280,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b5061b54a46e0_20791376 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_url_for_route')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.url_for_route.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
?>
<div class="clearfix ow_lp_albums">	
    <?php if ($_smarty_tpl->tpl_vars['showTitles']->value) {?>
	    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['albums']->value, 'album');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['album']->value) {
?>
            <div class="clearfix ow_smallmargin">
                <div class="ow_lp_wrapper<?php if (!empty($_smarty_tpl->tpl_vars['album']->value['class'])) {?> <?php echo $_smarty_tpl->tpl_vars['album']->value['class'];
}?>">
                    <?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'url', null, null);
?>

                        <?php echo smarty_function_url_for_route(array('for'=>"photo_user_album:[user=>".((string)$_smarty_tpl->tpl_vars['user']->value).", album=>".((string)$_smarty_tpl->tpl_vars['album']->value['dto']->id)."]"),$_smarty_tpl);?>

                    <?php $_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);
?>

		            <a href="<?php echo $_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'url');?>
">
                        <img title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['album']->value['dto']->name, ENT_QUOTES, 'UTF-8', true);?>
"  alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['album']->value['dto']->name, ENT_QUOTES, 'UTF-8', true);?>
"  src="<?php echo $_smarty_tpl->tpl_vars['album']->value['cover'];?>
" width="100" height="100" />
                    </a>
                </div>
                <div class="ow_lp_label ow_small">
                    <a href="<?php echo $_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'url');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['album']->value['dto']->name, ENT_QUOTES, 'UTF-8', true);?>
</a>
                </div>
            </div>
	    <?php
}
} else {
?>

	        <div class="ow_nocontent"><?php echo smarty_function_text(array('key'=>'photo+no_photo_found'),$_smarty_tpl);?>
</div>
	    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

    <?php } else { ?>
        <div class="ow_lp_photos ow_center">
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['albums']->value, 'album');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['album']->value) {
?>
                <?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'url', null, null);
?>

                    <?php echo smarty_function_url_for_route(array('for'=>"photo_user_album:[user=>".((string)$_smarty_tpl->tpl_vars['user']->value).", album=>".((string)$_smarty_tpl->tpl_vars['album']->value['dto']->id)."]"),$_smarty_tpl);?>

                <?php $_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);
?>

                <a class="ow_lp_wrapper<?php if (!empty($_smarty_tpl->tpl_vars['album']->value['class'])) {?> <?php echo $_smarty_tpl->tpl_vars['album']->value['class'];
}?>" href="<?php echo $_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'url');?>
">
                    <img title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['album']->value['dto']->name, ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['album']->value['dto']->name, ENT_QUOTES, 'UTF-8', true);?>
" src="<?php echo $_smarty_tpl->tpl_vars['album']->value['cover'];?>
" />
                </a>
            <?php
}
} else {
?>

                <div class="ow_nocontent"><?php echo smarty_function_text(array('key'=>'photo+no_photo_found'),$_smarty_tpl);?>
</div>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

        </div>
    <?php }?>
</div>
<?php }
}
