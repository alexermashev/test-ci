<?php
/* Smarty version 3.1.30, created on 2018-07-19 11:20:45
  from "/var/www/biyebiye/public_html/ow_system_plugins/base/views/controllers/user_forgot_password.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b50ac4d03f7e3_84896206',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3ebe52ea570f5ac856eb937b3e162c0236414d39' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_system_plugins/base/views/controllers/user_forgot_password.html',
      1 => 1479204252,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b50ac4d03f7e3_84896206 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_style')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.style.php';
if (!is_callable('smarty_block_block_decorator')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.block_decorator.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_block_form')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_input')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_submit')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.submit.php';
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('style', array());
$_block_repeat1=true;
echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>


.ow_forgot_password{
    margin:0 auto;
    padding:100px;
    width:350px;
}

<?php $_block_repeat1=false;
echo smarty_block_style(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

<div class="ow_forgot_password">
    <?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('block_decorator', array('name'=>'box','style'=>'text-align:center;','langLabel'=>'base+forgot_password_cap_label'));
$_block_repeat1=true;
echo smarty_block_block_decorator(array('name'=>'box','style'=>'text-align:center;','langLabel'=>'base+forgot_password_cap_label'), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

        <div style="padding: 0 5px 5px;"><?php echo smarty_function_text(array('key'=>'base+forgot_password_form_text'),$_smarty_tpl);?>
</div>
        <?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('name'=>'forgot-password'));
$_block_repeat2=true;
echo smarty_block_form(array('name'=>'forgot-password'), null, $_smarty_tpl, $_block_repeat2);
while ($_block_repeat2) {
ob_start();
?>

            <?php echo smarty_function_input(array('name'=>'email','class'=>'ow_smallmargin'),$_smarty_tpl);?>

            <div class="clearfix"><div class="ow_right"><?php echo smarty_function_submit(array('name'=>'submit','class'=>'ow_positive'),$_smarty_tpl);?>
</div></div>
        <?php $_block_repeat2=false;
echo smarty_block_form(array('name'=>'forgot-password'), ob_get_clean(), $_smarty_tpl, $_block_repeat2);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

    <?php $_block_repeat1=false;
echo smarty_block_block_decorator(array('name'=>'box','style'=>'text-align:center;','langLabel'=>'base+forgot_password_cap_label'), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

</div><?php }
}
