<?php

class VIEWPROFILE_BOL_Service
{
    const PLUGIN_KEY = 'viewprofile';
    const DATE_FOR_REDIRECT = 86400 * 7;
//    const DATE_FOR_REDIRECT = 20;
    const CONF_SENT_PROFILES_COUNT_ON_PAGE = 5;


    /**
     * @var VIEWPROFILE_BOL_LastVisitDao
     */
    private $lastVisitDao;

    /**
     * @var VIEWPROFILE_BOL_SendProfileDao
     */
    private $sendProfileDao;

    /**
     * Constructor.
     */
    private function __construct()
    {
        $this->lastVisitDao = VIEWPROFILE_BOL_LastVisitDao::getInstance();
        $this->sendProfileDao = VIEWPROFILE_BOL_SendProfileDao::getInstance();
    }
    /**
     * Singleton instance.
     *
     * @var VIEWPROFILE_BOL_Service
     */
    private static $classInstance;

    /**
     * Returns an instance of class (singleton pattern implementation).
     *
     * @return VIEWPROFILE_BOL_Service
     */
    public static function getInstance()
    {
        if ( self::$classInstance === null )
        {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    public static function getFreeQuestions()
    {
        return array(
            'birthdate', //Age - His/Her Age
            'field_8ef79652babd0ba70f9e3d65212b2bd7',   //Profession - Occupation/job
            'field_75a38e98722a8ea37f2efa05d1aff189',   //Ethnic origin - Ethnicity
            'aboutme',   //About Family - More information about our family
            'googlemap_location',   //Location
            'field_8fa636b32990cd65a56680bc7a3f6970',   //Information on grandparents
            'field_907acd9a94df8e1d8409049cf22d4d11',   //Family Type
            'field_0d7fd3eade5ef87619e00f0d3ee151b7',   //Family Status
            'field_62cc824e7ad9abf10a7724a5664c7baa',   //Community
            'field_12b283255a2d752ae8b60a31dc656045',   //Sub-Community
        );
    }

    /**
     * Set lastTime after visit edit profile and complete profile
     * @param null $userId
     * @param int $field
     */
    public function setLastTimeInEdit( $field = 0, $userId = null )
    {
        if ( empty($userId) )
        {
            $userId = OW::getUser()->getId();
        }

        if ( empty($userId) )
        {
            return;
        }

        /**
         * @var VIEWPROFILE_BOL_LastVisit $lastTime
         */
        $lastTime = $this->lastVisitDao->findByUserId($userId);

        if ( empty($lastTime) )
        {
            $lastTime = new VIEWPROFILE_BOL_LastVisit();
            $lastTime->userId = $userId;
        }

        switch ( $field )
        {
            case 0:
                $lastTime->dateTime = time();
                $lastTime->completeTime = time();
                break;
            case 1:
                $lastTime->dateTime = time();
                break;
            case 2:
                $lastTime->completeTime = time();
                break;
        }

        if ( empty($lastTime->dateTime) )
        {
            $lastTime->dateTime = time();
        }

        $this->saveLastTime($lastTime);
    }

    /**
     * @param VIEWPROFILE_BOL_LastVisit $lastTime
     */
    public function saveLastTime( VIEWPROFILE_BOL_LastVisit $lastTime )
    {
        $this->lastVisitDao->save($lastTime);
    }

    /**
     * @param VIEWPROFILE_BOL_SendProfile $sendProfile
     * @param bool $isForm
     * @return VIEWPROFILE_BOL_SendProfile
     */
    public function sendProfile( VIEWPROFILE_BOL_SendProfile $sendProfile, $isForm = false )
    {
        $this->sendProfileDao->save($sendProfile);

        if ( $isForm )
        {
            $this->sendPushNotification($sendProfile);
            $this->sendMail($sendProfile);
        }

        return $sendProfile;
    }

    public function sendPushNotification( VIEWPROFILE_BOL_SendProfile $sendProfile )
    {
        if ( !OW::getPluginManager()->isPluginActive('skmobileapp') ) return;

        $userId = $sendProfile->userId;
        $senderId = $sendProfile->whoseId;

        $pushMessage = new SKMOBILEAPP_BOL_PushMessage;
        $pushMessage->setMessageType('sendProfile');
        $pushMessage->setLanguagePrefix('viewprofile');

        $pushMessage->sendNotification($userId, 'pn_new_send_profile_title', 'pn_new_send_profile_text');
    }

    public function sendMail( VIEWPROFILE_BOL_SendProfile $sendProfile )
    {
        $languages = OW::getLanguage();

        $senderId = $sendProfile->whoseId;
        $subject = $languages->text('viewprofile', 'mail_subject');
        $html = $languages->text('viewprofile', 'mail_html');
        $text = $languages->text('viewprofile', 'mail_text');

        try
        {
            $mail = OW::getMailer()->createMail();
            $mail->addRecipientEmail($sendProfile->email);
            $mail->setSubject($subject);
            $mail->setHtmlContent($html);
            $mail->setTextContent($text);
            OW::getMailer()->send($mail);
        }
        catch ( Exception $ex )
        {
            OW::getLogger()->addEntry(json_encode($ex));
        }
    }

    /**
     * @param $userId
     * @return VIEWPROFILE_BOL_LastVisit|void
     */
    public function findLastVisitByUserId( $userId )
    {
        $data = $this->lastVisitDao->findByUserId($userId);

        if ( empty($data->dateTime) )
        {
            $this->setLastTimeInEdit(0, $userId);

            return $this->lastVisitDao->findByUserId($userId);
        }

        return $data;
    }

    /**
     * @param $hash
     * @return VIEWPROFILE_BOL_SendProfile|void
     */
    public function findSendProfileByHash( $hash )
    {
        return $this->sendProfileDao->findByHash($hash);
    }

    /**
     * @param $userId
     * @return VIEWPROFILE_BOL_SendProfile|void
     */
    public function findSendProfileByUserId( $userId )
    {
        return $this->sendProfileDao->findByUserId($userId);
    }

    /**
     * @param $id
     * @return OW_Entity
     */
    public function findSendProfileById( $id )
    {
        return $this->sendProfileDao->findById($id);
    }

    /**
     * @param $whoseId
     * @return VIEWPROFILE_BOL_SendProfile|void
     */
    public function findSendProfileByWhoseId( $whoseId )
    {
        return $this->sendProfileDao->findByWhoseId($whoseId);
    }

    /**
     * Returns sent profiles list.
     *
     * @param $userId
     * @param $page
     * @return array
     */
    public function findMySentProfiles( $userId, $page )
    {
        $page = ( $page === null ) ? 1 : (int) $page;
        $count = self::CONF_SENT_PROFILES_COUNT_ON_PAGE;
        $first = ( $page - 1 ) * $count;

        return $this->sendProfileDao->findMySentProfiles($userId, $first, $count);
    }

    /**
     * Returns all sent profiles list.
     *
     * @param $userId
     * @return array
     */
    public function findMyAllSentProfiles( $userId )
    {
        return $this->sendProfileDao->findMyAllSentProfiles($userId);
    }

    /**
     * Returns sent profiles count.
     *
     * @param $userId
     * @return mixed
     */
    public function findMySentProfilesCount( $userId )
    {
        return $this->sendProfileDao->findMySentProfilesCount($userId);
    }

    public function getLabelStatuses()
    {
        $language = OW::getLanguage();

        return array(
            0 => $language->text(VIEWPROFILE_BOL_Service::PLUGIN_KEY, 'label_status_pending'),
            1 => $language->text(VIEWPROFILE_BOL_Service::PLUGIN_KEY, 'label_status_yes'),
            2 => $language->text(VIEWPROFILE_BOL_Service::PLUGIN_KEY, 'label_status_no'),
            3 => $language->text(VIEWPROFILE_BOL_Service::PLUGIN_KEY, 'label_status_maybe')
        );
    }

    public function getListingData( array $data )
    {
        $resultArray = array();

        if ( empty($data) || count($data) == 0 )
        {
            return $resultArray;
        }

        $userService = BOL_UserService::getInstance();

        /* @var $item VIEWPROFILE_BOL_SendProfile */
        foreach ( $data as $item )
        {
            $idArray[] = $item->whoseId;
        }

        $userNames = $userService->getDisplayNamesForList($idArray);
        $urls = $userService->getUserUrlsForList($idArray);
        $avatarUrls = BOL_AvatarService::getInstance()->getAvatarsUrlList($idArray);
        $statuses = $this->getLabelStatuses();

        /* @var $item VIEWPROFILE_BOL_SendProfile */
        foreach ( $data as $item )
        {
            $resultArray[$item->getId()] = array(
                'content' => strip_tags($item->comment),
                'title' => $userNames[$item->whoseId],
                'whoseUrl' => $urls[$item->whoseId],
                'imageSrc' => $avatarUrls[$item->whoseId],
                'imageTitle' => $userNames[$item->whoseId]
            );

            $resultArray[$item->getId()]['toolbar'][] = array('label' => 'Email: ' . $item->email);
            $resultArray[$item->getId()]['toolbar'][] = array('label' => 'Status: ' . $statuses[$item->status]);
            $resultArray[$item->getId()]['toolbar'][] = array(
                'label' => UTIL_DateTime::formatSimpleDate($item->createdDatetime,$item->createdDatetime),
                'class' => 'ow_ipc_date'
            );
        }

        return $resultArray;
    }

    public function getFormatData( array $data )
    {
        $resultArray = array();
        $userService = BOL_UserService::getInstance();
        $idArray = [];

        /* @var $item VIEWPROFILE_BOL_SendProfile */
        foreach ( $data as $item )
        {
            $idArray[] = $item->whoseId;
        }

        $displayNames = $userService->getDisplayNamesForList($idArray);
        $userNames = $userService->getUserNamesForList($idArray);
        $statuses = $this->getLabelStatuses();

        $avatarList = BOL_AvatarService::getInstance()->findByUserIdList($idArray);
        $processedMatchList = [];

        foreach($avatarList as $avatar) {
            $processedMatchList[$avatar->userId] = SKMOBILEAPP_BOL_Service::getInstance()->getAvatarData($avatar, true);
        }

        /* @var $item VIEWPROFILE_BOL_SendProfile */
        foreach ( $data as $item )
        {
            $resultArray[] = array(
                'id' => (int) $item->whoseId,
                'status' => $statuses[$item->status],
                'email' => $item->email,
                'message' => strip_tags($item->comment),
                'displayName' => $displayNames[$item->whoseId],
                'whoseName' => $userNames[$item->whoseId],
                'avatar' => $processedMatchList[$item->whoseId],
                'truncating' => (bool) (strlen(strip_tags($item->comment)) > 100),
                'createdDatetime' => UTIL_DateTime::formatSimpleDate($item->createdDatetime,$item->createdDatetime)
            );
        }

        return $resultArray;
    }

    public function sendToEmail( VIEWPROFILE_BOL_SendProfile $sendData )
    {
        //sending email
        $cmp = new VIEWPROFILE_CMP_Notification($sendData);
        $cmp->sendNotification();
    }

    /**
     * @return string
     */
    public function generateHash()
    {
        return md5(uniqid());
    }

    public function getLogoImage()
    {
        $dto = BOL_ThemeService::getInstance()->findThemeByKey(OW::getConfig()->getValue('base', 'selectedTheme'));
        $controls = BOL_ThemeService::getInstance()->findThemeControls($dto->getId());
        $logoImageControl = null;

        foreach ( $controls as $control )
        {
            if ( $control['key'] == 'logoImage' )
            {
                $logoImageControl = $control;
            }
        }

        if ( $logoImageControl['value'] == null )
        {
            $resultString = substr($logoImageControl['defaultValue'], (strpos($logoImageControl['defaultValue'], 'images/') + 7));
            return 'url(' . OW::getThemeManager()->getSelectedTheme()->getStaticImagesUrl(false) . substr($resultString, 0, strpos($resultString, ')')) . ')';
        }

        $resultString = substr($logoImageControl['value'], (strpos($logoImageControl['value'], 'images/') + 4));
        $resultString = substr($resultString, 0, strpos($resultString, ')'));

        return $resultString;
    }

    public function checkComplete( $userId )
    {
        $lastTime = $this->findLastVisitByUserId($userId);
        $questionsEditStamp = OW::getConfig()->getValue('base', 'profile_question_edit_stamp');

        if ( time() - $lastTime->dateTime >= self::DATE_FOR_REDIRECT && $questionsEditStamp >= (int) $lastTime->completeTime )
        {
            $this->setLastTimeInEdit(2, $userId);
        }
    }

    public function saveOpinion( $data )
    {
        /**
         * @var VIEWPROFILE_BOL_SendProfile|void
         */
        $sentProfile = self::findSendProfileByHash($data['hash']);

        if ( $sentProfile )
        {
            $sentProfile->status = $data['status'];
            $sentProfile->comment = $data['opinion'];

            $this->sendProfile($sentProfile, true);

            OW::getFeedback()->info(OW::getLanguage()->text(self::PLUGIN_KEY, 'notification_success_save_opinion'));
            OW::getApplication()->redirect(OW::getRouter()->urlForRoute(self::PLUGIN_KEY . '_main_opinion'), OW::CONTEXT_DESKTOP);
        }

        OW::getFeedback()->error(OW::getLanguage()->text(self::PLUGIN_KEY, 'not_found_sent_profile'));
        OW::getApplication()->redirect(OW::getRouter()->urlForRoute(self::PLUGIN_KEY . '_main_opinion'), OW::CONTEXT_DESKTOP);
    }

    public function getCountEmptyRequiredQuestions( $userId )
    {
        $questionService = BOL_QuestionService::getInstance();

        $user = BOL_UserService::getInstance()->findUserById($userId);

        if ( empty($user) )
        {
            return 101;
        }

        $accountType = $questionService->findAccountTypeByName($user->accountType);

        if ( empty($accountType) )
        {
            return 102;
        }

        $questionsList = $questionService->findRequiredQuestionsForAccountType($user->accountType);

        if ( empty($questionsList) )
        {
            return 103;
        }

        $questionNameList = array();

        foreach ( $questionsList as $question )
        {
            $questionNameList[$question['name']] = $question['name'];
        }

        $questionDtoList = $questionService->findQuestionByNameList($questionNameList);
        $data = $questionService->getQuestionData( array($userId), $questionNameList );

        $count = 0;
        $countWithFree = 0;

        $freeQuestions = self::getFreeQuestions();

        foreach ( $questionsList as $question )
        {
            if ( in_array($question['name'], $freeQuestions) )
            {
                $countWithFree++;
            }
            else
            {
                /*@var $questionDto BOL_Question */
                $questionDto = $questionDtoList[$question['name']];

                $value = $questionService->prepareFieldValue($questionDto->presentation, empty($data[$userId][$question['name']]) ? null : $data[$userId][$question['name']] );

                if ( !empty($value) )
                {
                    $count++;
                }
            }
        }

        return $count;
    }
}