<?php
/* Smarty version 3.1.30, created on 2019-01-18 01:23:36
  from "/Users/esase/Sites/8418/ow_system_plugins/base/decorators/tooltip.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c4170e8816299_43675137',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ff5e86790603402bf8c134706ede29e28d402f82' => 
    array (
      0 => '/Users/esase/Sites/8418/ow_system_plugins/base/decorators/tooltip.html',
      1 => 1547792051,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c4170e8816299_43675137 (Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="ow_tooltip <?php if (!empty($_smarty_tpl->tpl_vars['data']->value['addClass'])) {?> <?php echo $_smarty_tpl->tpl_vars['data']->value['addClass'];
}?>">
    <div class="ow_tooltip_tail">
        <span></span>
    </div>
    <div class="ow_tooltip_body">
        <?php echo $_smarty_tpl->tpl_vars['data']->value['content'];?>

    </div>
</div><?php }
}
