<?php

class VIEWPROFILE_BOL_SendProfileDao extends OW_BaseDao
{
    const USER_ID = 'userId';
    const WHOSE_ID = 'whoseId';
    const EMAIL = 'email';
    const STATUS = 'status';
    const COMMENT = 'comment';
    const HASH = 'hash';
    const CREATED_DATETIME = 'createdDatetime';
    const PAGE_LIMIT = 1000;

    /**
     * Singleton instance.
     *
     * @var VIEWPROFILE_BOL_SendProfileDao
     */
    private static $classInstance;

    /**
     * Returns an instance of class (singleton pattern implementation).
     *
     * @return VIEWPROFILE_BOL_SendProfileDao
     */
    public static function getInstance()
    {
        if (self::$classInstance === null) {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    /**
     * @see OW_BaseDao::getDtoClassName()
     *
     */
    public function getDtoClassName()
    {
        return 'VIEWPROFILE_BOL_SendProfile';
    }

    /**
     * @see OW_BaseDao::getTableName()
     *
     */
    public function getTableName()
    {
        return OW_DB_PREFIX . 'viewprofile_send_profile';
    }

    /**
     * Find by userId
     *
     * @param $userId
     * @return array|void
     */
    public function findByUserId( $userId )
    {
        if (!$userId) {
            return;
        }

        $example = new OW_Example();
        $example->andFieldEqual(self::USER_ID, $userId);

        return $this->findListByExample($example);
    }

    /**
     * Find by hash
     *
     * @param $hash
     * @return array|void
     */
    public function findByHash( $hash )
    {
        if (!$hash) {
            return;
        }

        $example = new OW_Example();
        $example->andFieldEqual(self::HASH, $hash);

        return $this->findObjectByExample($example);
    }

    /**
     * Find by whoseId
     *
     * @param $whoseId
     * @return array|void
     */
    public function findByWhoseId( $whoseId )
    {
        if (!$whoseId) {
            return;
        }

        $example = new OW_Example();
        $example->andFieldEqual(self::WHOSE_ID, $whoseId);

        return $this->findListByExample($example);
    }

    /**
     * @param $params
     * @return array
     */
    public function findByParams( $params )
    {
        $example = new OW_Example();

        if ( isset($params['userId']) )
        {
            $example->andFieldEqual(self::USER_ID, $params['userId']);
        }

        if ( isset($params['whoseId']) )
        {
            $example->andFieldEqual(self::WHOSE_ID, $params['whoseId']);
        }

        if ( isset($params['email']) )
        {
            $example->andFieldEqual(self::EMAIL, $params['email']);
        }

        return $this->findListByExample($example);
    }

    /**
     * Returns sent profiles list.
     *
     * @param integer $userId
     * @param integer $first
     * @param integer $count
     * @return array
     */
    public function findMySentProfiles( $userId, $first = -1, $count = 0 )
    {
        $params = array('userId' => $userId, 'first' => (int) $first, 'count' => (int) $count);

        $where = " `" . self::USER_ID . "` = :userId ";

        $query = "SELECT * FROM `" . $this->getTableName() . "` WHERE " . $where . "
            ORDER BY `" . self::CREATED_DATETIME . "` DESC LIMIT :first, :count";

        return $this->dbo->queryForObjectList($query, $this->getDtoClassName(), $params);
    }

    /**
     * Returns all sent profiles list.
     *
     * @param integer $userId
     * @return array
     */
    public function findMyAllSentProfiles( $userId )
    {
        $params = array('userId' => $userId);

        $where = " `" . self::USER_ID . "` = :userId ";

        $query = "SELECT * FROM `" . $this->getTableName() . "` WHERE " . $where . "
            ORDER BY `" . self::CREATED_DATETIME . "` DESC LIMIT " . self::PAGE_LIMIT;

        return $this->dbo->queryForObjectList($query, $this->getDtoClassName(), $params);
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function findMySentProfilesCount( $userId )
    {
        $where = " `" . self::USER_ID . "` = :userId ";

        $query = "SELECT COUNT(*) FROM `" . $this->getTableName() . "` WHERE " . $where;

        return $this->dbo->queryForColumn($query, array('userId' => $userId));
    }
}