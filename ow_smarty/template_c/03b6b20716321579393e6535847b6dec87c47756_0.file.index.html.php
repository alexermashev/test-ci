<?php
/* Smarty version 3.1.30, created on 2018-07-19 04:48:19
  from "/var/www/biyebiye/public_html/ow_plugins/hotlist/views/components/index.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b505053bdc599_67380899',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '03b6b20716321579393e6535847b6dec87c47756' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_plugins/hotlist/views/components/index.html',
      1 => 1479204282,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b505053bdc599_67380899 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_script')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.script.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
?>

<?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('script', array());
$_block_repeat1=true;
echo smarty_block_script(array(), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

<?php if ($_smarty_tpl->tpl_vars['authorized']->value || $_smarty_tpl->tpl_vars['userInList']->value) {?>
    

    $("#add_to_list").click(function(){
        hotListFloatBox = OW.ajaxFloatBox("HOTLIST_CMP_Floatbox", {} , {width:380, iconClass: "ow_ic_heart", title: "<?php if ($_smarty_tpl->tpl_vars['userInList']->value) {
echo smarty_function_text(array('key'=>"hotlist+floatbox_header_remove_from_list"),$_smarty_tpl);
} else {
echo smarty_function_text(array('key'=>"hotlist+floatbox_header"),$_smarty_tpl);
}?>"});
    });

    

<?php } else { ?>
    

    $("#add_to_list").click(function(){
    OW.authorizationLimitedFloatbox(<?php echo $_smarty_tpl->tpl_vars['authMsg']->value;?>
);
    });

    
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['userInList']->value) {?>
    
        $(".hotlist").hover(
        function () {
            $('.hotlist_footer').css('visibility', 'visible');
        },
        function () {
            $('.hotlist_footer').css('visibility', 'hidden');
        }
        );
    
<?php }
$_block_repeat1=false;
echo smarty_block_script(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>


<div class="hotlist ow_center" style="height: auto;">
<?php if ($_smarty_tpl->tpl_vars['userList']->value) {?>
	<div class="users_slideshow ow_std_margin ow_automargin clearfix" style="height: 100%; width: 100%;">
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['userList']->value, 'user', false, NULL, 'userList', array (
  'first' => true,
  'iteration' => true,
  'last' => true,
  'index' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['user']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['index']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['first'] = !$_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['index'];
$_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['iteration'] == $_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['total'];
?>
		<?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['first']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['first'] : null)) {?><div class="hot_list_normal_item ow_lp_avatars" style="display: <?php if ($_smarty_tpl->tpl_vars['count']->value > $_smarty_tpl->tpl_vars['number_of_users']->value) {?>none<?php } else { ?>block<?php }?>;"><?php }?>
			<div class="ow_avatar">
                <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['isMarked'])) {?><div class="ow_ic_bookmark ow_bookmark_icon"></div><?php }?>
                <?php $_smarty_tpl->_assignInScope('title', ((string)$_smarty_tpl->tpl_vars['user']->value['displayName'])."<br/>".((string)$_smarty_tpl->tpl_vars['user']->value['sex']).", ".((string)$_smarty_tpl->tpl_vars['user']->value['age'])."<br/>".((string)$_smarty_tpl->tpl_vars['user']->value['googlemap_location']));
?>
                <a class="ow_item_set<?php echo $_smarty_tpl->tpl_vars['number_of_users']->value;?>
" href="<?php echo $_smarty_tpl->tpl_vars['user']->value['url'];?>
" target="_blank"><img title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8', true);?>
" src="<?php echo $_smarty_tpl->tpl_vars['user']->value['avatarUrl'];?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user']->value['displayName'], ENT_QUOTES, 'UTF-8', true);?>
" style="max-width: 100%" /></a>
            </div>
		<?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['iteration'] : null)%($_smarty_tpl->tpl_vars['number_of_users']->value*$_smarty_tpl->tpl_vars['number_of_rows']->value) == 0 && !(isset($_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['last'] : null)) {?></div><div class="hot_list_normal_item ow_lp_avatars" style="display: none;"><?php }?>
		<?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_userList']->value['last'] : null)) {?></div><?php }?>
	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

	</div>
<?php } else { ?>
    <div class="ow_nocontent"><?php echo smarty_function_text(array('key'=>"hotlist+label_no_users"),$_smarty_tpl);?>
</div>
<?php }?>
    <div class="ow_center hotlist_footer" <?php if ($_smarty_tpl->tpl_vars['userInList']->value) {?>style="visibility: hidden;"<?php }?>><a href="javascript://" id="add_to_list"><?php if ($_smarty_tpl->tpl_vars['userInList']->value) {
echo smarty_function_text(array('key'=>"hotlist+remove_from_hot_list"),$_smarty_tpl);
} else {
echo smarty_function_text(array('key'=>"hotlist+are_you_hot_too"),$_smarty_tpl);
}?></a></div>
</div>
<?php }
}
