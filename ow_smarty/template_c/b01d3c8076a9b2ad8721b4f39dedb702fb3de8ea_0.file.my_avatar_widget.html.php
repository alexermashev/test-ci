<?php
/* Smarty version 3.1.30, created on 2019-01-18 01:23:38
  from "/Users/esase/Sites/8418/ow_system_plugins/base/views/components/my_avatar_widget.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c4170ea0dd116_71402561',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b01d3c8076a9b2ad8721b4f39dedb702fb3de8ea' => 
    array (
      0 => '/Users/esase/Sites/8418/ow_system_plugins/base/views/components/my_avatar_widget.html',
      1 => 1547792051,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c4170ea0dd116_71402561 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_decorator')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.decorator.php';
?>

<div class="ow_my_avatar_widget clearfix">
	<div class="ow_left ow_my_avatar_img"><?php echo smarty_function_decorator(array('name'=>'avatar_item','data'=>$_smarty_tpl->tpl_vars['avatar']->value),$_smarty_tpl);?>
</div>
    <div class="ow_my_avatar_cont">
    	<a href="<?php echo $_smarty_tpl->tpl_vars['avatar']->value['url'];?>
" class="ow_my_avatar_username"><span><?php echo $_smarty_tpl->tpl_vars['avatar']->value['title'];?>
</span></a>
    </div>
</div><?php }
}
