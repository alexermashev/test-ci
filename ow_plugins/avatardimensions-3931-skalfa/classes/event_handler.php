<?php

class AVATARDIMENSIONS_CLASS_EventHandler
{
    /**
     * Class instance
     *
     * @var AVATARDIMENSIONS_CLASS_EventHandler
     */
    private static $classInstance;

    /**
     * Class constructor
     */
    private function __construct()
    {

    }

    /**
     * Get instance
     *
     * @return AVATARDIMENSIONS_CLASS_EventHandler
     */
    public static function getInstance()
    {
        if (self::$classInstance === null) {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    public function init()
    {
        OW::getEventManager()->bind(OW_EventManager::ON_PLUGINS_INIT, [$this, 'afterInit']);
    }

    public function afterInit()
    {
        $em = OW::getEventManager();

        $em->bind('class.get_instance.BASE_CMP_AvatarChange', array($this, 'onAvatarChange'));
        $em->bind('class.get_instance.BASE_CTRL_Avatar', array($this, 'onAvatar'));
        $em->bind('class.get_instance.SKADATE_CTRL_Join', array($this, 'onJoin'));
        $em->bind('class.get_instance.BASE_CTRL_Join', array($this, 'onJoin'));
        $em->bind(OW_EventManager::ON_BEFORE_PLUGIN_UNINSTALL, array($this, 'beforePluginDeactivate'));
        $em->bind(OW_EventManager::ON_BEFORE_DOCUMENT_RENDER, [$this, 'onBeforeDocumentRender']);
    }

    public function onAvatarChange( OW_Event $event )
    {
        $params = $event->getParams();

        $event->setData(OW::getClassInstanceArray('AVATARDIMENSIONS_CMP_AvatarChange', $params['arguments']));
    }

    public function onAvatar( OW_Event $event )
    {
        $params = $event->getParams();

        $event->setData(OW::getClassInstanceArray('AVATARDIMENSIONS_CTRL_Avatar', $params['arguments']));
    }

    public function onJoin( OW_Event $event )
    {
        $params = $event->getParams();

        $event->setData(OW::getClassInstanceArray('AVATARDIMENSIONS_CTRL_Join', $params['arguments']));
    }

    public function beforePluginDeactivate(OW_Event $e)
    {
        $params = $e->getParams();

        if ($params["pluginKey"] == AVATARDIMENSIONS_BOL_Service::PLUGIN_KEY) {
            OW::getFeedback()->warning(OW::getLanguage()->text(AVATARDIMENSIONS_BOL_Service::PLUGIN_KEY, "deactivate_blocked"));
            throw new RedirectException(OW::getRouter()->urlForRoute("admin_plugins_installed"));
        }
    }

    public function onBeforeDocumentRender()
    {
        $document = OW::getDocument();

        $document->addStyleDeclaration("
            .ow_profile_gallery_avatar_image {
                background-size: contain;
            }
        ");
    }
}