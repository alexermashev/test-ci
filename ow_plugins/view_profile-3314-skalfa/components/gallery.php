<?php

/**
 * Copyright (c) 2016, Skalfa LLC
 * All rights reserved.
 * 
 * ATTENTION: This commercial software is intended for exclusive use with SkaDate Dating Software (http://www.skadate.com)
 * and is licensed under SkaDate Exclusive License by Skalfa LLC.
 * 
 * Full text of this license can be found at http://www.skadate.com/sel.pdf
 */

class VIEWPROFILE_CMP_Gallery extends PCGALLERY_CMP_Gallery
{
    public function __construct( $userId )
    {
        parent::__construct($userId);

        $template = OW::getPluginManager()->getPlugin(VIEWPROFILE_BOL_Service::PLUGIN_KEY)->getCmpViewDir() . 'gallery.html';
        $this->setTemplate($template);
    }

    public function getPhotos()
    {
        $source = BOL_PreferenceService::getInstance()->getPreferenceValue("pcgallery_source", $this->userId);
        $album = BOL_PreferenceService::getInstance()->getPreferenceValue("pcgallery_album", $this->userId);

        if ( $source == "album" )
        {
            $photos = self::getAlbumPhotoList($this->userId, $album, array(0, self::PHOTO_LIMIT));
        }
        else
        {
            $photos = self::getPhotoList($this->userId, array(0, self::PHOTO_LIMIT));
        }

        if ( count($photos) < self::PHOTO_COUNT )
        {
            return array();
        }

        return $photos;
    }

    public function getPhotoList( $userId, $limit = null )
    {
        $params = array(
            'userId' => $userId,
            "entityType" => "user",
            "entityId" => $userId,
            "privacy" => null
        );

        if ( $limit !== null )
        {
            $params['offset'] = $limit[0];
            $params['limit'] = $limit[1];
        }

        $photos = OW::getEventManager()->call("photo.entity_photos_find", $params);

        if ( empty($photos) )
        {
            return array();
        }

        $out = array();
        foreach ( $photos as $photoId => $photo )
        {
            $out[] = array(
                'id' => $photoId,
                'src' => $photo['smallUrl']
            );
        }

        return $out;
    }

    public function getAlbumPhotoList( $userId, $albumId, $limit = null )
    {
        $params = array(
            'albumId' => $albumId,
            'userId' => $userId,
            "privacy" => null
        );

        if ( $limit !== null )
        {
            $params['offset'] = $limit[0];
            $params['limit'] = $limit[1];
        }

        $photos = OW::getEventManager()->call('photo.album_photos_find', $params);

        if ( empty($photos) )
        {
            return array();
        }

        $out = array();
        foreach ( $photos as $photo )
        {
            $out[] = array(
                'id' => $photo["id"],
                'src' => $photo['smallUrl']
            );
        }

        return $out;
    }
}