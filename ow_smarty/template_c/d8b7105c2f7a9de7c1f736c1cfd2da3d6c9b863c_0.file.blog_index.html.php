<?php
/* Smarty version 3.1.30, created on 2018-07-19 08:07:48
  from "/var/www/biyebiye/public_html/ow_plugins/blogs/views/controllers/blog_index.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b507f143ba126_96989649',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd8b7105c2f7a9de7c1f736c1cfd2da3d6c9b863c' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_plugins/blogs/views/controllers/blog_index.html',
      1 => 1479204280,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b507f143ba126_96989649 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_add_content')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.add_content.php';
if (!is_callable('smarty_block_style')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.style.php';
if (!is_callable('smarty_function_decorator')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.decorator.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
?>
<div class="clearfix"><?php echo smarty_function_add_content(array('key'=>'blogs.add_content.list.top','listType'=>$_smarty_tpl->tpl_vars['listType']->value),$_smarty_tpl);?>
</div>

<?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('style', array());
$_block_repeat1=true;
echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

.ow_wrap_normal{
    white-space: normal;
}
<?php $_block_repeat1=false;
echo smarty_block_style(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>


<?php if ($_smarty_tpl->tpl_vars['addNew_isAuthorized']->value) {?>
    <?php if ($_smarty_tpl->tpl_vars['addNew_promoted']->value) {?>
        <div class="ow_right"><?php echo smarty_function_decorator(array('name'=>'button','class'=>'ow_ic_add','id'=>'btn-add-new-post','langLabel'=>'blogs+add_new'),$_smarty_tpl);?>
</div>
    <?php } else { ?>
        <div class="ow_right"><?php echo smarty_function_decorator(array('name'=>'button','class'=>'ow_ic_add','id'=>'btn-add-new-post','langLabel'=>'blogs+add_new','onclick'=>"location.href='".((string)$_smarty_tpl->tpl_vars['url_new_post']->value)."'"),$_smarty_tpl);?>
</div>
    <?php }
}?>
	<?php echo $_smarty_tpl->tpl_vars['menu']->value;?>

      <div class="ow_blogs_list clearfix">

         <div class="ow_superwide" style="float:left;">

			<?php if ($_smarty_tpl->tpl_vars['isBrowseByTagCase']->value) {?>       
				<?php if ($_smarty_tpl->tpl_vars['tag']->value) {?>
	         	<div class="ow_anno ow_stdmargin ow_center ow_ic_warning">
					<?php echo smarty_function_text(array('key'=>"blogs+results_by_tag",'tag'=>$_smarty_tpl->tpl_vars['tag']->value),$_smarty_tpl);?>

				</div>
				<?php } else { ?>
					<?php echo $_smarty_tpl->tpl_vars['tagCloud']->value;?>
				
				<?php }?>
         	<?php }?>
                
            <?php if ($_smarty_tpl->tpl_vars['showList']->value) {?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['list']->value, 'post');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
?>
            
				<?php $_smarty_tpl->_assignInScope('dto', $_smarty_tpl->tpl_vars['post']->value['dto']);
?>

				

				<?php $_smarty_tpl->_assignInScope('userId', $_smarty_tpl->tpl_vars['dto']->value->getAuthorId());
?>

				<?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'default', 'info_string', null);
?>

					<a href="<?php echo $_smarty_tpl->tpl_vars['post']->value['url'];?>
"><?php echo $_smarty_tpl->tpl_vars['dto']->value->getTitle();?>
</a>
				<?php $_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);
?>


				<?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'default', 'content', null);
?>

					<?php echo $_smarty_tpl->tpl_vars['post']->value['text'];
if ($_smarty_tpl->tpl_vars['post']->value['showMore']) {?>... <a class="ow_lbutton" href="<?php echo $_smarty_tpl->tpl_vars['post']->value['url'];?>
"><?php echo smarty_function_text(array('key'=>'blogs+more'),$_smarty_tpl);?>
</a><?php }?>
				<?php $_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);
?>

				<?php $_smarty_tpl->_assignInScope('id', $_smarty_tpl->tpl_vars['dto']->value->getId());
?>

				

            	<?php echo smarty_function_decorator(array('name'=>'ipc','infoString'=>$_smarty_tpl->tpl_vars['info_string']->value,'addClass'=>"ow_stdmargin",'content'=>$_smarty_tpl->tpl_vars['content']->value,'toolbar'=>$_smarty_tpl->tpl_vars['toolbars']->value[$_smarty_tpl->tpl_vars['id']->value],'avatar'=>$_smarty_tpl->tpl_vars['avatars']->value[$_smarty_tpl->tpl_vars['userId']->value]),$_smarty_tpl);?>

            <?php
}
} else {
?>

            	<?php echo smarty_function_text(array('key'=>'base+empty_list'),$_smarty_tpl);?>

            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>


            <?php if ($_smarty_tpl->tpl_vars['paging']->value) {?><center><?php echo $_smarty_tpl->tpl_vars['paging']->value;?>
</center><?php }?>
            <?php }?>
        </div>    

         <div class="ow_supernarrow" style="float:right;">
         	<?php echo $_smarty_tpl->tpl_vars['tagSearch']->value;?>

         	<?php if (count($_smarty_tpl->tpl_vars['list']->value) > 0) {?>
	         	<?php echo $_smarty_tpl->tpl_vars['tagCloud']->value;?>

         	<?php }?>
         </div>

      </div><?php }
}
