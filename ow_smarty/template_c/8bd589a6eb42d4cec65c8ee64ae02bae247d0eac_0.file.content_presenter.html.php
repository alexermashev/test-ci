<?php
/* Smarty version 3.1.30, created on 2018-07-19 06:55:09
  from "/var/www/biyebiye/public_html/ow_system_plugins/base/views/components/content_presenter.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b506e0d42cd55_66037245',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8bd589a6eb42d4cec65c8ee64ae02bae247d0eac' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_system_plugins/base/views/components/content_presenter.html',
      1 => 1479204252,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b506e0d42cd55_66037245 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_more')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/modifier.more.php';
?>
<div id="<?php echo $_smarty_tpl->tpl_vars['uniqId']->value;?>
">
<?php if (!empty($_smarty_tpl->tpl_vars['data']->value['text'])) {?>
    <div class="ow_newsfeed_body_status ow_smallmargin"><?php echo smarty_modifier_more($_smarty_tpl->tpl_vars['data']->value['text'],150);?>
</div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['displayFormat']->value == "video") {?>


<div class="clearfix ow_newsfeed_oembed_atch">
    <div class="ow_newsfeed_item_picture">
        <a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['url'];?>
" data-action="play">
            <img src="<?php echo $_smarty_tpl->tpl_vars['data']->value['image']['thumbnail'];?>
" style="max-width: 100%;">
            <div class="ow_oembed_video_cover"></div>
        </a>
    </div>
    <div class="ow_newsfeed_item_content">
        <a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['url'];?>
" class="ow_newsfeed_item_title">
            <?php echo $_smarty_tpl->tpl_vars['data']->value['title'];?>

        </a>
        
        <div class="ow_remark ow_smallmargin">
            <?php echo smarty_modifier_more($_smarty_tpl->tpl_vars['data']->value['description'],150);?>

        </div>
    </div>
</div>


<?php } elseif ($_smarty_tpl->tpl_vars['displayFormat']->value == "image_content") {?>


<div class="ow_newsfeed_oembed_atch clearfix">
    <div class="ow_newsfeed_item_picture">
        <a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['url'];?>
">
            <img src="<?php echo $_smarty_tpl->tpl_vars['data']->value['image']['thumbnail'];?>
" style="max-width: 100%;" />
        </a>
    </div>
    <div class="ow_newsfeed_item_content">
        
        <a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['url'];?>
" class="ow_newsfeed_item_title">
            <?php echo $_smarty_tpl->tpl_vars['data']->value['title'];?>

        </a>
        
        <div class="ow_remark ow_smallmargin">
            <?php echo smarty_modifier_more($_smarty_tpl->tpl_vars['data']->value['description'],150);?>

        </div>
    </div>
</div>


<?php } elseif ($_smarty_tpl->tpl_vars['displayFormat']->value == "image") {?>


<div class="ow_newsfeed_large_image clearfix">
    <div class="ow_newsfeed_item_picture">
        <a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['image']['preview'];?>
" data-src="<?php echo $_smarty_tpl->tpl_vars['data']->value['image']['view'];?>
" onclick="OW.showImageInFloatBox($(this).data('src')); return false;">
            <img style="max-width: 100%;" src="<?php echo $_smarty_tpl->tpl_vars['data']->value['image']['preview'];?>
" />
        </a>
    </div>
</div>


<?php } elseif ($_smarty_tpl->tpl_vars['displayFormat']->value == "content") {?>


<div class="ow_newsfeed_item_content">
    <a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['url'];?>
" class="ow_newsfeed_item_title">
        <?php echo $_smarty_tpl->tpl_vars['data']->value['title'];?>

    </a>
    <div class="ow_remark ow_smallmargin">
        <?php echo smarty_modifier_more($_smarty_tpl->tpl_vars['data']->value['description'],150);?>

    </div>
</div>



<?php }?>
</div><?php }
}
