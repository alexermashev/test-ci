<?php
/* Smarty version 3.1.30, created on 2019-01-27 22:58:38
  from "/Users/esase/Sites/8418/ow_system_plugins/base/views/components/change_password.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c4e7dee151647_78481480',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6c9bf278f4ee7afd3c3dc79a9bd66f2a39e9beae' => 
    array (
      0 => '/Users/esase/Sites/8418/ow_system_plugins/base/views/components/change_password.html',
      1 => 1547792051,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c4e7dee151647_78481480 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_script')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/block.script.php';
if (!is_callable('smarty_function_text')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_decorator')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.decorator.php';
if (!is_callable('smarty_block_form')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_label')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.label.php';
if (!is_callable('smarty_function_input')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_error')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.error.php';
if (!is_callable('smarty_function_submit')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.submit.php';
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('script', array());
$_block_repeat1=true;
echo smarty_block_script(array(), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>



        $(function(){
            $("#change_password_button").click(
                function() { 
                    window.oldPassword.floatBox = new OW_FloatBox({$title: '<?php echo smarty_function_text(array('key'=>'base+change_password'),$_smarty_tpl);?>
', $contents: $('#change-password-div'), width: 480});
                    window.owForms['change-user-password'].resetForm();
                    window.owForms['change-user-password'].removeErrors();
                }
            );
       });


<?php $_block_repeat1=false;
echo smarty_block_script(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>


<?php echo smarty_function_decorator(array('name'=>"button",'id'=>"change_password_button",'langLabel'=>'base+change_password'),$_smarty_tpl);?>

<div style="display:none;">
    <div id="change-password-div">
        <?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('name'=>"change-user-password"));
$_block_repeat1=true;
echo smarty_block_form(array('name'=>"change-user-password"), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

                <table class="ow_table_1 ow_form">
                    <tr class="ow_alt2 ow_tr_first">
                        <td class="ow_label" style="width:40%;"><?php echo smarty_function_label(array('name'=>'oldPassword'),$_smarty_tpl);?>
</td>
                        <td class="ow_value"><?php echo smarty_function_input(array('name'=>"oldPassword"),$_smarty_tpl);?>
<br/><?php echo smarty_function_error(array('name'=>"oldPassword"),$_smarty_tpl);?>
</td>
                    </tr>
                    <tr class="ow_alt1" width="40">
                        <td class="ow_label" style="width:40%;"><?php echo smarty_function_label(array('name'=>'password'),$_smarty_tpl);?>
</td>
                        <td class="ow_value"><?php echo smarty_function_input(array('name'=>"password"),$_smarty_tpl);?>
<br/><?php echo smarty_function_error(array('name'=>"password"),$_smarty_tpl);?>
</td>
                    </tr>
                    <tr class="ow_alt2 ow_tr_last">
                        <td class="ow_label" style="width:40%;"><?php echo smarty_function_label(array('name'=>'repeatPassword'),$_smarty_tpl);?>
</td>
                        <td class="ow_value"><?php echo smarty_function_input(array('name'=>"repeatPassword"),$_smarty_tpl);?>
<br/><?php echo smarty_function_error(array('name'=>"repeatPassword"),$_smarty_tpl);?>
</td>
                    </tr>

                </table>
                
                <div class="clearfix ow_stdmargin"><div class="ow_right"><?php echo smarty_function_submit(array('name'=>"change"),$_smarty_tpl);?>
</div></div>
         <?php $_block_repeat1=false;
echo smarty_block_form(array('name'=>"change-user-password"), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

    </div>
</div>
<?php }
}
