<?php

class STRIPETRIAL_CTRL_Main extends BILLINGSTRIPE_CTRL_Action
{
    use STRIPETRIAL_CLASS_ActionMethods;

    /**
     * @var STRIPETRIAL_BOL_Service
     */
    private $service;

    public function __construct()
    {
        $this->service = STRIPETRIAL_BOL_Service::getInstance();
    }

    public function ajaxResponder()
    {
        if ( ! OW::getRequest()->isAjax() )
        {
            exit(json_encode(array('result' => false)));
        }

        if ( $_POST['command'] == 'skip' )
        {
            $this->service->setTrial(1);
//            $this->service->setEmailVerifyForUser(OW::getUser()->getId());
        }

        exit(json_encode(array('result' => true)));
    }

    public function index()
    {
        $lang = OW::getLanguage();
        //$url = OW::getRouter()->urlForRoute('membership_subscribe');
        $url = OW::getRouter()->getBaseUrl();

        $plan = $this->service->getMembershipPlanByAccountType(null, 'number');

        if ( empty($plan) )
        {
            OW::getFeedback()->error($lang->text('membership', 'plan_not_found'));
            OW::getApplication()->redirect($url);
        }

        $gatewayKey = 'billingstripe';
        $userId = OW::getUser()->getId();

        $billingService = BOL_BillingService::getInstance();
        $membershipService = MEMBERSHIP_BOL_MembershipService::getInstance();

        /* get membership plans */
        $authService = BOL_AuthorizationService::getInstance();
        $accTypeName = OW::getUser()->getUserObject()->getAccountType();
        $accType = BOL_QuestionService::getInstance()->findAccountTypeByName($accTypeName);
        $mTypes = $membershipService->getTypeList($accType->id);

        /* @var $defaultRole BOL_AuthorizationRole */
        $defaultRole = $authService->getDefaultRole();

        /* @var $default MEMBERSHIP_BOL_MembershipType */
        $default = new MEMBERSHIP_BOL_MembershipType();
        $default->roleId = $defaultRole->id;

        $mTypes = array_merge(array($default), $mTypes);
        $userMembership = $membershipService->getUserMembership($userId);
        $userRoleIds = array($defaultRole->id);

        if ( $userMembership )
        {
            $type = $membershipService->findTypeById($userMembership->typeId);
            if ( $type )
            {
                $userRoleIds[] = $type->roleId;
                $this->assign('currentTitle', $membershipService->getMembershipTitle($type->roleId));
            }

            $this->assign('current', $userMembership);
        }

        $permissions = $authService->getPermissionList();
        $perms = array();
        foreach ( $permissions as $permission )
        {
            /* @var $permission BOL_AuthorizationPermission */
            $perms[$permission->roleId][$permission->actionId] = true;
        }

        $exclude = $membershipService->getUserTrialPlansUsage($userId);
        $mPlans = $membershipService->getTypePlanList($exclude);

        $plansNumber = 0;
        $mTypesPermissions = array();
        foreach ( $mTypes as $membership )
        {
            $mId = $membership->id;
            $plans = isset($mPlans[$mId]) ? $mPlans[$mId] : null;
            $data = array(
                'id' => $mId,
                'title' => $membershipService->getMembershipTitle($membership->roleId),
                'roleId' => $membership->roleId,
                'permissions' => isset($perms[$membership->roleId]) ? $perms[$membership->roleId] : null,
                'current' => in_array($membership->roleId, $userRoleIds),
                'plans' => $plans
            );
            $plansNumber += count($plans);
            $mTypesPermissions[$mId] = $data;
        }

        $this->assign('mTypePermissions', $mTypesPermissions);
        $this->assign('plansNumber', $plansNumber);
        $this->assign('typesNumber', count($mTypes));
        /* - get membership plans - */

        if ( !$plan = $membershipService->findPlanById($plan) )
        {
            OW::getFeedback()->error($lang->text('membership', 'plan_not_found'));
            OW::getApplication()->redirect($url);
        }

        if ( $plan->price == 0 ) // trial plan
        {
            // check if trial plan used
            $used = $membershipService->isTrialUsedByUser($userId);

            if ( $used )
            {
                OW::getFeedback()->error($lang->text('membership', 'trial_used_error'));
                OW::getApplication()->redirect($url);
            }
            else // give trial plan
            {
                $userMembership = new MEMBERSHIP_BOL_MembershipUser();

                $userMembership->userId = $userId;
                $userMembership->typeId = $plan->typeId;
                $userMembership->expirationStamp = time() + $plan->period * MEMBERSHIP_BOL_MembershipService::getInstance()->getPeriodUnitFactor($plan->periodUnits);
                $userMembership->recurring = 0;
                $userMembership->trial = 1;

                $membershipService->setUserMembership($userMembership);
                $membershipService->addTrialPlanUsage($userId, $plan->id, $plan->period, $plan->periodUnits);

                OW::getFeedback()->info($lang->text('membership', 'trial_granted', array('period' => $plan->period, 'periodUnits' => OW::getLanguage()->text('membership', $plan->periodUnits))));
                OW::getApplication()->redirect($url);
            }
        }

        $gateway = $billingService->findGatewayByKey($gatewayKey);
        if ( !$gateway || !$gateway->active )
        {
            OW::getFeedback()->error($lang->text('base', 'billing_gateway_not_found'));
            OW::getApplication()->redirect($url);
        }

        // create membership plan product adapter object
        $productAdapter = new MEMBERSHIP_CLASS_MembershipPlanProductAdapter();

        // sale object
        $sale = new BOL_BillingSale();
        $sale->pluginKey = 'membership';
        $sale->entityDescription = $membershipService->getFormattedPlan($plan->price, $plan->period, $plan->recurring, null, $plan->periodUnits);
        $sale->entityKey = $productAdapter->getProductKey();
        $sale->entityId = $plan->id;
        $sale->price = floatval($plan->price);
        $sale->period = $plan->period;
        $sale->userId = $userId ? $userId : 0;
        $sale->recurring = $plan->recurring;
        $sale->periodUnits = $plan->periodUnits;

        $saleId = $billingService->initSale($sale, $gatewayKey);

        if ( $saleId )
        {
            // sale Id is temporarily stored in session
            $billingService->storeSaleInSession($saleId);
            $billingService->setSessionBackUrl($url);
//            $billingService->setSessionBackUrl($productAdapter->getProductOrderUrl());

            // redirect to gateway form page
            //UTIL_Url::redirect(OW::getRouter()->urlForRoute(self::PLUGIN_KEY . '_main_index'));
        }

//        parent::orderForm();
        self::orderForm();


        OW::getDocument()->addScriptDeclaration(
            UTIL_JsGenerator::composeJsString(';
                var url = {$url};
                $(document).delegate(".st_skip", "click", function() {
                
                    $.ajax( {
                        url: ' . json_encode(OW::getRouter()->urlForRoute(STRIPETRIAL_BOL_Service::PLUGIN_KEY . '_main_ajaxResponder')) . ',
                        type: \'POST\',
                        data: {command: \'skip\',},
                        dataType: \'json\',
                        success: function(data){
                            window.location.href = url;
                        }
                    } );
                });
                    ', array(
                        'url' => OW::getRouter()->getBaseUrl()
                )
            )
        );

        $this->setPageHeading(OW::getLanguage()->text('stripetrial', 'subscribe'));
    }
}