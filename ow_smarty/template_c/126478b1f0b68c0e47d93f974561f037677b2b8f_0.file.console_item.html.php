<?php
/* Smarty version 3.1.30, created on 2018-07-19 06:54:53
  from "/var/www/biyebiye/public_html/ow_plugins/premoderation/views/components/console_item.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b506dfd0f6192_65443546',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '126478b1f0b68c0e47d93f974561f037677b2b8f' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_plugins/premoderation/views/components/console_item.html',
      1 => 1479204282,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b506dfd0f6192_65443546 (Smarty_Internal_Template $_smarty_tpl) {
?>
<ul class="ow_console_dropdown">
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['items']->value, 'item');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
?>
        <li class="ow_dropdown_menu_item ow_cursor_pointer" >
            <div class="ow_console_dropdown_cont">
                <a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['url'];?>
" class="clearfix">
                    <span class="ow_left"><?php echo $_smarty_tpl->tpl_vars['item']->value['label'];?>
</span>
                    <span class="ow_count_wrap ow_right">          
                        <span class="ow_count_bg">           
                            <span class="ow_count"><?php echo $_smarty_tpl->tpl_vars['item']->value['count'];?>
</span>          
                        </span>    
                    </span>
                </a>
            </div>
        </li>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</ul><?php }
}
