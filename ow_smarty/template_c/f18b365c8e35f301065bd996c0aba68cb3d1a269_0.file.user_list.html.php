<?php
/* Smarty version 3.1.30, created on 2019-01-28 05:28:29
  from "/Users/esase/Sites/8418/ow_plugins/skadate/views/components/user_list.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c4ed94dda60f3_05476048',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f18b365c8e35f301065bd996c0aba68cb3d1a269' => 
    array (
      0 => '/Users/esase/Sites/8418/ow_plugins/skadate/views/components/user_list.html',
      1 => 1547792046,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c4ed94dda60f3_05476048 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_format_date')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.format_date.php';
if (!is_callable('smarty_function_decorator')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.decorator.php';
if (!empty($_smarty_tpl->tpl_vars['userList']->value)) {?>
    <div class="ow_ulist_big clearfix ow_stdmargin" id="user-list">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['userList']->value, 'data', false, 'userId');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['userId']->value => $_smarty_tpl->tpl_vars['data']->value) {
?>
            <?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'default', "fields", null);
?>

                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['fields'], 'field', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['field']->value) {
?>
                    <div class="ow_ulist_big_<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
 ow_small"><?php echo $_smarty_tpl->tpl_vars['field']->value;?>
</div>
                <?php
}
} else {
?>

                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

            <?php $_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);
?>

            
            <?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'default', "activity", null);
?>

                <?php if ($_smarty_tpl->tpl_vars['displayActivity']->value) {?>
                    <div class="ow_ulist_big_activity ow_small">
                        <?php echo smarty_function_text(array('key'=>"base+user_list_activity"),$_smarty_tpl);?>
: <span class="ow_remark"><?php echo smarty_function_format_date(array('timestamp'=>$_smarty_tpl->tpl_vars['data']->value['dto']->activityStamp),$_smarty_tpl);?>
</span>
                    </div>
                <?php } else {
}?>
            <?php $_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);
?>


            <?php echo smarty_function_decorator(array('name'=>"user_big_list_item",'id'=>$_smarty_tpl->tpl_vars['userId']->value,'avatar'=>$_smarty_tpl->tpl_vars['avatars']->value[$_smarty_tpl->tpl_vars['userId']->value],'username'=>$_smarty_tpl->tpl_vars['usernameList']->value[$_smarty_tpl->tpl_vars['userId']->value],'displayName'=>$_smarty_tpl->tpl_vars['displaynameList']->value[$_smarty_tpl->tpl_vars['userId']->value],'online'=>!empty($_smarty_tpl->tpl_vars['onlineList']->value[$_smarty_tpl->tpl_vars['userId']->value]),'fields'=>$_smarty_tpl->tpl_vars['fields']->value,'activity'=>$_smarty_tpl->tpl_vars['activity']->value,'set_class'=>"ow_item_set3"),$_smarty_tpl);?>

        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

    </div>
    <center>
        <?php echo $_smarty_tpl->tpl_vars['paging']->value;?>

    </center>
<?php } else { ?>
    <center><?php echo smarty_function_text(array('key'=>"base+user_no_users"),$_smarty_tpl);?>
</center>
<?php }
}
}
