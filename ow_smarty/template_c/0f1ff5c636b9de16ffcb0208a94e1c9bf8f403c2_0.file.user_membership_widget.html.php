<?php
/* Smarty version 3.1.30, created on 2018-07-26 07:35:28
  from "/var/www/biyebiye/public_html/ow_plugins/membership/views/components/user_membership_widget.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b59b2008d0c28_79224254',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0f1ff5c636b9de16ffcb0208a94e1c9bf8f403c2' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_plugins/membership/views/components/user_membership_widget.html',
      1 => 1479204282,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b59b2008d0c28_79224254 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
?>
<table class="ow_table_3 ow_automargin ow_smallmargin">
    <tr class="<?php if ($_smarty_tpl->tpl_vars['isModerator']->value && !$_smarty_tpl->tpl_vars['isOwner']->value) {?>ow_tr_first<?php } else { ?>ow_tr_last ow_tr_first<?php }?> ">
        <td class="ow_label"><?php echo smarty_function_text(array('key'=>'membership+membership'),$_smarty_tpl);?>
</td>
        <td class="ow_value"><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</td>
    </tr>
    <?php if ($_smarty_tpl->tpl_vars['isModerator']->value && !$_smarty_tpl->tpl_vars['isOwner']->value) {?>
    <tr class="ow_tr_last">
        <td class="ow_label"><?php echo smarty_function_text(array('key'=>'membership+expires'),$_smarty_tpl);?>
</td>
        <td class="ow_value"><?php echo MEMBERSHIP_BOL_MembershipService::formatDate(array('timestamp'=>$_smarty_tpl->tpl_vars['membership']->value->expirationStamp),$_smarty_tpl);?>
</td>
    </tr>
    <?php }?>
</table><?php }
}
