<?php

class STRIPETRIAL_BOL_TrialDao extends OW_BaseDao
{
    /**
     * Singleton instance.
     *
     * @var STRIPETRIAL_BOL_TrialDao
     */
    private static $classInstance;

    /**
     * Returns an instance of class (singleton pattern implementation).
     *
     * @return STRIPETRIAL_BOL_TrialDao
     */
    public static function getInstance()
    {
        if (self::$classInstance === null) {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    /**
     * @see OW_BaseDao::getDtoClassName()
     *
     */
    public function getDtoClassName()
    {
        return 'STRIPETRIAL_BOL_Trial';
    }

    /**
     * @see OW_BaseDao::getTableName()
     *
     */
    public function getTableName()
    {
        return OW_DB_PREFIX . 'stripetrial_trial';
    }

    /**
     * Find by userId
     *
     * @param $userId
     * @return array|void
     */
    public function findByUserId( $userId )
    {
        if ( !$userId ) {
            return;
        }

        $example = new OW_Example();
        $example->andFieldEqual('userId', $userId);

        return $this->findObjectByExample($example);
    }
}