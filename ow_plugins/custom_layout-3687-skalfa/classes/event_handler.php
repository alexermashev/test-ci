<?php

class CUSTOMLAYOUT_CLASS_EventHandler
{
    /**
     * Class instance
     *
     * @var CUSTOMLAYOUT_CLASS_EventHandler
     */
    private static $classInstance;

    /**
     * Class constructor
     */
    private function __construct()
    {

    }

    /**
     * Get instance
     *
     * @return CUSTOMLAYOUT_CLASS_EventHandler
     */
    public static function getInstance()
    {
        if (self::$classInstance === null) {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    public function init()
    {
        OW::getEventManager()->bind(OW_EventManager::ON_PLUGINS_INIT, [$this, 'afterInit']);
    }

    public function afterInit()
    {
        OW::getEventManager()->bind('skmobileapp.formatted_users_data', array($this, 'onFormattedUsersData'));
        OW::getEventManager()->bind('skmobileapp.get_translations', array($this, 'onGetTranslations'));
    }

    public function onFormattedUsersData( OW_Event $event )
    {
        $data = $event->getData();

        $ids = [];
        foreach ( $data as $user )
        {
            $ids[] = $user['id'];
        }

        $customQuestions = CUSTOMLAYOUT_BOL_Service::getCustomQuestions();
        $customLineByUserIds = CUSTOMLAYOUT_BOL_Service::getInstance()->getCustomLineByUserIds($ids);

        $newData = [];

        foreach ( $data as $user )
        {
            if ( !empty($user['viewQuestions']) )
            {
                foreach ( $user['viewQuestions'] as $skey => $section )
                {
                    $newItems = [];
                    foreach ( $section['items'] as $qKey => $question )
                    {
                        if ( ! in_array($question['name'], $customQuestions) )
                        {
                            $newItems[] = $question;
                        }
                    }
                }
            }

            $user['customLine'] = $customLineByUserIds[$user['id']];

            $newData[] = $user;
        }

        $event->setData($newData);
    }

    /**
     * Get translations to skmobileapp
     * @param OW_Event $event
     */
    public function onGetTranslations( OW_Event $event )
    {
        $langService = BOL_LanguageService::getInstance();
        $translations = [];

        $my_keys = $langService->findAllPrefixKeys($langService->findPrefixId(CUSTOMLAYOUT_BOL_Service::PLUGIN_KEY));

        foreach ( $my_keys as $item ) {
            $translations[$item->key] = OW::getLanguage()->text(CUSTOMLAYOUT_BOL_Service::PLUGIN_KEY, $item->key);
        }

        $event->add(CUSTOMLAYOUT_BOL_Service::PLUGIN_KEY, $translations);
    }
}