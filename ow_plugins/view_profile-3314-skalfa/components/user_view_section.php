<?php

class VIEWPROFILE_CMP_UserViewSection extends BASE_CMP_UserViewSection
{
    public function __construct( $section, $sectionQuestions, $data, $labels, $template = 'table', $hideSection = false, $additionalParams = array() )
    {
        parent::__construct($section, $sectionQuestions, $data, $labels, $template = 'table', $hideSection = false, $additionalParams = array());

        $this->assign('sectionName', $section);
        $this->assign('questions', $sectionQuestions);
        $this->assign('questionsData', $data);
        $this->assign('labels', $labels);
        $this->assign('display', !$hideSection);

        $this->setTemplate(OW::getPluginManager()->getPlugin('viewprofile')->getCmpViewDir() . 'user_view_section_table.html' );
    }
}