<?php
/* Smarty version 3.1.30, created on 2018-07-26 00:45:29
  from "/var/www/biyebiye/public_html/ow_plugins/billing_stripe/views/controllers/action_order_form.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5951e9e09e95_12287471',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3dbd5b380c21e4c605f9474046e4f0d61cda88c6' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_plugins/billing_stripe/views/controllers/action_order_form.html',
      1 => 1532322022,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b5951e9e09e95_12287471 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_style')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.style.php';
if (!is_callable('smarty_block_block_decorator')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.block_decorator.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_decorator')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.decorator.php';
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('style', array());
$_block_repeat1=true;
echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

    
    .ow_credit_card {
        margin-bottom: 32px;
        border-radius: 6px;
        -moz-border-radius: 6px;
        -webkit-border-radius: 6px;
    }
    .ow_credit_card .ow_table_3 {
        padding: 0px 14px;
    }
    .ow_credit_card .ow_box_empty {
        padding-left: 14px;
    }
    .ow_credit_card .ow_box_empty .ow_outline {
        font-weight: normal;
    }
    .ow_credit_card th {
        background: none;
        padding: 8px 0px;
        text-align: left;
    }
    .ow_credit_card .ow_table_3 td {
        border: none;
        padding: 30px 17px 0px;
    }
    .ow_credit_card .ow_table_3 td.ow_label {
        vertical-align: middle;
        text-align: left;
        width: 25%;
    }
    .ow_credit_card .ow_table_3 td.ow_value {
        width: auto;
    }
    .ow_credit_card select ~ select {
        margin-left: 32px;
    }
    
<?php $_block_repeat1=false;
echo smarty_block_style(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>


<div class="ow_superwide ow_automargin">
<form method="post" id="<?php echo $_smarty_tpl->tpl_vars['formId']->value;?>
" action="<?php echo $_smarty_tpl->tpl_vars['formAction']->value;?>
">
<?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('block_decorator', array('name'=>'box','capEnabled'=>false,'addClass'=>'ow_credit_card ow_break_word'));
$_block_repeat1=true;
echo smarty_block_block_decorator(array('name'=>'box','capEnabled'=>false,'addClass'=>'ow_credit_card ow_break_word'), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

    <span class="ow_outline"><?php echo $_smarty_tpl->tpl_vars['productString']->value;?>
</span>

    <table class="ow_table_3 ow_credit_card">
        <tbody>
        <tr class="ow_tr_first">
            <th colspan="2"><?php echo smarty_function_text(array('key'=>'billingstripe+card_details'),$_smarty_tpl);?>
</th>
        </tr>
        <tr>
            <td class="ow_label"><?php echo smarty_function_text(array('key'=>'billingstripe+card_number'),$_smarty_tpl);?>
</td>
            <td class="ow_value"><input type="text" size="20" autocomplete="off" class="c-number" /></td>
        </tr>
        <tr>
            <td class="ow_label"><?php echo smarty_function_text(array('key'=>'billingstripe+cvc'),$_smarty_tpl);?>
</td>
            <td class="ow_value"><input type="text" maxlength="4" class="c-cvc" style="width: 40px;"></td>
        </tr>
        <tr>
            <td class="ow_label"><?php echo smarty_function_text(array('key'=>'billingstripe+expiration_date'),$_smarty_tpl);?>
</td>
            <td class="ow_value">
                <select class="c-expiry-month" style="width: 80px;">
                    <option><?php echo smarty_function_text(array('key'=>'base+month'),$_smarty_tpl);?>
</option>
                    <?php
$__section_m_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_m']) ? $_smarty_tpl->tpl_vars['__smarty_section_m'] : false;
$_smarty_tpl->tpl_vars['__smarty_section_m'] = new Smarty_Variable(array());
if (true) {
for ($_smarty_tpl->tpl_vars['__smarty_section_m']->value['iteration'] = 1, $_smarty_tpl->tpl_vars['__smarty_section_m']->value['index'] = 1; $_smarty_tpl->tpl_vars['__smarty_section_m']->value['iteration'] <= 12; $_smarty_tpl->tpl_vars['__smarty_section_m']->value['iteration']++, $_smarty_tpl->tpl_vars['__smarty_section_m']->value['index']++){
?><option value="<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_m']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_section_m']->value['iteration'] : null);?>
"><?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_m']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_section_m']->value['iteration'] : null);?>
</option><?php
}
}
if ($__section_m_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_m'] = $__section_m_0_saved;
}
?>
                </select>
                <select class="c-expiry-year" style="width: 80px;">
                    <option><?php echo smarty_function_text(array('key'=>'base+year'),$_smarty_tpl);?>
</option>
                    <?php
$__section_y_1_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_y']) ? $_smarty_tpl->tpl_vars['__smarty_section_y'] : false;
$__section_y_1_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['endYear']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_y_1_start = (int)@$_smarty_tpl->tpl_vars['startYear']->value < 0 ? max(0, (int)@$_smarty_tpl->tpl_vars['startYear']->value + $__section_y_1_loop) : min((int)@$_smarty_tpl->tpl_vars['startYear']->value, $__section_y_1_loop);
$__section_y_1_total = min(($__section_y_1_loop - $__section_y_1_start), $__section_y_1_loop);
$_smarty_tpl->tpl_vars['__smarty_section_y'] = new Smarty_Variable(array());
if ($__section_y_1_total != 0) {
for ($__section_y_1_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_y']->value['index'] = $__section_y_1_start; $__section_y_1_iteration <= $__section_y_1_total; $__section_y_1_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_y']->value['index']++){
?><option value="<?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_y']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_y']->value['index'] : null);?>
"><?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_y']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_y']->value['index'] : null);?>
</option><?php
}
}
if ($__section_y_1_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_y'] = $__section_y_1_saved;
}
?>
                </select>
            </td>
        </tr>
        <tr class="ow_tr_last">
            <td class="ow_label"><?php echo smarty_function_text(array('key'=>'billingstripe+name_on_card'),$_smarty_tpl);?>
</td>
            <td class="ow_value"><input type="text" autocomplete="off" class="c-name" /></td>
        </tr>
        </tbody>
    </table>
    <?php if ($_smarty_tpl->tpl_vars['requireData']->value) {?>
    <table class="ow_table_3 ow_credit_card ow_billing_info">
        <tbody>
        <tr class="ow_tr_first">
            <th colspan="2"><?php echo smarty_function_text(array('key'=>'billingstripe+billing_info'),$_smarty_tpl);?>
</th>
        </tr>
        <tr>
            <td class="ow_label"><?php echo smarty_function_text(array('key'=>'billingstripe+country'),$_smarty_tpl);?>
</td>
            <td class="ow_value">
                <select class="billing-country">
                    <option value=""><?php echo smarty_function_text(array('key'=>'billingstripe+choose_country'),$_smarty_tpl);?>
</option>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['countries']->value, 'label', false, 'code');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['code']->value => $_smarty_tpl->tpl_vars['label']->value) {
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['code']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['label']->value;?>
</option>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                </select>
            </td>
        </tr>
        <tr>
            <td class="ow_label"><?php echo smarty_function_text(array('key'=>'billingstripe+state'),$_smarty_tpl);?>
</td>
            <td class="ow_value"><input type="text" class="billing-state"></td>
        </tr>
        <tr>
            <td class="ow_label"><?php echo smarty_function_text(array('key'=>'billingstripe+address'),$_smarty_tpl);?>
</td>
            <td class="ow_value"><input type="text" class="billing-address1"></td>
        </tr>
        <tr class="ow_tr_last">
            <td class="ow_label"><?php echo smarty_function_text(array('key'=>'billingstripe+zip_code'),$_smarty_tpl);?>
</td>
            <td class="ow_value"><input type="text" class="billing-zip" style="width: 80px;" /></td>
        </tr>
        </tbody>
    </table>
    <?php }
$_block_repeat1=false;
echo smarty_block_block_decorator(array('name'=>'box','capEnabled'=>false,'addClass'=>'ow_credit_card ow_break_word'), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

<div class="clearfix">
    <div class="ow_right">
        <?php echo smarty_function_decorator(array('name'=>'button','type'=>'submit','langLabel'=>'billingstripe+complete_order','id'=>'btn-stripe-checkout'),$_smarty_tpl);?>

    </div>
</div>
</form>
</div><?php }
}
