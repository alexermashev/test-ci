<?php

Updater::getLanguageService()->deleteLangKey('stripetrial', 'browser_custom_desc');

$updateDir = dirname(__FILE__) . DS;
Updater::getLanguageService()->importPrefixFromZip($updateDir . 'langs.zip', 'stripetrial');