<?php
/* Smarty version 3.1.30, created on 2018-07-19 04:47:47
  from "/var/www/biyebiye/public_html/ow_plugins/membership/views/components/promo_widget.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b505033c05108_50553747',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '954a35b42439cad3275d219d41353fae6aeb0db1' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_plugins/membership/views/components/promo_widget.html',
      1 => 1479204282,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b505033c05108_50553747 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_style')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.style.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_decorator')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.decorator.php';
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('style', array());
$_block_repeat1=true;
echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>


.ow_mship_widget_block {
    line-height: 16px;
}
.ow_mship_widget_benefits ul {
    margin-bottom: 0px;
}
.ow_mship_widget_more {
    padding-left: 16px;
}

<?php $_block_repeat1=false;
echo smarty_block_style(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>


<div class="ow_mship_widget_block">
    <?php if (isset($_smarty_tpl->tpl_vars['title']->value)) {?>
    <div class="ow_mship_widget_current">
        <span class="ow_mship_widget_label"><?php echo smarty_function_text(array('key'=>'membership+your_membership'),$_smarty_tpl);?>
: </span>
        <span class="ow_mship_widget_value ow_outline"><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</span>
    </div>
    <?php }?>
    <div class="ow_mship_widget_txt ow_smallmargin"><?php echo smarty_function_text(array('key'=>'membership+consider_upgrading'),$_smarty_tpl);?>
</div>
    <div class="ow_mship_widget_benefits ow_smallmargin">
        <ul class="ow_regular">
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['actions']->value, 'a');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['a']->value) {
?>
            <li><?php echo $_smarty_tpl->tpl_vars['a']->value;?>
</li>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

        </ul>
        <?php if ($_smarty_tpl->tpl_vars['showMore']->value) {?><div class="ow_mship_widget_more"><?php echo smarty_function_text(array('key'=>'membership+and_more'),$_smarty_tpl);?>
</div><?php }?>
    </div>
    <div class="ow_mship_widget_btn ow_center">
        <?php echo smarty_function_decorator(array('name'=>'button','class'=>'ow_ic_up_arrow','id'=>'btn-sidebar-upgrade','langLabel'=>'membership+upgrade'),$_smarty_tpl);?>

    </div>
</div><?php }
}
