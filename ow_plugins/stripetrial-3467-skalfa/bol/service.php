<?php

class STRIPETRIAL_BOL_Service
{
    const PLUGIN_KEY = 'stripetrial';

    /**
     * @var STRIPETRIAL_BOL_TrialDao
     */
    private $trialDao;

    /**
     * Constructor.
     */
    private function __construct()
    {
        $this->trialDao = STRIPETRIAL_BOL_TrialDao::getInstance();
    }
    /**
     * Singleton instance.
     *
     * @var STRIPETRIAL_BOL_Service
     */
    private static $classInstance;

    /**
     * Returns an instance of class (singleton pattern implementation).
     *
     * @return STRIPETRIAL_BOL_Service
     */
    public static function getInstance()
    {
        if ( self::$classInstance === null )
        {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    public function getStatusByUserId( $userId )
    {
        return $this->trialDao->findByUserId($userId);
    }

    /**
     * @param STRIPETRIAL_BOL_Trial $trial
     */
    public function saveTrial( STRIPETRIAL_BOL_Trial $trial )
    {
        $this->trialDao->save($trial);
    }

    /**
     * Set trial after visit custom subxcribe and click on buttons (skip, subscribe)
     * @param null $userId
     * @param int $status
     */
    public function setTrial( $status = 0, $userId = null )
    {
        if ( empty($userId) )
        {
            $userId = OW::getUser()->getId();
        }

        if ( empty($userId) )
        {
            return;
        }

        /**
         * @var STRIPETRIAL_BOL_Trial $lastTime
         */
        $trial = self::getStatusByUserId($userId);

        if ( empty($trial) )
        {
            $trial = new STRIPETRIAL_BOL_Trial();
            $trial->userId = $userId;
        }

        $trial->status = $status;

        $this->saveTrial($trial);
    }

    public function getMemberships($userId)
    {
        $membershipService = MEMBERSHIP_BOL_MembershipService::getInstance();
        $authService = BOL_AuthorizationService::getInstance();

        $accTypeName = BOL_UserService::getInstance()->findUserById($userId)->getAccountType();
        $accType = BOL_QuestionService::getInstance()->findAccountTypeByName($accTypeName);

        $mTypes = $membershipService->getTypeList($accType->id);

        /* @var $defaultRole BOL_AuthorizationRole */
        $defaultRole = $authService->getDefaultRole();

        /* @var $default MEMBERSHIP_BOL_MembershipType */
        $default = new MEMBERSHIP_BOL_MembershipType();
        $default->roleId = $defaultRole->id;

        $mTypes = array_merge(array($default), $mTypes);

        $userMembership = $membershipService->getUserMembership($userId);
        $userRoleIds = array($defaultRole->id);

        if ( $userMembership )
        {
            $type = $membershipService->findTypeById($userMembership->typeId);
            if ( $type )
            {
                $userRoleIds[] = $type->roleId;
            }
        }

        $permissions = $authService->getPermissionList();

        $perms = array();
        foreach ( $permissions as $permission )
        {
            /* @var $permission BOL_AuthorizationPermission */
            $perms[$permission->roleId][$permission->actionId] = true;
        }

        $exclude = $membershipService->getUserTrialPlansUsage($userId);

        $mPlans = $membershipService->getTypePlanList($exclude);

        $plansNumber = 0;
        $mTypesPermissions = array();
        foreach ( $mTypes as $membership )
        {
            $mId = $membership->id;
            $plans = isset($mPlans[$mId]) ? $mPlans[$mId] : null;
            $data = array(
                'id' => $mId,
                'title' => $membershipService->getMembershipTitle($membership->roleId),
                'roleId' => $membership->roleId,
                'permissions' => isset($perms[$membership->roleId]) ? $perms[$membership->roleId] : null,
                'current' => in_array($membership->roleId, $userRoleIds),
                'plans' => $plans
            );
            $plansNumber += count($plans);
            $mTypesPermissions[] = $data;
        }
        return $mTypesPermissions;
    }

    public function redirectToForm()
    {
        $lang = OW::getLanguage();
        $url = OW::getRouter()->urlForRoute('membership_subscribe');

        $plan = self::getMembershipPlanByAccountType(null, 'number');

        if ( empty($plan) )
        {
            OW::getFeedback()->error($lang->text('membership', 'plan_not_found'));
            OW::getApplication()->redirect($url);
        }

        $gatewayKey = 'billingstripe';

        $userId = OW::getUser()->getId();

        $billingService = BOL_BillingService::getInstance();
        $membershipService = MEMBERSHIP_BOL_MembershipService::getInstance();

        if ( !$plan = $membershipService->findPlanById($plan) )
        {
            OW::getFeedback()->error($lang->text('membership', 'plan_not_found'));
            OW::getApplication()->redirect($url);
        }

        if ( $plan->price == 0 ) // trial plan
        {
            // check if trial plan used
            $used = $membershipService->isTrialUsedByUser($userId);

            if ( $used )
            {
                OW::getFeedback()->error($lang->text('membership', 'trial_used_error'));
                OW::getApplication()->redirect($url);
            }
            else // give trial plan
            {
                $userMembership = new MEMBERSHIP_BOL_MembershipUser();

                $userMembership->userId = $userId;
                $userMembership->typeId = $plan->typeId;
                $userMembership->expirationStamp = time() + $plan->period * MEMBERSHIP_BOL_MembershipService::getInstance()->getPeriodUnitFactor($plan->periodUnits);
                $userMembership->recurring = 0;
                $userMembership->trial = 1;

                $membershipService->setUserMembership($userMembership);
                $membershipService->addTrialPlanUsage($userId, $plan->id, $plan->period, $plan->periodUnits);

                OW::getFeedback()->info($lang->text('membership', 'trial_granted', array('period' => $plan->period, 'periodUnits' => OW::getLanguage()->text('membership', $plan->periodUnits))));
                OW::getApplication()->redirect($url);
            }
        }

        $gateway = $billingService->findGatewayByKey($gatewayKey);
        if ( !$gateway || !$gateway->active )
        {
            OW::getFeedback()->error($lang->text('base', 'billing_gateway_not_found'));
            OW::getApplication()->redirect($url);
        }

        // create membership plan product adapter object
        $productAdapter = new MEMBERSHIP_CLASS_MembershipPlanProductAdapter();

        // sale object
        $sale = new BOL_BillingSale();
        $sale->pluginKey = 'membership';
        $sale->entityDescription = $membershipService->getFormattedPlan($plan->price, $plan->period, $plan->recurring, null, $plan->periodUnits);
        $sale->entityKey = $productAdapter->getProductKey();
        $sale->entityId = $plan->id;
        $sale->price = floatval($plan->price);
        $sale->period = $plan->period;
        $sale->userId = $userId ? $userId : 0;
        $sale->recurring = $plan->recurring;
        $sale->periodUnits = $plan->periodUnits;

        $saleId = $billingService->initSale($sale, $gatewayKey);

        if ( $saleId )
        {
            // sale Id is temporarily stored in session
            $billingService->storeSaleInSession($saleId);
            $billingService->setSessionBackUrl($productAdapter->getProductOrderUrl());

            // redirect to gateway form page
            UTIL_Url::redirect(OW::getRouter()->urlForRoute(self::PLUGIN_KEY . '_main_index'));
        }
    }

    /**
     * Get membership plan by account type
     * @param null $accountType
     * @param string $isFormat
     * @return null|string|int|MEMBERSHIP_BOL_MembershipPlan
     */
    public function getMembershipPlanByAccountType( $accountType = null, $isFormat = 'string' )
    {
        $stripeTrialPlans = [ 7, 8 ]; //add site

        if ( OW::getUser()->getUserObject() && $accountType == null ) {
            $accountType = OW::getUser()->getUserObject()->getAccountType();
        }

        $accTypeList = BOL_QuestionAccountTypeDao::getInstance()->findAccountTypeByNameList(array($accountType));
        $accountTypeId = isset($accTypeList[0]) ? $accTypeList[0]->id : null;
        $typeList = MEMBERSHIP_BOL_MembershipTypeDao::getInstance()->getTypeList($accountTypeId);
        $plans = MEMBERSHIP_BOL_MembershipPlanDao::getInstance()->findPlanListByTypeId($typeList[0]['id']);

        $currentPlan = null;

        if ( isset($plans[0]) )
        {
            $currentPlan = $plans[0];
        }

//        foreach ( $plans as $plan )
//        {
//            if ( in_array($plan->id, $stripeTrialPlans) )
//            {
//                $currentPlan = $plan;
//                break;
//            }
//        }

        $result = null;

        switch ($isFormat) {
            case "string":
                $result = 'MEMBERSHIP_PLAN_' . $currentPlan->id;
                break;
            case "number":
                $result = $currentPlan->id;
                break;
            case "object":
                $result = $currentPlan;
                break;
        }

        return $result;
    }

    public function setEmailVerifyForUser( $userId, $status = 0 )
    {
        $userDao = BOL_UserDao::getInstance();

        $userDto = $userDao->findById($userId);
        $userDto->emailVerify = $status;

        $userDao->save($userDto);
    }
}