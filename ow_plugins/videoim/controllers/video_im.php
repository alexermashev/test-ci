<?php

/**
 * Copyright (c) 2015, Skalfa LLC
 * All rights reserved.
 *
 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is licensed under SkaDate Exclusive License by Skalfa LLC.
 *
 * Full text of this license can be found at http://www.skadate.com/sel.pdf
 */

/**
 * Video IM controller
 *
 * @author Alex Ermashev <alexermashev@gmail.com>
 * @package ow_plugin.videoim.controllers
 * @since 1.8.1
 */
class VIDEOIM_CTRL_VideoIm extends OW_ActionController
{
    /**
     * Service
     *
     * @var VIDEOIM_BOL_VideoImService
     */
    protected $service;

    /**
     * Init
     *
     * @return void
     */
    public function init()
    {
        $this->service = VIDEOIM_BOL_VideoImService::getInstance();
    }

    /**
     * Get chat link
     */
    public function ajaxGetChatLink()
    {
        $recipientId = !empty($_GET['recipientId']) ? (int) $_GET['recipientId'] : -1;

        echo json_encode(array(
            'content' => OW::getClassInstance('VIDEOIM_CMP_ChatLink', $recipientId)->render()
        ));

        exit;
    }

    /**
     * Ajax mark notifications as accepted
     */
    public function ajaxNotificationsMarkAccepted()
    {
        if ( OW::getRequest()->isPost() && OW::getUser()->getId() )
        {
            $userId = !empty($_POST['user_id'])
                ? (int) $_POST['user_id']
                : -1;

            $this->service->markAcceptedNotifications($userId, OW::getUser()->getId());
        }

        exit;
    }

    /**
     * Ajax track credits timing call
     */
    public function ajaxTrackCreditsTimingCall()
    {
        $allowed = false;

        if ( OW::getRequest()->isPost() && OW::getUser()->getId() )
        {
            $allowed = $this->service->isCreditsTimingCallEnough();

            if ( $allowed )
            {
                $this->service->trackTimedAction();
            }
        }

        echo json_encode(array(
            'allowed' => $allowed
        ));

        exit;
    }

    /**
     * Ajax decline request
     */
    public function ajaxDeclineRequest()
    {
        if ( OW::getRequest()->isPost() && OW::getUser()->getId() )
        {
            $userId = !empty($_POST['user_id'])
                ? (int) $_POST['user_id']
                : -1;

            // clear all old notifications
            $this->service->deleteUserNotifications($userId, OW::getUser()->getId());

            // add a new notification
            $notificationBol = new VIDEOIM_BOL_Notification;
            $notificationBol->userId = OW::getUser()->getId();
            $notificationBol->createStamp = time();
            $notificationBol->recipientId = $userId;
            $notificationBol->accepted = VIDEOIM_BOL_NotificationDao::NOTIFICATION_NOT_ACCEPTED;
            $notificationBol->notification = json_encode(array(
                'type' => VIDEOIM_BOL_NotificationDao::NOTIFICATION_TYPE_DECLINED
            ));

            $this->service->addNotification($notificationBol);
        }

        exit;
    }

    /**
     * Ajax block user
     */
    public function ajaxBlockUser()
    {
        if ( OW::getRequest()->isPost() && OW::getUser()->getId() )
        {
            $userId = !empty($_POST['user_id'])
                ? (int) $_POST['user_id']
                : -1;

            if (  !$this->service->isSuperModerator($userId) )
            {
                // clear all old notifications
                $this->service->deleteUserNotifications($userId, OW::getUser()->getId());

                // add a new notification
                $notificationBol = new VIDEOIM_BOL_Notification;
                $notificationBol->userId = OW::getUser()->getId();
                $notificationBol->createStamp = time();
                $notificationBol->recipientId = $userId;
                $notificationBol->accepted = VIDEOIM_BOL_NotificationDao::NOTIFICATION_NOT_ACCEPTED;
                $notificationBol->notification = json_encode(array(
                    'type' => VIDEOIM_BOL_NotificationDao::NOTIFICATION_TYPE_BLOCKED
                ));

                $this->service->addNotification($notificationBol);

                // block
                BOL_UserService::getInstance()->block($userId);
            }
        }

        exit;
    }

    /**
     * Ajax send notification
     */
    public function ajaxSendNotification()
    {
        $result = false;
        $errorMessage = OW::getLanguage()->text('videoim', 'unsupported_notification');

        if ( OW::getRequest()->isPost() && OW::getUser()->getId() )
        {
            $recipientId = !empty($_POST['recipient_id'])
                ? (int) $_POST['recipient_id']
                : -1;

            $notification = !empty($_POST['notification'])
                ? $_POST['notification']
                : null;

            if ( $notification )
            {
                $notificationArray = json_decode($notification, true);
                $clearOldNotifications = false;

                if ( !empty($notificationArray['type']) )
                {
                    switch($notificationArray['type'])
                    {
                        case VIDEOIM_BOL_NotificationDao::NOTIFICATION_TYPE_OFFER :
                            list($result, $errorMessage) = $this->service->isAllowedSendVideoImRequest($recipientId);
                            $this->service->saveAuthActionsGroupKey();
                            $clearOldNotifications = true;
                            break;

                        case VIDEOIM_BOL_NotificationDao::NOTIFICATION_TYPE_ANSWER :
                            list($result, $errorMessage) = $this->service->isAllowedReceiveVideoImRequest();

                            if ( !$result )
                            {
                                // answer is not permitted
                                $notificationBol = new VIDEOIM_BOL_Notification;
                                $notificationBol->userId = OW::getUser()->getId();
                                $notificationBol->recipientId = $recipientId;
                                $notificationBol->notification = json_encode(array(
                                    'type' => VIDEOIM_BOL_NotificationDao::NOTIFICATION_TYPE_NOT_PERMITTED,

                                ));

                                $notificationBol->createStamp = time();
                                $notificationBol->accepted = VIDEOIM_BOL_NotificationDao::NOTIFICATION_NOT_ACCEPTED;
                                $this->service->addNotification($notificationBol);
                            }
                            else
                            {
                                // increase the actions track
                                $this->service->saveAuthActionsGroupKey();
                                $this->service->trackBasicActions($recipientId);
                            }
                            break;

                        case VIDEOIM_BOL_NotificationDao::NOTIFICATION_TYPE_CANDIDATE :
                            $result = true;
                            $errorMessage = null;
                            break;

                        case VIDEOIM_BOL_NotificationDao::NOTIFICATION_TYPE_NOT_SUPPORTED :
                        case VIDEOIM_BOL_NotificationDao::NOTIFICATION_TYPE_BYE :
                        case VIDEOIM_BOL_NotificationDao::NOTIFICATION_TYPE_CREDITS_OUT :
                            $result = true;
                            $errorMessage = null;
                            $clearOldNotifications = true;
                            break;

                        default :
                    }
                }

                if ( $clearOldNotifications )
                {
                    $this->service->deleteUserNotifications(OW::getUser()->getId(), $recipientId);
                }

                if ( $result )
                {
                    // add a new notification
                    $notificationBol = new VIDEOIM_BOL_Notification;
                    $notificationBol->userId = OW::getUser()->getId();
                    $notificationBol->recipientId = $recipientId;
                    $notificationBol->notification = $notification;
                    $notificationBol->createStamp = time();
                    $notificationBol->accepted = VIDEOIM_BOL_NotificationDao::NOTIFICATION_NOT_ACCEPTED;

                    $this->service->addNotification($notificationBol);
                }
            }
        }

        echo json_encode(array(
            'result'  => $result,
            'message' => $errorMessage
        ));

        exit;
    }

    /**
     * Chat window
     */
    public function chatWindow()
    {
        // get vars
        $recipientId = !empty($_GET['recipientId']) ? (int) $_GET['recipientId'] : -1;
        $isInitiator = !empty($_GET['initiator']) ? 1 : 0;

        // check user existing
        if ( null === BOL_UserService::getInstance()->findUserById($recipientId) )
        {
            throw new Redirect404Exception();
        }

        // clear all old notifications
        if ( $isInitiator )
        {
            $this->service->deleteUserNotifications(OW::getUser()->getId(), $recipientId);
        }

        // change the master page
        OW::getDocument()->getMasterPage()->
                setTemplate(OW::getPluginManager()->getPlugin('videoim')->getRootDir() . 'views/master_pages/blank.html');

        // include necessary js and css files
        OW::getDocument()->addStyleSheet(OW::getPluginManager()->
                getPlugin('videoim')->getStaticCssUrl() . 'videoim.css?build=' . $this->service->getPluginBuild());

        $this->includeBaseResources($recipientId, $isInitiator);
    }

    /**
     * Include base resources
     *
     * @param integer $recipientId
     * @param integer $isInitiator
     * @apram integer $avatarSize
     * @return void
     */
    protected function includeBaseResources($recipientId, $isInitiator, $avatarSize = 2)
    {
        $currentPluginBuild = $this->service->getPluginBuild();

        // include necessary js and css files
        OW::getDocument()->addScript(OW::getPluginManager()->
                getPlugin('videoim')->getStaticJsUrl() . 'adapter.js?build=' . $currentPluginBuild);

        OW::getDocument()->addScript(OW::getPluginManager()->
                getPlugin('videoim')->getStaticJsUrl() . 'videoim.js?build=' . $currentPluginBuild);

        OW::getDocument()->addScript(OW::getPluginManager()->
                getPlugin('videoim')->getStaticJsUrl() . 'videoim_controls.js?build=' . $currentPluginBuild);

        OW::getDocument()->addScript(OW::getPluginManager()->
                getPlugin('videoim')->getStaticJsUrl() . 'jquery.fullscreen.js?build=' . $currentPluginBuild);

        OW::getDocument()->addScript(OW::getPluginManager()->
                getPlugin('videoim')->getStaticJsUrl() . 'timer.jquery.js?build=' . $currentPluginBuild);

        $configs = OW::getConfig()->getValues('videoim');
        $timedCallPrice =!OW::getUser()->isAdmin()
            ? $this->service->getTimedCallPrice()
            : 0;

        // init view variables
        $this->assign('serverList', $configs['server_list']);
        $this->assign('recipientId', $recipientId);
        $this->assign('recipientUrl', BOL_UserService::getInstance()->getUserUrl($recipientId));
        $this->assign('recipientAvatar', $this->service->getUserAvatar($recipientId, $avatarSize));
        $this->assign('recipientName', BOL_UserService::getInstance()->getDisplayName($recipientId));
        $this->assign('isInitiator', $isInitiator);
        $this->assign('notificationsLifetime', VIDEOIM_BOL_NotificationDao::NOTIFICATIONS_LIFETIME);
        $this->assign('timedCallPrice', $timedCallPrice);
    }
}