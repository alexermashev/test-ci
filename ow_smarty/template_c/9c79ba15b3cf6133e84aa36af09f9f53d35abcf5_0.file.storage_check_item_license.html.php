<?php
/* Smarty version 3.1.30, created on 2018-07-23 01:00:27
  from "/var/www/biyebiye/public_html/ow_system_plugins/admin/views/controllers/storage_check_item_license.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5560eb63b729_92140095',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9c79ba15b3cf6133e84aa36af09f9f53d35abcf5' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_system_plugins/admin/views/controllers/storage_check_item_license.html',
      1 => 1479204252,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b5560eb63b729_92140095 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_block_decorator')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.block_decorator.php';
if (!is_callable('smarty_block_form')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_label')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.label.php';
if (!is_callable('smarty_function_input')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_submit')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.submit.php';
?>
<div class="ow_wide ow_automargin">
    <?php if (empty($_smarty_tpl->tpl_vars['message']->value)) {?>    
        <?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('block_decorator', array('name'=>'box','addClass'=>'ow_stdmargin','iconClass'=>'ow_ic_plugin','langLabel'=>'admin+item_license_request_box_cap_label'));
$_block_repeat1=true;
echo smarty_block_block_decorator(array('name'=>'box','addClass'=>'ow_stdmargin','iconClass'=>'ow_ic_plugin','langLabel'=>'admin+item_license_request_box_cap_label'), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

        <div style="text-align:center;">
            <?php echo $_smarty_tpl->tpl_vars['text']->value;?>

            <br /><br />
            <?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('name'=>'license-key'));
$_block_repeat2=true;
echo smarty_block_form(array('name'=>'license-key'), null, $_smarty_tpl, $_block_repeat2);
while ($_block_repeat2) {
ob_start();
echo smarty_function_label(array('name'=>'key'),$_smarty_tpl);?>
&nbsp;&nbsp;<?php echo smarty_function_input(array('name'=>'key','style'=>'width:160px;'),$_smarty_tpl);?>
<br /><br />
            <?php echo smarty_function_submit(array('name'=>'submit'),$_smarty_tpl);?>
&nbsp;&nbsp;&nbsp;<?php if (!empty($_smarty_tpl->tpl_vars['backButton']->value)) {
echo smarty_function_submit(array('name'=>'button'),$_smarty_tpl);
}?>
            <?php $_block_repeat2=false;
echo smarty_block_form(array('name'=>'license-key'), ob_get_clean(), $_smarty_tpl, $_block_repeat2);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

        </div>
        <?php $_block_repeat1=false;
echo smarty_block_block_decorator(array('name'=>'box','addClass'=>'ow_stdmargin','iconClass'=>'ow_ic_plugin','langLabel'=>'admin+item_license_request_box_cap_label'), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

    <?php } else { ?>
        <?php echo $_smarty_tpl->tpl_vars['message']->value;?>

    <?php }?>
</div><?php }
}
