<?php
/* Smarty version 3.1.30, created on 2018-07-19 05:15:11
  from "/var/www/biyebiye/public_html/ow_themes/trend/master_pages/index.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b50569f138294_27091856',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7ee5f0d194a9c6352cfb7564fc63cd2c204c1483' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_themes/trend/master_pages/index.html',
      1 => 1479204282,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b50569f138294_27091856 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_component')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.component.php';
if (!is_callable('smarty_block_block_decorator')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.block_decorator.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_decorator')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.decorator.php';
?>
<div class="ow_page_wrap">
	<div class="ow_page_padding">
		<div class="ow_site_panel">
			<?php echo smarty_function_component(array('class'=>'BASE_CMP_Console'),$_smarty_tpl);?>

		</div>
		<div class="ow_header">
			<div class="sitelogo_and_join_btn">
				<a href="<?php echo $_smarty_tpl->tpl_vars['siteUrl']->value;?>
" class="sitelogo"></a>
				<?php echo smarty_function_component(array('class'=>'BASE_CMP_JoinButton','cssClass'=>'index_join_button'),$_smarty_tpl);?>

			</div>
            <?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('block_decorator', array('name'=>"box",'addClass'=>"index_qs ow_right"));
$_block_repeat1=true;
echo smarty_block_block_decorator(array('name'=>"box",'addClass'=>"index_qs ow_right"), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

	            <div class="index_qs_cap"><h3><?php echo smarty_function_text(array('key'=>"usearch+quick_search"),$_smarty_tpl);?>
</h3></div>
            	<?php echo smarty_function_component(array('class'=>'USEARCH_CMP_QuickSearch'),$_smarty_tpl);?>

            <?php $_block_repeat1=false;
echo smarty_block_block_decorator(array('name'=>"box",'addClass'=>"index_qs ow_right"), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

		</div>
		<div class="ow_page_container">
			
			<div class="ow_canvas">
				<div class="ow_page ow_bg_color clearfix ow_page_index">
					<?php if (!empty($_smarty_tpl->tpl_vars['heading']->value)) {?><h1 class="ow_stdmargin <?php echo $_smarty_tpl->tpl_vars['heading_icon_class']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['heading']->value;?>
</h1><?php }?>
					<div class="ow_content">
						<?php echo $_smarty_tpl->tpl_vars['content']->value;?>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ow_footer">
	<div class="ow_canvas">
		<div class="ow_page clearfix">
			<?php echo $_smarty_tpl->tpl_vars['bottom_menu']->value;?>

			<div class="ow_copyright">
				<?php echo smarty_function_text(array('key'=>'base+copyright'),$_smarty_tpl);?>

			</div>
			<div style="float:right;">
				<?php echo $_smarty_tpl->tpl_vars['bottomPoweredByLink']->value;?>

			</div>
		</div>
	</div>
</div>    
<?php echo smarty_function_decorator(array('name'=>'floatbox'),$_smarty_tpl);
}
}
