<?php

/**
 * Copyright (c) 2009, Skalfa LLC
 * All rights reserved.
 *
 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is licensed under SkaDate Exclusive License by Skalfa LLC.
 *
 * Full text of this license can be found at http://www.skadate.com/sel.pdf
 */

/**
 * Stripe Billing Service Class.
 *
 * @author Egor Bulgakov <egor.bulgakov@gmail.com>
 * @package ow.ow_plugins.billing_stripe.bol
 * @since 1.0
 */
final class BILLINGSTRIPE_BOL_Service
{
    /**
     * @var BILLINGSTRIPE_BOL_CustomerDao
     */
    private $customerDao;
    /**
     * @var BILLINGSTRIPE_BOL_ChargeDao
     */
    private $chargeDao;
    /**
     * @var BILLINGSTRIPE_BOL_SubscriptionDao
     */
    private $subscriptionDao;

    /**
     * Class instance
     *
     * @var BILLINGSTRIPE_BOL_Service
     */
    private static $classInstance;
    
    /**
     * Class constructor
     */
    private function __construct()
    {
        $this->customerDao = BILLINGSTRIPE_BOL_CustomerDao::getInstance();
        $this->chargeDao = BILLINGSTRIPE_BOL_ChargeDao::getInstance();
        $this->subscriptionDao = BILLINGSTRIPE_BOL_SubscriptionDao::getInstance();
    }

    /**
     * Returns class instance
     *
     * @return BILLINGSTRIPE_BOL_Service
     */
    public static function getInstance()
    {
        if ( null === self::$classInstance )
        {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    /**
     * @param $userId
     * @return BILLINGSTRIPE_BOL_Customer
     */
    public function findCustomerByUserId( $userId )
    {
        if ( !$userId )
        {
            return null;
        }

        return $this->customerDao->findByUserId($userId);
    }

    /**
     * @param $id
     * @return BILLINGSTRIPE_BOL_Customer
     */
    public function findCustomerByStripeId( $id )
    {
        if ( !mb_strlen($id) )
        {
            return null;
        }

        return $this->customerDao->findByStripeId($id);
    }

    /**
     * @param $id
     * @return BILLINGSTRIPE_BOL_Subscription
     */
    public function findSubscriptionByStripeId( $id )
    {
        if ( !mb_strlen($id) )
        {
            return null;
        }

        return $this->subscriptionDao->findByStripeId($id);
    }

    /**
     * @param BILLINGSTRIPE_BOL_Customer $customer
     */
    public function storeCustomer( BILLINGSTRIPE_BOL_Customer $customer )
    {
        $this->customerDao->save($customer);
    }

    /**
     * @param BILLINGSTRIPE_BOL_Charge $charge
     */
    public function storeCharge( BILLINGSTRIPE_BOL_Charge $charge )
    {
        $this->chargeDao->save($charge);
    }

    /**
     * @param BILLINGSTRIPE_BOL_Subscription $subscription
     */
    public function storeSubscription( BILLINGSTRIPE_BOL_Subscription $subscription )
    {
        $this->subscriptionDao->save($subscription);
    }
}