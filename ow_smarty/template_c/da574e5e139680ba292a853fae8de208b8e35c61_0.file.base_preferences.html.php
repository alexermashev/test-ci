<?php
/* Smarty version 3.1.30, created on 2018-07-19 06:02:35
  from "/var/www/biyebiye/public_html/ow_plugins/matchmaking/views/controllers/base_preferences.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5061bb8eefd4_03173287',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'da574e5e139680ba292a853fae8de208b8e35c61' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_plugins/matchmaking/views/controllers/base_preferences.html',
      1 => 1531989433,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b5061bb8eefd4_03173287 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_style')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.style.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_block_form')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_cycle')) require_once '/var/www/biyebiye/public_html/ow_libraries/vendor/smarty/smarty/libs/plugins/function.cycle.php';
if (!is_callable('smarty_function_label')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.label.php';
if (!is_callable('smarty_function_input')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_error')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.error.php';
if (!is_callable('smarty_function_submit')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.submit.php';
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('style', array());
$_block_repeat1=true;
echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

form .ow_matchmaking_distance_field input {
    width: 50px;
    padding-right:10px;
}

form .ow_matchmaking_distance_field {
    padding-right:10px;
}

<?php $_block_repeat1=false;
echo smarty_block_style(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>


<div class="ow_box_empty ow_superwide ow_automargin ow_no_cap ow_break_word" style="">
    <?php if ($_smarty_tpl->tpl_vars['noQuestions']->value) {?>
    <div class="ow_stdmargin">
        <?php echo smarty_function_text(array('key'=>'matchmaking+no_questions'),$_smarty_tpl);?>

    </div>
    <?php } else { ?>
    <div class="ow_stdmargin">
        <?php echo smarty_function_text(array('key'=>'matchmaking+preferences_invitation'),$_smarty_tpl);?>

    </div>

    <?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('name'=>'MATCHMAKING_PreferencesForm'));
$_block_repeat1=true;
echo smarty_block_form(array('name'=>'MATCHMAKING_PreferencesForm'), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

        <table class="ow_table_1 ow_form ow_stdmargin">
            <tbody>
                <tr class="ow_tr_first"><th colspan="3"><?php echo smarty_function_text(array('key'=>"base+questions_section_about_my_match_label"),$_smarty_tpl);?>
</th></tr>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['questionArray']->value, 'questions', false, 'section', 'q', array (
  'last' => true,
  'iteration' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['section']->value => $_smarty_tpl->tpl_vars['questions']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_q']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_q']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_q']->value['iteration'] == $_smarty_tpl->tpl_vars['__smarty_foreach_q']->value['total'];
?>

                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['questions']->value, 'question', false, NULL, 'question', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['question']->value) {
?>
                <?php if ($_smarty_tpl->tpl_vars['question']->value['name'] == 'googlemap_location') {?>
                    <?php $_smarty_tpl->_assignInScope('questionName', 'distance_from_my_location');
?>
                <?php } else { ?>
                    <?php $_smarty_tpl->_assignInScope('questionName', $_smarty_tpl->tpl_vars['question']->value['name']);
?>
                <?php }?>
                
                <tr class="<?php echo smarty_function_cycle(array('values'=>'ow_alt1, ow_alt2'),$_smarty_tpl);?>
 <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_q']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_q']->value['last'] : null)) {?>ow_tr_last<?php }?>">
                    <td class="ow_label"><?php echo smarty_function_label(array('name'=>$_smarty_tpl->tpl_vars['questionName']->value),$_smarty_tpl);?>
</td>
                    <td class="ow_value"><?php echo smarty_function_input(array('name'=>$_smarty_tpl->tpl_vars['questionName']->value),$_smarty_tpl);?>
<div style="height:1px;"></div><?php echo smarty_function_error(array('name'=>$_smarty_tpl->tpl_vars['questionName']->value),$_smarty_tpl);?>
</td>
                    <td class="ow_desc ow_small ow_txtcenter"><?php echo $_smarty_tpl->tpl_vars['question']->value['acc_type_label'];?>
</td>
                </tr>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>


                <tr class="ow_tr_delimiter"><td></td></tr>
            </tbody>
        </table>

        <!------------ ---------->

        <div class="clearfix ow_stdmargin">
            <div class="ow_right">
                <?php echo smarty_function_submit(array('name'=>'save','class'=>'ow_ic_save'),$_smarty_tpl);?>

            </div>
        </div>
    <?php $_block_repeat1=false;
echo smarty_block_form(array('name'=>'MATCHMAKING_PreferencesForm'), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

    <?php }?>
</div><?php }
}
