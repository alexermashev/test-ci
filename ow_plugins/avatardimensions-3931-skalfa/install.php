<?php

$pluginKey = 'avatardimensions';

$plugin = OW::getPluginManager()->getPlugin($pluginKey);
OW::getLanguage()->importLangsFromDir($plugin->getRootDir() . 'langs');

OW::getPluginManager()->addPluginSettingsRouteName($pluginKey, 'avatardimensions-admin-settings');

if ( !OW::getConfig()->configExists($pluginKey, 'custom_avatar_size') )
{
    OW::getConfig()->addConfig($pluginKey, 'custom_avatar_size', 250, 'Custom avatar size');
}