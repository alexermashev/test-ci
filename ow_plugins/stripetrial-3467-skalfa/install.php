<?php

$pluginKey = 'stripetrial';

$plugin = OW::getPluginManager()->getPlugin($pluginKey);
OW::getLanguage()->importLangsFromDir($plugin->getRootDir() . 'langs');

$createTableTrialQuery = "
  DROP TABLE IF EXISTS `" . OW_DB_PREFIX . $pluginKey . "_trial`;
  CREATE TABLE IF NOT EXISTS `" . OW_DB_PREFIX . $pluginKey . "_trial` (
      `id` int(11) NOT NULL auto_increment,
      `userId` int(11) NOT NULL,
      `status` int(11) DEFAULT 0,
  PRIMARY KEY  (`id`),
  KEY `userId` (`userId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";

try
{
    OW::getDbo()->query( $createTableTrialQuery );
}
catch ( Exception $e )
{
    OW::getLogger()->addEntry( $e->getTraceAsString(), 'plugin_install_error' );
}