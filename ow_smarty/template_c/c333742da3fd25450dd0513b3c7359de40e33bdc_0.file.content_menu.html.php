<?php
/* Smarty version 3.1.30, created on 2018-07-29 08:28:00
  from "/var/www/biyebiye/public_html/ow_system_plugins/base/mobile/views/components/content_menu.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5db2d00102b2_75983277',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c333742da3fd25450dd0513b3c7359de40e33bdc' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_system_plugins/base/mobile/views/components/content_menu.html',
      1 => 1479204252,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b5db2d00102b2_75983277 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="owm_content_menu_wrap owm_padding">
	<ul class="owm_content_menu clearfix">
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value, 'item', false, NULL, 'contentMenu', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
?><li class="<?php echo $_smarty_tpl->tpl_vars['item']->value['class'];?>
 <?php if ($_smarty_tpl->tpl_vars['item']->value['active']) {?> active<?php }?>"><a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['url'];?>
"><span><?php echo $_smarty_tpl->tpl_vars['item']->value['label'];?>
</span></a></li><?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

	</ul>
</div><?php }
}
