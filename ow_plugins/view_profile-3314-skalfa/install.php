<?php

$pluginKey = 'viewprofile';

$plugin = OW::getPluginManager()->getPlugin($pluginKey);
OW::getLanguage()->importLangsFromDir($plugin->getRootDir() . 'langs');

// permissions
$authorization = OW::getAuthorization();
$authorization->addGroup($pluginKey);
$authorization->addAction($pluginKey, 'view_profile');
$authorization->addAction($pluginKey, 'send_profile');

$createTableLastVisitQuery = "
  DROP TABLE IF EXISTS `" . OW_DB_PREFIX . $pluginKey . "_last_visit`;
  CREATE TABLE IF NOT EXISTS `" . OW_DB_PREFIX . $pluginKey . "_last_visit` (
      `id` int(11) NOT NULL auto_increment,
      `userId` int(11) NOT NULL,
      `dateTime` int(11) DEFAULT NULL,
      `completeTime` int(11) DEFAULT NULL,
  PRIMARY KEY  (`id`),
  KEY `userId` (`userId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";

$createTableSendProfileQuery = "
  DROP TABLE IF EXISTS `" . OW_DB_PREFIX . $pluginKey . "_send_profile`;
  CREATE TABLE IF NOT EXISTS `" . OW_DB_PREFIX . $pluginKey . "_send_profile` (
      `id` int(11) NOT NULL auto_increment,
      `userId` int(11) NOT NULL,
      `whoseId` int(11) NOT NULL,
      `email` varchar(200) NOT NULL,
      `status` tinyint(3) NOT NULL DEFAULT 0,
      `comment` text DEFAULT NULL,
      `hash` varchar(200) NOT NULL,
      `createdDatetime` int(11) DEFAULT NULL,
  PRIMARY KEY  (`id`),
  KEY `userId` (`userId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";

try
{
    OW::getDbo()->query( $createTableLastVisitQuery );
    OW::getDbo()->query( $createTableSendProfileQuery );
}
catch ( Exception $e )
{
    OW::getLogger()->addEntry( $e->getTraceAsString(), 'plugin_install_error' );
}