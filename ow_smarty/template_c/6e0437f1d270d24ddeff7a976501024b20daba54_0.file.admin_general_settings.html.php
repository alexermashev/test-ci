<?php
/* Smarty version 3.1.30, created on 2018-07-19 04:48:07
  from "/var/www/biyebiye/public_html/ow_plugins/usearch/views/controllers/admin_general_settings.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b505047bc7d79_11693647',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6e0437f1d270d24ddeff7a976501024b20daba54' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_plugins/usearch/views/controllers/admin_general_settings.html',
      1 => 1479204280,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b505047bc7d79_11693647 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_form')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_label')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.label.php';
if (!is_callable('smarty_function_input')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_error')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.error.php';
if (!is_callable('smarty_function_submit')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.submit.php';
echo $_smarty_tpl->tpl_vars['contentMenu']->value;?>

<?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('name'=>'usearch_general_settings'));
$_block_repeat1=true;
echo smarty_block_form(array('name'=>'usearch_general_settings'), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

    <table class="ow_table_1 ow_form">
        <tr class="ow_tr_first">
            <th class="ow_name ow_txtleft" colspan="3">
                <span class="ow_section_icon ow_ic_file"><?php echo smarty_function_text(array('key'=>'usearch+search_options'),$_smarty_tpl);?>
</span>
            </th>
        </tr>
        <tr>
            <td class="ow_label"><?php echo smarty_function_label(array('name'=>'enable_username_search'),$_smarty_tpl);?>
</td>
            <td class="ow_value"><?php echo smarty_function_input(array('name'=>'enable_username_search'),$_smarty_tpl);?>
 <?php echo smarty_function_error(array('name'=>'enable_username_search'),$_smarty_tpl);?>
</td>
            <td class="ow_desc">&nbsp;</td>
        </tr>
        <tr class="ow_tr_first">
            <th class="ow_name ow_txtleft" colspan="3">
                <span class="ow_section_icon ow_ic_file"><?php echo smarty_function_text(array('key'=>'usearch+sort_setting'),$_smarty_tpl);?>
</span>
            </th>
        </tr>
        <tr >
            <td class="ow_label"><?php echo smarty_function_label(array('name'=>'latest_activity'),$_smarty_tpl);?>
</td>
            <td class="ow_value"><?php echo smarty_function_input(array('name'=>'latest_activity'),$_smarty_tpl);?>
 <?php echo smarty_function_error(array('name'=>'latest_activity'),$_smarty_tpl);?>
</td>
            <td class="ow_desc">&nbsp;</td>
        </tr>
        <tr >
            <td class="ow_label"><?php echo smarty_function_label(array('name'=>'recently_joined'),$_smarty_tpl);?>
</td>
            <td class="ow_value"><?php echo smarty_function_input(array('name'=>'recently_joined'),$_smarty_tpl);?>
 <?php echo smarty_function_error(array('name'=>'recently_joined'),$_smarty_tpl);?>
</td>
            <td class="ow_desc">&nbsp;</td>
        </tr>
        <tr >
            <td class="ow_label"><?php echo smarty_function_label(array('name'=>'match_compatibitity'),$_smarty_tpl);?>
</td>
            <td class="ow_value"><?php echo smarty_function_input(array('name'=>'match_compatibitity'),$_smarty_tpl);?>
 <?php echo smarty_function_error(array('name'=>'match_compatibitity'),$_smarty_tpl);?>
</td>
            <td class="ow_desc"><?php echo smarty_function_text(array('key'=>"usearch+match_compatibitity_description"),$_smarty_tpl);?>
</td>
        </tr>
        <tr>
            <td class="ow_label"><?php echo smarty_function_label(array('name'=>'distance'),$_smarty_tpl);?>
</td>
            <td class="ow_value"><?php echo smarty_function_input(array('name'=>'distance'),$_smarty_tpl);?>
 <?php echo smarty_function_error(array('name'=>'distance'),$_smarty_tpl);?>
</td>
            <td class="ow_desc"><?php echo smarty_function_text(array('key'=>"usearch+distance_description"),$_smarty_tpl);?>
</td>
        </tr>
        <tr class="ow_tr_delimiter"><td colspan="3" ></td></tr>
        <tr class="ow_tr_first">
            <th class="ow_name ow_txtleft" colspan="3">
                <span class="ow_section_icon ow_ic_picture"><?php echo smarty_function_text(array('key'=>'usearch+view_settings'),$_smarty_tpl);?>
</span>
            </th>
        </tr>
        <tr>
            <td class="ow_label"><?php echo smarty_function_label(array('name'=>'hide_user_activity_after'),$_smarty_tpl);?>
</td>
            <td class="ow_value"><?php echo smarty_function_input(array('name'=>'hide_user_activity_after'),$_smarty_tpl);?>
&nbsp;<span><?php echo smarty_function_text(array('key'=>"usearch+days_ago"),$_smarty_tpl);?>
</span> <?php echo smarty_function_error(array('name'=>'hide_user_activity_after'),$_smarty_tpl);?>
</td>
            <td class="ow_desc">&nbsp;</td>
        </tr>
    </table>
<div class="clearfix ow_stdmargin"><div class="ow_right"><?php echo smarty_function_submit(array('name'=>'save','class'=>'ow_ic_save ow_positive'),$_smarty_tpl);?>
</div></div>
<?php $_block_repeat1=false;
echo smarty_block_form(array('name'=>'usearch_general_settings'), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);
}
}
