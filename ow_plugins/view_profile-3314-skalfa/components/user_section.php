<?php

class VIEWPROFILE_CMP_UserSection extends OW_Component
{
    private $user;

    public function __construct( $userId )
    {
        parent::__construct();

        $this->user = BOL_UserService::getInstance()->findUserById($userId);
    }

    public function onBeforeRender()
    {
        parent::onBeforeRender();

        $viewerId = OW::getUser()->getId();

        $questionService = BOL_QuestionService::getInstance();
        $userId = $this->user->getId();
        $accountType = $this->user->accountType;
        $ownerMode = $userId == $viewerId;
        $adminMode = OW::getUser()->isAdmin() || OW::getUser()->isAuthorized('base');
        $isSuperAdmin = BOL_AuthorizationService::getInstance()->isSuperModerator($userId);

        $template = OW::getPluginManager()->getPlugin('viewprofile')->getViewDir() . 'components' . DS . 'user_section_table.html';

        $this->setTemplate($template);

        $questions = BASE_CMP_UserViewWidget::getUserViewQuestions($userId, false);

        $sectionsHtml = $questions['sections'];

        $sections = array_keys($sectionsHtml);

        $accountTypes = $questionService->findAllAccountTypes();

        if ( !isset($sections[0]) )
        {
            $sections[0] = 0;
        }

        if ( count($accountTypes) > 1 )
        {
            if ( !isset($questionArray[$sections[0]]) )
            {
                $questionArray[$sections[0]] = array();
            }

            array_unshift($questionArray[$sections[0]], array('name' => 'accountType', 'presentation' => 'select'));
            $questionData[$userId]['accountType'] = $questionService->getAccountTypeLang($accountType);
        }

        if ( !isset($questionData[$userId]) )
        {
            $questionData[$userId] = array();
        }

        $this->assign('firstSection', $sections[0]);
        $this->assign('sectionsHtml', $sectionsHtml);
        $this->assign('ownerMode', $ownerMode);
        $this->assign('adminMode', $adminMode);
        $this->assign('superAdminProfile', $isSuperAdmin);
        $this->assign('profileEditUrl', OW::getRouter()->urlForRoute('base_edit'));

        if ( $adminMode && !$ownerMode )
        {
            $this->assign('profileEditUrl', OW::getRouter()->urlForRoute('base_edit_user_datails', array('userId' => $userId) ));
        }

        $this->assign('avatarUrl', BOL_AvatarService::getInstance()->getAvatarUrl($userId) );
        $this->assign('displayName', BOL_UserService::getInstance()->getDisplayName($userId) );
        $this->assign('userId', $userId);
    }

}