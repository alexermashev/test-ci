<?php

/**
 * Copyright (c) 2015, Skalfa LLC
 * All rights reserved.
 *
 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is licensed under SkaDate Exclusive License by Skalfa LLC.
 *
 * Full text of this license can be found at http://www.skadate.com/sel.pdf
 */

/**
 * VideoIm cron job.
 *
 * @author Alex Ermashev <alexermashev@gmail.com>
 * @package ow.ow_plugins.videoim
 * @since 8.1
 */
class VIDEOIM_Cron extends OW_Cron
{
    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->addJob('deleteExpiredNotifications', 60);
    }

    /**
     * Do nothing
     */
    public function run()
    {}

    /**
     * Delete expired notifications
     */
    public function deleteExpiredNotifications()
    {
        VIDEOIM_BOL_VideoImService::getInstance()->deleteExpiredNotifications();
    }
}