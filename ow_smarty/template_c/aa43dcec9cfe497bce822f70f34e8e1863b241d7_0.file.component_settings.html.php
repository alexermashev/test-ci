<?php
/* Smarty version 3.1.30, created on 2018-07-20 02:05:35
  from "/var/www/biyebiye/public_html/ow_system_plugins/base/views/components/component_settings.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b517bafb513b4_50900916',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'aa43dcec9cfe497bce822f70f34e8e1863b241d7' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_system_plugins/base/views/components/component_settings.html',
      1 => 1479204252,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b517bafb513b4_50900916 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_cycle')) require_once '/var/www/biyebiye/public_html/ow_libraries/vendor/smarty/smarty/libs/plugins/function.cycle.php';
if (!is_callable('smarty_block_block_decorator')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.block_decorator.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
?>

<?php if ($_smarty_tpl->tpl_vars['settings']->value) {?>
<table class="ow_table_1 ow_small ow_form ow_smallmargin">
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['settings']->value, 'item', false, 'name', 'i', array (
  'first' => true,
  'last' => true,
  'index' => true,
  'iteration' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['name']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_i']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_i']->value['index']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_i']->value['first'] = !$_smarty_tpl->tpl_vars['__smarty_foreach_i']->value['index'];
$_smarty_tpl->tpl_vars['__smarty_foreach_i']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_i']->value['iteration'] == $_smarty_tpl->tpl_vars['__smarty_foreach_i']->value['total'];
?>
        <?php if (!in_array($_smarty_tpl->tpl_vars['name']->value,$_smarty_tpl->tpl_vars['hidden']->value)) {?>
		    <tr class="<?php if (!empty($_smarty_tpl->tpl_vars['item']->value['class'])) {
echo $_smarty_tpl->tpl_vars['item']->value['class'];
}?> <?php if ($_smarty_tpl->tpl_vars['item']->value['display'] == 'table') {
echo smarty_function_cycle(array('name'=>"custom",'values'=>"ow_alt2,ow_alt1"),$_smarty_tpl);
}?> <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_i']->value['first']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_i']->value['first'] : null)) {?>ow_tr_first<?php }
if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_i']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_i']->value['last'] : null)) {?>ow_tr_last<?php }?>">
                        <?php if (!empty($_smarty_tpl->tpl_vars['item']->value['label'])) {?>
                            <td class="ow_label"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['label'], ENT_QUOTES, 'UTF-8', true);?>
</td>
                            <td class="ow_value">
                        <?php } else { ?>
                            <td colspan="2">
                        <?php }?>

                            <?php if ($_smarty_tpl->tpl_vars['item']->value['presentation'] == 'custom') {?>
                                <?php echo $_smarty_tpl->tpl_vars['item']->value['markup'];?>

		            <?php } elseif ($_smarty_tpl->tpl_vars['item']->value['presentation'] == 'number') {?>
		                <input type="text" name="<?php echo $_smarty_tpl->tpl_vars['name']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['item']->value['value'];?>
" />
		            <?php } elseif ($_smarty_tpl->tpl_vars['item']->value['presentation'] == 'text') {?>
		                <input type="text" name="<?php echo $_smarty_tpl->tpl_vars['name']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['item']->value['value'];?>
" />
		            <?php } elseif ($_smarty_tpl->tpl_vars['item']->value['presentation'] == 'textarea') {?>
		                <textarea name="<?php echo $_smarty_tpl->tpl_vars['name']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['item']->value['value'];?>
</textarea>
		            <?php } elseif ($_smarty_tpl->tpl_vars['item']->value['presentation'] == 'checkbox') {?>
		                <input type="checkbox" name="<?php echo $_smarty_tpl->tpl_vars['name']->value;?>
"<?php if ($_smarty_tpl->tpl_vars['item']->value['value']) {?> checked="checked"<?php }?> />
		            <?php } elseif ($_smarty_tpl->tpl_vars['item']->value['presentation'] == 'select') {?>
		                <select name="<?php echo $_smarty_tpl->tpl_vars['name']->value;?>
">
		                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['item']->value['optionList'], 'label', false, 'value');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['value']->value => $_smarty_tpl->tpl_vars['label']->value) {
?>
		                        <option value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
"<?php if ($_smarty_tpl->tpl_vars['value']->value == $_smarty_tpl->tpl_vars['item']->value['value']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['label']->value;?>
</option>
		                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

		                </select>
		            <?php }?>
		            <div id="error_<?php echo $_smarty_tpl->tpl_vars['name']->value;?>
" class="setting_error ow_error error"></div>
		        </td>
		    </tr>
        <?php }?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</table>
<?php }?>

<?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('block_decorator', array('iconClass'=>'ow_ic_gear_wheel','langLabel'=>'base+widgets_fb_default_settings_label','name'=>'box','type'=>"empty",'addClass'=>"clearfix"));
$_block_repeat1=true;
echo smarty_block_block_decorator(array('iconClass'=>'ow_ic_gear_wheel','langLabel'=>'base+widgets_fb_default_settings_label','name'=>'box','type'=>"empty",'addClass'=>"clearfix"), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>


    <table class="ow_table_1 ow_small ow_form ow_smallmargin">
        <?php if (!in_array('title',$_smarty_tpl->tpl_vars['hidden']->value)) {?>
	        <tr class="<?php echo smarty_function_cycle(array('name'=>"system",'values'=>"ow_alt2,ow_alt1"),$_smarty_tpl);?>
 ow_tr_first <?php if (in_array('freeze',$_smarty_tpl->tpl_vars['hidden']->value) && in_array('wrap_in_box',$_smarty_tpl->tpl_vars['hidden']->value) && in_array('show_title',$_smarty_tpl->tpl_vars['hidden']->value)) {?> ow_tr_last<?php }?>">
	            <td class="ow_label"><?php echo smarty_function_text(array('key'=>"base+widgets_default_settings_title"),$_smarty_tpl);?>
</td>
	            <td class="ow_value">
	                <input type="text" name="title" <?php if (isset($_smarty_tpl->tpl_vars['values']->value['title'])) {?>value="<?php echo $_smarty_tpl->tpl_vars['values']->value['title'];?>
" beforevalue="<?php echo $_smarty_tpl->tpl_vars['values']->value['title'];?>
"<?php }?> />
	                <div id="error_title" class="setting_error ow_error error"></div>
	            </td>
	        </tr>
        <?php }?>
        <?php if (!in_array('show_title',$_smarty_tpl->tpl_vars['hidden']->value)) {?>
	        <tr class="<?php echo smarty_function_cycle(array('name'=>"system",'values'=>"ow_alt2,ow_alt1"),$_smarty_tpl);
if (in_array('freeze',$_smarty_tpl->tpl_vars['hidden']->value) && in_array('wrap_in_box',$_smarty_tpl->tpl_vars['hidden']->value)) {?> ow_tr_last<?php }?>">
	            <td class="ow_label"><?php echo smarty_function_text(array('key'=>"base+widgets_default_settings_show_title"),$_smarty_tpl);?>
</td>
	            <td class="ow_value">
	                <input type="checkbox" name="show_title" <?php if (isset($_smarty_tpl->tpl_vars['values']->value['show_title']) && $_smarty_tpl->tpl_vars['values']->value['show_title']) {?>checked="checked"<?php }?> />
                        <?php if (!in_array("icon",$_smarty_tpl->tpl_vars['hidden']->value)) {?>
                            <?php echo smarty_function_text(array('key'=>"base+widgets_default_settings_icon"),$_smarty_tpl);?>
:
                            <select class="choose_icon" name="icon" >
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['avaliableIcons']->value, 'icon');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['icon']->value) {
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['icon']->value['class'];?>
" <?php if (isset($_smarty_tpl->tpl_vars['values']->value['icon']) && $_smarty_tpl->tpl_vars['values']->value['icon'] == $_smarty_tpl->tpl_vars['icon']->value['class']) {?>selected="selected"<?php }?> >
                                        <?php echo $_smarty_tpl->tpl_vars['icon']->value['label'];?>

                                    </option>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                            </select>
                        <?php }?>
	            </td>
	        </tr>
        <?php }?>
        <?php if (!in_array('wrap_in_box',$_smarty_tpl->tpl_vars['hidden']->value)) {?>
	        <tr class="<?php echo smarty_function_cycle(array('name'=>"system",'values'=>"ow_alt2,ow_alt1"),$_smarty_tpl);?>
 <?php if (in_array('freeze',$_smarty_tpl->tpl_vars['hidden']->value) && in_array('restrict_view',$_smarty_tpl->tpl_vars['hidden']->value)) {?> ow_tr_last<?php }?>">
	            <td class="ow_label"><?php echo smarty_function_text(array('key'=>"base+widgets_default_settings_wib"),$_smarty_tpl);?>
</td>
	            <td class="ow_value">
	                <input type="checkbox" name="wrap_in_box" <?php if (isset($_smarty_tpl->tpl_vars['values']->value['wrap_in_box']) && $_smarty_tpl->tpl_vars['values']->value['wrap_in_box']) {?>checked="checked"<?php }?> />
	            </td>
	        </tr>
        <?php }?>
        <?php if (!in_array('freeze',$_smarty_tpl->tpl_vars['hidden']->value)) {?>
	        <tr class="<?php echo smarty_function_cycle(array('name'=>"system",'values'=>"ow_alt2,ow_alt1"),$_smarty_tpl);?>
 <?php if (in_array('restrict_view',$_smarty_tpl->tpl_vars['hidden']->value)) {?>ow_tr_last<?php }?>">
	            <td class="ow_label"><?php echo smarty_function_text(array('key'=>"base+widgets_default_settings_freeze"),$_smarty_tpl);?>
</td>
	            <td class="ow_value">
	                <input type="checkbox" name="freeze" <?php if (isset($_smarty_tpl->tpl_vars['values']->value['freeze']) && $_smarty_tpl->tpl_vars['values']->value['freeze']) {?>checked="checked"<?php }?> />
	            </td>
	        </tr>
        <?php }?>

        <?php if (!in_array('restrict_view',$_smarty_tpl->tpl_vars['hidden']->value)) {?>
	        <tr class="<?php echo smarty_function_cycle(array('name'=>"system",'values'=>"ow_alt2,ow_alt1"),$_smarty_tpl);?>
 ow_tr_last" id="ws_restrict_view">
	            <td class="ow_label"><?php echo smarty_function_text(array('key'=>"base+widgets_default_settings_restrict_view"),$_smarty_tpl);?>
</td>
	            <td class="ow_value">
	                <input type="checkbox" name="restrict_view" <?php if (isset($_smarty_tpl->tpl_vars['values']->value['restrict_view']) && $_smarty_tpl->tpl_vars['values']->value['restrict_view']) {?>checked="checked"<?php }?> onclick="$('#ws_access_restrictions')[this.checked ? 'show' : 'hide'](); $('#ws_restrict_view')[this.checked ? 'removeClass' : 'addClass']('ow_tr_last');" />
	            </td>
	        </tr>
                <tr class="<?php echo smarty_function_cycle(array('name'=>"system",'values'=>"ow_alt2,ow_alt1"),$_smarty_tpl);?>
 ow_tr_last" id="ws_access_restrictions" <?php if (!isset($_smarty_tpl->tpl_vars['values']->value['restrict_view']) || !$_smarty_tpl->tpl_vars['values']->value['restrict_view']) {?>style="display: none;"<?php }?>>
	            <td class="ow_label"><?php echo smarty_function_text(array('key'=>"base+widgets_default_settings_access_restrictions"),$_smarty_tpl);?>
</td>
	            <td class="ow_value">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['roleList']->value, 'role');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['role']->value) {
?>
                        <input type="checkbox" class="ow_vertical_middle" value="<?php echo $_smarty_tpl->tpl_vars['role']->value->id;?>
" name="access_restrictions[]" <?php if (!isset($_smarty_tpl->tpl_vars['values']->value['access_restrictions']) || in_array($_smarty_tpl->tpl_vars['role']->value->id,$_smarty_tpl->tpl_vars['values']->value['access_restrictions'])) {?>checked="checked"<?php }?> />
                            <?php echo smarty_function_text(array('key'=>"base+authorization_role_".((string)$_smarty_tpl->tpl_vars['role']->value->name)),$_smarty_tpl);?>

                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

	            </td>
	        </tr>
        <?php }?>
    </table>
<?php $_block_repeat1=false;
echo smarty_block_block_decorator(array('iconClass'=>'ow_ic_gear_wheel','langLabel'=>'base+widgets_fb_default_settings_label','name'=>'box','type'=>"empty",'addClass'=>"clearfix"), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);
}
}
