<?php
/* Smarty version 3.1.30, created on 2018-07-27 05:19:24
  from "/var/www/biyebiye/public_html/ow_plugins/ocs_guests/views/components/my_guests_widget.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5ae39ca82a33_91551694',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '21f7ed9a9c34d0edbd0155241e69ed1f0543d1c3' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_plugins/ocs_guests/views/components/my_guests_widget.html',
      1 => 1479204282,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b5ae39ca82a33_91551694 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_style')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.style.php';
if (!is_callable('smarty_function_decorator')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.decorator.php';
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('style', array());
$_block_repeat1=true;
echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>


    .ow_guest_user {
        display: inline-block;
        margin: 0px 4px 0px 4px;
        text-align: center;
        width: 70px;
        line-height: 11px;
    }
    .ow_guest_time { height: 25px; padding-top: 3px; overflow: hidden; }

<?php $_block_repeat1=false;
echo smarty_block_style(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>


<div class="ow_center">
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['guests']->value, 'g');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['g']->value) {
?>
    <?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'default', 'guestId', null);
echo $_smarty_tpl->tpl_vars['g']->value->guestId;
$_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);
?>

    <div class="ow_guest_user ow_center">
        <?php echo smarty_function_decorator(array('name'=>'avatar_item','data'=>$_smarty_tpl->tpl_vars['avatars']->value[$_smarty_tpl->tpl_vars['guestId']->value],'isMarked'=>!empty($_smarty_tpl->tpl_vars['avatars']->value[$_smarty_tpl->tpl_vars['guestId']->value]['isMarked'])),$_smarty_tpl);?>

        <div class="ow_guest_time ow_tiny"><?php echo $_smarty_tpl->tpl_vars['g']->value->visitTimestamp;?>
<br /><br />&nbsp;</div>
    </div>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</div><?php }
}
