<?php
/* Smarty version 3.1.30, created on 2018-07-28 10:29:05
  from "/var/www/biyebiye/public_html/ow_system_plugins/base/mobile/views/controllers/user_forgot_password.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5c7db16e1103_34109164',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cbb45af5eaa911eeef097be82155a97ce805da16' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_system_plugins/base/mobile/views/controllers/user_forgot_password.html',
      1 => 1479204252,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b5c7db16e1103_34109164 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_form')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_input')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_submit')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.submit.php';
if (!is_callable('smarty_function_component')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.component.php';
?>
<div class="owm_std_margin_top owm_blank_content">
    <div class="owm_std_margin_bottom">
        <?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('name'=>'forgot-password'));
$_block_repeat1=true;
echo smarty_block_form(array('name'=>'forgot-password'), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

        <div class="owm_login_txt">
            <?php echo smarty_function_text(array('key'=>'base+forgot_password_form_text'),$_smarty_tpl);?>

        </div>
        <div class="owm_login_field owm_login_pass">
            <?php echo smarty_function_input(array('name'=>'email','class'=>'ow_smallmargin'),$_smarty_tpl);?>

        </div>
        <div class="owm_btn_wide owm_btn_positive owm_std_margin_top">
            <?php echo smarty_function_submit(array('name'=>'submit','class'=>'ow_positive'),$_smarty_tpl);?>

        </div>
        <?php $_block_repeat1=false;
echo smarty_block_form(array('name'=>'forgot-password'), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

        <div class="owm_std_margin_top">
        <?php echo smarty_function_component(array('class'=>"BASE_MCMP_ConnectButtonList"),$_smarty_tpl);?>

        </div>
    </div>
</div><?php }
}
