<?php

class AVATARDIMENSIONS_BOL_Service
{
    const PLUGIN_KEY = 'avatardimensions';

    /**
     * @var BOL_AvatarService
     */
    private $avatarService;
    
    /**
     * Constructor.
     */
    private function __construct()
    {
        $this->avatarService = BOL_AvatarService::getInstance();
    }
    /**
     * Singleton instance.
     *
     * @var AVATARDIMENSIONS_BOL_Service
     */
    private static $classInstance;

    /**
     * Returns an instance of class (singleton pattern implementation).
     *
     * @return AVATARDIMENSIONS_BOL_Service
     */
    public static function getInstance()
    {
        if ( self::$classInstance === null )
        {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    /**
     * Crops user avatar using coordinates
     *
     * @param int $userId
     * @param $path
     * @param array $coords
     * @param int $viewSize
     * @return bool
     */
    public function cropAvatar( $userId, $path, $coords, $viewSize, array $editionalParams = array() )
    {
        $this->avatarService->deleteUserAvatar($userId);

        $avatar = new BOL_Avatar();
        $avatar->userId = $userId;
        $avatar->hash = time();

        $this->avatarService->updateAvatar($avatar);

        $params = array(
            'avatarId' => $avatar->id,
            'userId' => $userId,
            'trackAction' => isset($editionalParams['trackAction'])  ? $editionalParams['trackAction'] : true
        );

        $event = new OW_Event('base.after_avatar_update', array_merge($editionalParams, $params));
        OW::getEventManager()->trigger($event);

        // destination path
        $avatarPath = $this->avatarService->getAvatarPath($userId, 1, $avatar->hash);
        $avatarBigPath = $this->avatarService->getAvatarPath($userId, 2, $avatar->hash);
        $avatarOriginalPath = $this->avatarService->getAvatarPath($userId, 3, $avatar->hash);

        // pluginfiles tmp path
        $avatarPFPath = $this->avatarService->getAvatarPluginFilesPath($userId, 1, $avatar->hash);
        $avatarPFBigPath = $this->avatarService->getAvatarPluginFilesPath($userId, 2, $avatar->hash);
        $avatarPFOriginalPath = $this->avatarService->getAvatarPluginFilesPath($userId, 3, $avatar->hash);

        if ( !is_writable(dirname($avatarPFPath)) )
        {
            $this->avatarService->deleteUserAvatar($userId);

            return false;
        }

        $storage = OW::getStorage();

        if ( !empty($editionalParams['isLocalFile']) )
        {
            $toFilePath = $path;
        }
        else
        {
            $toFilePath = OW::getPluginManager()->getPlugin('base')->getPluginFilesDir() . uniqid(md5( rand(0,9999999999) )).UTIL_File::getExtension($path);

            $storage->copyFileToLocalFS($path, $toFilePath);
        }

        $result = true;
        try
        {
            $image = new UTIL_Image($toFilePath);

            $width = $image->getWidth();
            $k = $width / $viewSize;

            $config = OW::getConfig();
            $avatarSize = (int) $config->getValue('base', 'avatar_size');
            $bigAvatarSize = (int) $config->getValue('base', 'avatar_big_size');

            $customMinSize = $config->getValue(AVATARDIMENSIONS_BOL_Service::PLUGIN_KEY, 'custom_avatar_size');

            $bigAvatarSize = $customMinSize;
            $avatarSize = $customMinSize;

            $image->copyImage($avatarPFOriginalPath)
                ->cropImage($coords['x'] * $k, $coords['y'] * $k, $coords['w'] * $k, $coords['h'] * $k)
                ->resizeImage($bigAvatarSize, $bigAvatarSize, true)
                ->saveImage($avatarPFBigPath)
                ->resizeImage($avatarSize, $avatarSize, true)
                ->saveImage($avatarPFPath);

            $storage->copyFile($avatarPFOriginalPath, $avatarOriginalPath);
            $storage->copyFile($avatarPFBigPath, $avatarBigPath);
            $storage->copyFile($avatarPFPath, $avatarPath);
        }
        catch (Exception $ex)
        {
            $result = false;
        }

        @unlink($avatarPFPath);
        @unlink($avatarPFBigPath);
        @unlink($avatarPFOriginalPath);
        @unlink($toFilePath);

        return $result;
    }

    public function cropTempAvatar( $key, $coords, $viewSize )
    {
        $originalPath = $this->avatarService->getTempAvatarPath($key, 3);
        $bigAvatarPath = $this->avatarService->getTempAvatarPath($key, 2);
        $avatarPath = $this->avatarService->getTempAvatarPath($key, 1);

        $image = new UTIL_Image($originalPath);

        $width = $image->getWidth();

        $k = $width / $viewSize;

        $config = OW::getConfig();
        $avatarSize = (int) $config->getValue('base', 'avatar_size');
        $bigAvatarSize = (int) $config->getValue('base', 'avatar_big_size');

        $customMinSize = $config->getValue(AVATARDIMENSIONS_BOL_Service::PLUGIN_KEY, 'custom_avatar_size');

        $bigAvatarSize = $customMinSize;
        $avatarSize = $customMinSize;

        $image->cropImage($coords['x'] * $k, $coords['y'] * $k, $coords['w'] * $k, $coords['h'] * $k)
            ->resizeImage($bigAvatarSize, $bigAvatarSize, true)
            ->saveImage($bigAvatarPath)
            ->resizeImage($avatarSize, $avatarSize, true)
            ->saveImage($avatarPath);

        return true;
    }

    public function createAvatar( $userId, $isModerable = true, $trackAction = true)
    {
        $key = $this->avatarService->getAvatarChangeSessionKey();
        $path = $this->avatarService->getTempAvatarPath($key, 2);

        if ( !file_exists($path) )
        {
            return false;
        }

        if ( !UTIL_File::validateImage($path) )
        {
            return false;
        }

        $event = new OW_Event('base.before_avatar_change', array(
            'userId' => $userId,
            'avatarId' => null,
            'upload' => true,
            'crop' => false,
            'isModerable' => $isModerable
        ));
        OW::getEventManager()->trigger($event);

        $avatarSet = $this->setUserAvatar($userId, $path, array('isModerable' => $isModerable, 'trackAction' => $trackAction ));

        if ( $avatarSet )
        {
            $avatar = $this->avatarService->findByUserId($userId);

            if ( $avatar )
            {
                $event = new OW_Event('base.after_avatar_change', array(
                    'userId' => $userId,
                    'avatarId' => $avatar->id,
                    'upload' => true,
                    'crop' => false
                ));
                OW::getEventManager()->trigger($event);
            }

            $this->avatarService->deleteUserTempAvatar($key);
        }

        return $avatarSet;
    }

    public function setUserAvatar( $userId, $uploadedFileName, array $editionalParams = array() )
    {
        $avatar = $this->avatarService->findByUserId($userId);

        if ( !$avatar )
        {
            $avatar = new BOL_Avatar();
            $avatar->userId = $userId;
        }
        else
        {
            $oldHash = $avatar->hash;
        }

        $avatar->hash = time();

        // destination path
        $avatarPath = $this->avatarService->getAvatarPath($userId, 1, $avatar->hash);
        $avatarBigPath = $this->avatarService->getAvatarPath($userId, 2, $avatar->hash);
        $avatarOriginalPath = $this->avatarService->getAvatarPath($userId, 3, $avatar->hash);

        // pluginfiles tmp path
        $avatarPFPath = $this->avatarService->getAvatarPluginFilesPath($userId, 1, $avatar->hash);
        $avatarPFBigPath = $this->avatarService->getAvatarPluginFilesPath($userId, 2, $avatar->hash);
        $avatarPFOriginalPath = $this->avatarService->getAvatarPluginFilesPath($userId, 3, $avatar->hash);

        if ( !is_writable(dirname($avatarPFPath)) )
        {
            return false;
        }

        try
        {
            $image = new UTIL_Image($uploadedFileName);
            $image->orientateImage()->copyImage($avatarPFOriginalPath);
            $image = new UTIL_Image($avatarPFOriginalPath);

            $config = OW::getConfig();

            $configAvatarSize = $config->getValue('base', 'avatar_size');
            $configBigAvatarSize = $config->getValue('base', 'avatar_big_size');

            $customMinSize = $config->getValue(AVATARDIMENSIONS_BOL_Service::PLUGIN_KEY, 'custom_avatar_size');

            $configAvatarSize = $customMinSize;
            $configBigAvatarSize = $customMinSize;

            $image
                ->resizeImage($configBigAvatarSize, $configBigAvatarSize, true)
                ->saveImage($avatarPFBigPath)
                ->resizeImage($configAvatarSize, $configAvatarSize, true)
                ->saveImage($avatarPFPath);

            $this->avatarService->updateAvatar($avatar);

            $params = array(
                'avatarId' => $avatar->id,
                'userId' => $userId,
                'trackAction' => isset($editionalParams['trackAction'])  ? $editionalParams['trackAction'] : true
            );

            $event = new OW_Event('base.after_avatar_update', array_merge( $editionalParams, $params) );
            OW::getEventManager()->trigger($event);

            // remove old images
            if ( isset($oldHash) )
            {
                $oldAvatarPath = $this->avatarService->getAvatarPath($userId, 1, $oldHash);
                $oldAvatarBigPath = $this->avatarService->getAvatarPath($userId, 2, $oldHash);
                $oldAvatarOriginalPath = $this->avatarService->getAvatarPath($userId, 3, $oldHash);

                $this->avatarService->removeAvatarImage($oldAvatarPath);
                $this->avatarService->removeAvatarImage($oldAvatarBigPath);
                $this->avatarService->removeAvatarImage($oldAvatarOriginalPath);
            }

            $storage = OW::getStorage();

            $storage->copyFile($avatarPFOriginalPath, $avatarOriginalPath);
            $storage->copyFile($avatarPFBigPath, $avatarBigPath);
            $storage->copyFile($avatarPFPath, $avatarPath);

            @unlink($avatarPFPath);
            @unlink($avatarPFBigPath);
            @unlink($avatarPFOriginalPath);

            return true;
        }
        catch ( Exception $e )
        {
            return false;
        }
    }
}