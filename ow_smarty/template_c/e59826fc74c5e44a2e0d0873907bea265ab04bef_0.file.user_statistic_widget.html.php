<?php
/* Smarty version 3.1.30, created on 2019-01-18 01:23:45
  from "/Users/esase/Sites/8418/ow_system_plugins/admin/views/components/user_statistic_widget.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c4170f1cb0a73_49231899',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e59826fc74c5e44a2e0d0873907bea265ab04bef' => 
    array (
      0 => '/Users/esase/Sites/8418/ow_system_plugins/admin/views/components/user_statistic_widget.html',
      1 => 1547792051,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c4170f1cb0a73_49231899 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_style')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/block.style.php';
if (!is_callable('smarty_block_script')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/block.script.php';
if (!is_callable('smarty_function_url_for_route')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.url_for_route.php';
if (!is_callable('smarty_function_decorator')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.decorator.php';
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('style', array());
$_block_repeat1=true;
echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

    #admin-user-statistics-header #user-statistics-menu {
        float:right;
    }

    #admin-user-statistics-container .ow_ajaxloader_preloader {
        min-height:300px;
    }

    #admin-user-browse {
        float:right;
    }
<?php $_block_repeat1=false;
echo smarty_block_style(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>


<?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('script', array());
$_block_repeat1=true;
echo smarty_block_script(array(), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

    var defaultPeriod = "<?php echo $_smarty_tpl->tpl_vars['defaultPeriod']->value;?>
";

    $("#user-statistics-menu a").on("click", function(){
        defaultPeriod = $(this).attr("id");
        defaultPeriod = defaultPeriod.replace("user_menu_statistics_", "");

        reloadChart();
    });

    /**
     * Reload chart
     *
     * @return void
     */
    function reloadChart()
    {
        if (!defaultPeriod)
        {
            return;
        }

        OW.loadComponent("ADMIN_CMP_UserStatistic",
                [{ "defaultPeriod" : defaultPeriod }], "#admin-user-statistics-container");
    }
<?php $_block_repeat1=false;
echo smarty_block_script(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>


<div id="admin-user-statistics-header">
    <div id="user-statistics-menu">
        <?php echo $_smarty_tpl->tpl_vars['menu']->value;?>

    </div>
    <div class="clearfix"></div>
</div>

<div id="admin-user-statistics-container">
    <?php echo $_smarty_tpl->tpl_vars['statistics']->value;?>

</div>

<div id="admin-user-browse">
    <?php ob_start();
echo smarty_function_url_for_route(array('for'=>'admin_users_browse'),$_smarty_tpl);
$_prefixVariable2=ob_get_clean();
echo smarty_function_decorator(array('name'=>"button_list_item",'langLabel'=>"admin+statistics_browse_users",'onclick'=>"location.href='".$_prefixVariable2."'"),$_smarty_tpl);?>

</div>
<div class="clearfix"></div>
<?php }
}
