<?php
/* Smarty version 3.1.30, created on 2019-01-25 06:41:59
  from "/Users/esase/Sites/8418/ow_system_plugins/base/views/controllers/base_document_password_protection.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c4af6073a7563_30586488',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '78498620ea11911aca86b027ff44d4054bfd44a7' => 
    array (
      0 => '/Users/esase/Sites/8418/ow_system_plugins/base/views/controllers/base_document_password_protection.html',
      1 => 1547792051,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c4af6073a7563_30586488 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_block_form')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_input')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_submit')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.submit.php';
?>
<div class="pass_protection_cont ow_center">
    <h3><?php echo smarty_function_text(array('key'=>'base+password_protection_cap_label'),$_smarty_tpl);?>
</h3>
    <div style="padding: 5px 0;"><?php echo smarty_function_text(array('key'=>'base+password_protection_text'),$_smarty_tpl);?>
</div>
    <?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('name'=>'password_protection'));
$_block_repeat1=true;
echo smarty_block_form(array('name'=>'password_protection'), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

    <div style="padding: 5px 0;"><?php echo smarty_function_input(array('name'=>'password','style'=>'width:200px;'),$_smarty_tpl);?>
&nbsp;<?php echo smarty_function_submit(array('name'=>'submit'),$_smarty_tpl);?>
</div>
    <?php $_block_repeat1=false;
echo smarty_block_form(array('name'=>'password_protection'), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

</div><?php }
}
