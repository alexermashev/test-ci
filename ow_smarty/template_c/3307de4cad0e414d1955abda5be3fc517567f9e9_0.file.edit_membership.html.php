<?php
/* Smarty version 3.1.30, created on 2018-07-19 05:17:00
  from "/var/www/biyebiye/public_html/ow_plugins/membership/views/components/edit_membership.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b50570c052832_25768255',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3307de4cad0e414d1955abda5be3fc517567f9e9' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_plugins/membership/views/components/edit_membership.html',
      1 => 1479204282,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b50570c052832_25768255 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_block_block_decorator')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.block_decorator.php';
if (!is_callable('smarty_function_cycle')) require_once '/var/www/biyebiye/public_html/ow_libraries/vendor/smarty/smarty/libs/plugins/function.cycle.php';
if (!is_callable('smarty_function_decorator')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.decorator.php';
$_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'default', "options", null);
?>

    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['periodUnitsList']->value, 'option');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['option']->value) {
?>
        <option value="<?php echo $_smarty_tpl->tpl_vars['option']->value;?>
"><?php echo smarty_function_text(array('key'=>"membership+".((string)$_smarty_tpl->tpl_vars['option']->value)),$_smarty_tpl);?>
</option>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

<?php $_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);
?>


<?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('block_decorator', array('name'=>'box','type'=>'empty','iconClass'=>'ow_ic_clock','langLabel'=>'membership+plans','addClass'=>"ow_stdmargin"));
$_block_repeat1=true;
echo smarty_block_block_decorator(array('name'=>'box','type'=>'empty','iconClass'=>'ow_ic_clock','langLabel'=>'membership+plans','addClass'=>"ow_stdmargin"), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

<div class="ow_smallmargin" style="padding-left: 4px;"><?php echo smarty_function_text(array('key'=>'membership+manage_membership_plans','membership'=>$_smarty_tpl->tpl_vars['membership']->value),$_smarty_tpl);?>
</div>

<form method="post" id="plans-form">
    <input type="hidden" name="form_name" value="edit-plans-form" />
    <input type="hidden" name="type_id" value="<?php echo $_smarty_tpl->tpl_vars['typeId']->value;?>
" />
    <table id="plans" class="ow_table_1 ow_form ow_center ow_smallmargin">
        <tr class="ow_tr_first">
            <th width="1"></th>
            <th><?php echo smarty_function_text(array('key'=>'membership+period'),$_smarty_tpl);?>
, <span class="ow_small"><?php echo smarty_function_text(array('key'=>'membership+days'),$_smarty_tpl);?>
</span></th>
            <th><?php echo smarty_function_text(array('key'=>'membership+price'),$_smarty_tpl);?>
, <span class="ow_small"><?php echo $_smarty_tpl->tpl_vars['currency']->value;?>
</span></th>
            <th width="1"><?php echo smarty_function_text(array('key'=>'membership+recurring'),$_smarty_tpl);?>
</th>
            <?php if (!empty($_smarty_tpl->tpl_vars['plans']->value[0]['productId'])) {?><th><?php echo smarty_function_text(array('key'=>'membership+product_id'),$_smarty_tpl);?>
</th><?php }?>
        </tr>
        <?php if ($_smarty_tpl->tpl_vars['plans']->value) {?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['plans']->value, 'plan');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['plan']->value) {
?>
        <tr class="plan <?php echo smarty_function_cycle(array('values'=>'ow_alt1,ow_alt2'),$_smarty_tpl);?>
">
            <td><input type="checkbox" class="plan_id" name="plans[<?php echo $_smarty_tpl->tpl_vars['plan']->value['dto']->id;?>
]" value="1" data-pid="<?php echo $_smarty_tpl->tpl_vars['plan']->value['dto']->id;?>
" /></td>
            <td>
                <input type="text" class="ow_settings_input" name="periods[<?php echo $_smarty_tpl->tpl_vars['plan']->value['dto']->id;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['plan']->value['dto']->period;?>
" />
                <select name="periodUnits[<?php echo $_smarty_tpl->tpl_vars['plan']->value['dto']->id;?>
]">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['periodUnitsList']->value, 'option');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['option']->value) {
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['option']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['option']->value == $_smarty_tpl->tpl_vars['plan']->value['dto']->periodUnits) {?>selected<?php }?>><?php echo smarty_function_text(array('key'=>"membership+".((string)$_smarty_tpl->tpl_vars['option']->value)),$_smarty_tpl);?>
</option>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                </select>
            </td>
            <td>
                <?php if ($_smarty_tpl->tpl_vars['plan']->value['dto']->price != 0) {?>
                    <input type="text" class="ow_settings_input" name="prices[<?php echo $_smarty_tpl->tpl_vars['plan']->value['dto']->id;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['plan']->value['dto']->price;?>
" />
                <?php } else { ?>
                    <div class="ow_remark"><?php echo smarty_function_text(array('key'=>'membership+trial'),$_smarty_tpl);?>
</div>
                <?php }?>
            </td>
            <td><?php if ($_smarty_tpl->tpl_vars['plan']->value['dto']->price != 0) {?><input type="checkbox" name="recurring[<?php echo $_smarty_tpl->tpl_vars['plan']->value['dto']->id;?>
]" value="1" <?php if ($_smarty_tpl->tpl_vars['plan']->value['dto']->recurring) {?>checked="checked"<?php }?> /><?php }?></td>
            <?php if (!empty($_smarty_tpl->tpl_vars['plan']->value['productId'])) {?><td class="ow_small"><?php echo $_smarty_tpl->tpl_vars['plan']->value['productId'];?>
</td><?php }?>
        </tr>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

        <?php }?>
        <tr class="paid-plan-template" style="display: none;">
            <td><input type="checkbox" class="new_plan_id" name="paid_plans[]" value="1" /></td>
            <td><input type="text" class="ow_settings_input" name="paid_periods[]" />
                <select name="paid_period_units[]">
                    <?php echo $_smarty_tpl->tpl_vars['options']->value;?>

                </select></td>
            <td><input type="text" class="ow_settings_input" name="paid_prices[]" /></td>
            <td><input type="checkbox" name="paid_recurring[]" value="1" /></td>
            <?php if (!empty($_smarty_tpl->tpl_vars['plans']->value[0]['productId'])) {?><td></td><?php }?>
        </tr>
        <tr class="trial-plan-template" style="display: none;">
            <td><input type="checkbox" class="new_plan_id" name="trial_plans[]" value="1" /></td>
            <td><input type="text" class="ow_settings_input" name="trial_periods[]" />                
                <select name="trial_period_units[]">
                    <?php echo $_smarty_tpl->tpl_vars['options']->value;?>

                </select></td></td>
            <td><div class="ow_remark"><?php echo smarty_function_text(array('key'=>'membership+trial'),$_smarty_tpl);?>
</div></td>
            <td></td>
            <?php if (!empty($_smarty_tpl->tpl_vars['plans']->value[0]['productId'])) {?><td></td><?php }?>
        </tr>
        <tr class="ow_tr_last">
            <td>
                <input id="check_all" title="<?php echo smarty_function_text(array('key'=>'base+check_all'),$_smarty_tpl);?>
" type="checkbox" />
            </td>
            <td colspan="<?php if (!empty($_smarty_tpl->tpl_vars['plans']->value[0]['productId'])) {?>4<?php } else { ?>3<?php }?>">
                <div class="ow_txtleft">
                    <?php echo smarty_function_decorator(array('name'=>"button_list_item",'type'=>"button",'langLabel'=>"membership+delete_selected",'buttonName'=>"delete_plans",'id'=>"btn_delete",'class'=>"ow_red"),$_smarty_tpl);?>

                    <?php echo smarty_function_decorator(array('name'=>"button_list_item",'type'=>"button",'langLabel'=>"membership+add_paid_plan",'id'=>"btn_add_plan"),$_smarty_tpl);?>

                    <?php echo smarty_function_decorator(array('name'=>"button_list_item",'type'=>"button",'langLabel'=>"membership+add_trial_plan",'id'=>"btn_add_trial_plan"),$_smarty_tpl);?>

                </div>
            </td>
        </tr>
    </table>
    <div class="clearfix"><div class="ow_right">
        <?php echo smarty_function_decorator(array('name'=>"button",'type'=>"submit",'langLabel'=>"admin+save_btn_label",'buttonName'=>"update_plans"),$_smarty_tpl);?>

    </div></div>
</form>

<?php $_block_repeat1=false;
echo smarty_block_block_decorator(array('name'=>'box','type'=>'empty','iconClass'=>'ow_ic_clock','langLabel'=>'membership+plans','addClass'=>"ow_stdmargin"), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);
}
}
