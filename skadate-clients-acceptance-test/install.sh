#!/bin/bash

# installl php dependencies fot the skdate tests
cd skadate;
composer install;

# create a link for firebird tests
if [ -d "../../ow_plugins/skmobileapp/application/e2e/" ]; then
    cd ../../ow_plugins/skmobileapp/application/e2e/;
    rm -rf features;
    ln -s ../../../../clients-acceptance-test/firebird features
fi
