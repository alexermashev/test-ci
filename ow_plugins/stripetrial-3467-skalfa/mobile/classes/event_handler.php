<?php

class STRIPETRIAL_MCLASS_EventHandler
{
    /**
     * Class instance
     *
     * @var STRIPETRIAL_MCLASS_EventHandler
     */
    private static $classInstance;

    /**
     * Class constructor
     */
    private function __construct()
    {

    }

    /**
     * Get instance
     *
     * @return STRIPETRIAL_MCLASS_EventHandler
     */
    public static function getInstance()
    {
        if (self::$classInstance === null) {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    public function init()
    {
        $requestHandler = OW::getRequestHandler();

        $requestHandler->addCatchAllRequestsExclude('base.email_verify', 'STRIPETRIAL_CTRL_Main');
        $requestHandler->addCatchAllRequestsExclude('base.email_verify', 'BILLINGSTRIPE_CTRL_Action');

        OW::getEventManager()->bind(OW_EventManager::ON_PLUGINS_INIT, [$this, 'afterInit']);
    }

    public function afterInit()
    {
        $em = OW::getEventManager();

        $em->bind($em::ON_USER_REGISTER, array($this, 'onAfterRegister'));
        $em->bind('skmobileapp.get_translations', array($this, 'onGetTranslations'));
        $em->bind(BOL_BillingService::EVENT_ON_AFTER_DELIVER_SALE, array($this, 'onAfterDeliverSale'));
    }

    /**
     * Get translations to skmobileapp
     * @param OW_Event $event
     */
    public function onGetTranslations( OW_Event $event )
    {
        $langService = BOL_LanguageService::getInstance();
        $translations = [];

        $my_keys = $langService->findAllPrefixKeys($langService->findPrefixId(STRIPETRIAL_BOL_Service::PLUGIN_KEY));

        foreach ( $my_keys as $item )
        {
            $translations[$item->key] = OW::getLanguage()->text(STRIPETRIAL_BOL_Service::PLUGIN_KEY, $item->key);
        }

        $event->add(STRIPETRIAL_BOL_Service::PLUGIN_KEY, $translations);
    }

    public function onAfterRegister( OW_Event $event )
    {
        if ( !OW::getUser()->isAuthenticated() )
        {
            return;
        }

        $user = OW::getUser();

        if ( $user->isAdmin() )
        {
            return;
        }

        $params = $event->getParams();
        $userId = $params['userId'];

        $trialService = STRIPETRIAL_BOL_Service::getInstance();
        $trialService->setTrial(0, $userId);
    }

    public function onAfterDeliverSale( OW_Event $event )
    {
        $sale = $event->getParams()['saleDbo'];

        $trialService = STRIPETRIAL_BOL_Service::getInstance();

        $user = BOL_UserService::getInstance()->findUserById($sale->userId);

        if ( empty($user) )
        {
            return;
        }

        $accType = $user->getAccountType();

        if ( mb_strtolower($sale->pluginKey) === 'membership' &&
            mb_strtolower($sale->entityKey) === 'membership_plan' &&
            $sale->entityId == $trialService->getMembershipPlanByAccountType($accType, 'number')
        )
        {
            $trialService->setTrial(2, $sale->userId);
        }
    }
}