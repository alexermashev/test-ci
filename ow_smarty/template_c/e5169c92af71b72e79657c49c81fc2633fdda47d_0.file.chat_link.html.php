<?php
/* Smarty version 3.1.30, created on 2019-01-28 22:03:59
  from "/Users/esase/Sites/8418/ow_plugins/videoim/views/components/chat_link.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c4fc29f5274f1_59371386',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e5169c92af71b72e79657c49c81fc2633fdda47d' => 
    array (
      0 => '/Users/esase/Sites/8418/ow_plugins/videoim/views/components/chat_link.html',
      1 => 1547792046,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c4fc29f5274f1_59371386 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.text.php';
?>
<img id="videoim_chatlink_<?php echo $_smarty_tpl->tpl_vars['recipientId']->value;?>
" title="<?php echo smarty_function_text(array('key'=>"videoim+chat_call_title",'escape'=>"quotes"),$_smarty_tpl);?>
" class="videoim_chat_link" src="<?php echo $_smarty_tpl->tpl_vars['baseImagesUrl']->value;?>
/call.svg" width="20" height="20" />

<?php echo '<script'; ?>
 type="text/javascript">
    $("#videoim_chatlink_<?php echo $_smarty_tpl->tpl_vars['recipientId']->value;?>
").click(function(e){
        e.stopPropagation();
        videoImRequest.getChatWindow(<?php echo $_smarty_tpl->tpl_vars['recipientId']->value;?>
);
    });
<?php echo '</script'; ?>
><?php }
}
