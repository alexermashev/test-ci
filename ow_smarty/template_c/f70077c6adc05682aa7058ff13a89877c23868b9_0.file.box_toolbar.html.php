<?php
/* Smarty version 3.1.30, created on 2019-01-18 01:23:36
  from "/Users/esase/Sites/8418/ow_system_plugins/base/decorators/box_toolbar.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c4170e85a5d73_98462801',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f70077c6adc05682aa7058ff13a89877c23868b9' => 
    array (
      0 => '/Users/esase/Sites/8418/ow_system_plugins/base/decorators/box_toolbar.html',
      1 => 1547792051,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c4170e85a5d73_98462801 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_style')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/block.style.php';
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('style', array());
$_block_repeat1=true;
echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>


.ow_box_toolbar span{
display:inline-block;
}

<?php $_block_repeat1=false;
echo smarty_block_style(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

<ul class="ow_box_toolbar ow_remark ow_bl">
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['itemList'], 'item', false, NULL, 'toolbar', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
?>    
    <li <?php if (!empty($_smarty_tpl->tpl_vars['item']->value['id'])) {?>id="<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['item']->value['display'])) {?> style="display:<?php echo $_smarty_tpl->tpl_vars['item']->value['display'];?>
"<?php }
if (!empty($_smarty_tpl->tpl_vars['item']->value['class'])) {?> class="<?php echo $_smarty_tpl->tpl_vars['item']->value['class'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['item']->value['title'])) {?> title="<?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
"<?php }?>>
        <?php if (isset($_smarty_tpl->tpl_vars['item']->value['href'])) {?>
        <a<?php if (isset($_smarty_tpl->tpl_vars['item']->value['click'])) {?> onclick="<?php echo $_smarty_tpl->tpl_vars['item']->value['click'];?>
"<?php }
if (isset($_smarty_tpl->tpl_vars['item']->value['rel'])) {?> rel="<?php echo $_smarty_tpl->tpl_vars['item']->value['rel'];?>
"<?php }?> href="<?php echo $_smarty_tpl->tpl_vars['item']->value['href'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['label'];?>
</a>
        <?php } else { ?>
        <?php echo $_smarty_tpl->tpl_vars['item']->value['label'];?>

        <?php }?>
    </li>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</ul>


<?php }
}
