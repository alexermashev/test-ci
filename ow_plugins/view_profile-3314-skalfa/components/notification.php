<?php

class VIEWPROFILE_CMP_Notification extends OW_Component
{
    /**
     * @var VIEWPROFILE_BOL_SendProfile
     */
    private $sendData;
    private $user;
    private $items = array();

    const NL_PLACEHOLDER = '%%%nl%%%';
    const TAB_PLACEHOLDER = '%%%tab%%%';
    const SPACE_PLACEHOLDER = '%%%space%%%';

    public function __construct( VIEWPROFILE_BOL_SendProfile $sendData )
    {
        parent::__construct();

        $this->sendData = $sendData;
        $this->user = BOL_UserService::getInstance()->findUserById($sendData->whoseId);
        $this->items = [];
    }

    public function onBeforeRender()
    {
        parent::onBeforeRender();
    }

    private function getSubject()
    {
        return OW::getLanguage()->text('viewprofile', 'email_notification_send_profile_subject');
    }

    private function getHtml()
    {
        $template = OW::getPluginManager()->getPlugin(VIEWPROFILE_BOL_Service::PLUGIN_KEY)->getCmpViewDir() . 'notification_html.html';
        $this->setTemplate($template);

        $whoseId = $this->user->getId();

        $staticUrl = OW::getPluginManager()->getPlugin('pcgallery')->getStaticUrl();
        $styles[] = $staticUrl . 'style.css';
        $scripts[] = $staticUrl . 'script.js';

        $userQuestions = new VIEWPROFILE_CMP_UserSection($whoseId);
        $pcgallery = new VIEWPROFILE_CMP_Gallery($whoseId);

        $avatar = BOL_AvatarService::getInstance()->getAvatarUrl($whoseId, 2, null, false);
        $user['avatar'] = $avatar ? $avatar : BOL_AvatarService::getInstance()->getDefaultAvatarUrl($whoseId);

        $sendHash = $this->sendData->hash;

        $links = [
            1 => OW::getRouter()->urlForRoute(VIEWPROFILE_BOL_Service::PLUGIN_KEY . '_main_opinion') . '?status=1&hash=' . $sendHash,
            2 => OW::getRouter()->urlForRoute(VIEWPROFILE_BOL_Service::PLUGIN_KEY . '_main_opinion') . '?status=2&hash=' . $sendHash,
            3 => OW::getRouter()->urlForRoute(VIEWPROFILE_BOL_Service::PLUGIN_KEY . '_main_opinion') . '?status=3&hash=' . $sendHash,
        ];

        $this->addComponent('pcgallery', $pcgallery);
        $this->addComponent('userQuestions', $userQuestions);

        $this->assign('user', $user);

        $photos = $pcgallery->getPhotos();

        $pcgallery->assign('empty', empty($photos));
        $pcgallery->assign('photos', $photos);
        $pcgallery->assign('logoImage', VIEWPROFILE_BOL_Service::getInstance()->getLogoImage());
        $pcgallery->assign('links', $links);

        return parent::render();
    }

    private function getTxt()
    {
        $template = OW::getPluginManager()->getPlugin(VIEWPROFILE_BOL_Service::PLUGIN_KEY)->getCmpViewDir() . 'notification_txt.html';
        $this->setTemplate($template);

        $sendHash = $this->sendData->hash;

        $links = [
            1 => OW::getRouter()->urlForRoute(VIEWPROFILE_BOL_Service::PLUGIN_KEY . '_main_opinion') . '?status=1&hash=' . $sendHash,
            2 => OW::getRouter()->urlForRoute(VIEWPROFILE_BOL_Service::PLUGIN_KEY . '_main_opinion') . '?status=2&hash=' . $sendHash,
            3 => OW::getRouter()->urlForRoute(VIEWPROFILE_BOL_Service::PLUGIN_KEY . '_main_opinion') . '?status=3&hash=' . $sendHash,
        ];

        $this->assign('nl', self::NL_PLACEHOLDER);
        $this->assign('tab', self::TAB_PLACEHOLDER);
        $this->assign('space', self::SPACE_PLACEHOLDER);
        $this->assign('links', $links);
        $this->assign('email', $this->sendData->email);
        $this->assign('username', BOL_UserService::getInstance()->getDisplayName($this->sendData->whoseId));

        $content = parent::render();
        $search = array(self::NL_PLACEHOLDER, self::TAB_PLACEHOLDER, self::SPACE_PLACEHOLDER);
        $replace = array("\n", '    ', ' ');

        return str_replace($search, $replace, $content);
    }

    public function sendNotification()
    {
        $subject = $this->getSubject();
        $txt = $this->getTxt();
        $html = $this->getHtml();

        $mail = OW::getMailer()->createMail()
            ->addRecipientEmail($this->sendData->email)
            ->setTextContent($txt)
            ->setHtmlContent($html)
            ->setSubject($subject);

        OW::getMailer()->send($mail);
    }
}