<?php
/* Smarty version 3.1.30, created on 2018-07-29 08:28:00
  from "/var/www/biyebiye/public_html/ow_plugins/membership/mobile/views/controllers/subscribe_index.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5db2d006edc9_43396754',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ec3a21a127a45fb719bed773377f2c0c77233a65' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_plugins/membership/mobile/views/controllers/subscribe_index.html',
      1 => 1479204282,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b5db2d006edc9_43396754 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
?>
<div class="owm_subscribe_page">
    <div class="owm_content_menu_wrap owm_padding">
        <?php echo $_smarty_tpl->tpl_vars['menu']->value;?>

    </div>
    <div class="owm_membership_list">
        <?php if (isset($_smarty_tpl->tpl_vars['current']->value)) {?>
            <div class="owm_current_membership owm_membersip_item owm_border">
                <div class="owm_sidemenu_item_wrap owm_bg_color_1">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['yourMembershipInfoLink']->value;?>
" class="owm_sidemenu_item clearfix">
                        <span class="owm_sidemenu_item_label owm_float_left"><?php echo smarty_function_text(array('key'=>'membership+your_membership'),$_smarty_tpl);?>
: </span>
                        <span class="owm_sidemenu_item_value owm_float_right"><b><?php if (isset($_smarty_tpl->tpl_vars['currentTitle']->value)) {
echo $_smarty_tpl->tpl_vars['currentTitle']->value;
if ($_smarty_tpl->tpl_vars['current']->value->recurring) {?> (<?php echo smarty_function_text(array('key'=>'membership+recurring'),$_smarty_tpl);?>
)<?php }
}?></b></span>
                    </a>
                </div>
            </div>
        <?php } else { ?>

        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['mTypePermissions']->value, 'mt');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['mt']->value) {
?>
            <?php if ($_smarty_tpl->tpl_vars['mt']->value['current'] == true) {?>
            <div class="owm_current_membership owm_membersip_item owm_border">
                <div class="owm_sidemenu_item_wrap owm_bg_color_1">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['mt']->value['yourMembershipInfoLink'];?>
" class="owm_sidemenu_item clearfix">
                        <span class="owm_sidemenu_item_label owm_float_left"><?php echo smarty_function_text(array('key'=>'membership+your_membership'),$_smarty_tpl);?>
: </span>
                        <span class="owm_sidemenu_item_value owm_float_right"><b><?php if (isset($_smarty_tpl->tpl_vars['mt']->value['title'])) {
echo $_smarty_tpl->tpl_vars['mt']->value['title'];
}?></b></span>
                    </a>
                </div>
            </div>
            <?php }?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

        <?php }?>

        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['mTypePermissions']->value, 'mt');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['mt']->value) {
?>
        <div class="owm_membersip_item owm_border">
            <div class="owm_sidemenu_item_wrap owm_bg_color_3">
                <a href="<?php echo $_smarty_tpl->tpl_vars['mt']->value['membershipInfoLink'];?>
" class="owm_sidemenu_item clearfix">
                    <span class="owm_sidemenu_item_label owm_float_left"><?php echo $_smarty_tpl->tpl_vars['mt']->value['title'];?>
</span>
                </a>
            </div>
        </div>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

    </div>
</div>



<?php }
}
