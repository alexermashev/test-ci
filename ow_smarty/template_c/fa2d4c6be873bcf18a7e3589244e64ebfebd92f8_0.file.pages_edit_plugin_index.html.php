<?php
/* Smarty version 3.1.30, created on 2018-07-20 02:20:59
  from "/var/www/biyebiye/public_html/ow_system_plugins/admin/views/controllers/pages_edit_plugin_index.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b517f4ba3d487_75151551',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fa2d4c6be873bcf18a7e3589244e64ebfebd92f8' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_system_plugins/admin/views/controllers/pages_edit_plugin_index.html',
      1 => 1479204252,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b517f4ba3d487_75151551 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_url_for_route')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.url_for_route.php';
if (!is_callable('smarty_function_decorator')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.decorator.php';
if (!is_callable('smarty_block_form')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_cycle')) require_once '/var/www/biyebiye/public_html/ow_libraries/vendor/smarty/smarty/libs/plugins/function.cycle.php';
if (!is_callable('smarty_function_label')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.label.php';
if (!is_callable('smarty_function_input')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_error')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.error.php';
if (!is_callable('smarty_function_submit')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.submit.php';
$_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'default', "back_url", null);
echo smarty_function_url_for_route(array('for'=>"admin_pages_main"),$_smarty_tpl);
$_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);
?>


<div class="ow_stdmargin"><?php echo smarty_function_decorator(array('name'=>"button",'class'=>"ow_ic_left_arrow",'onclick'=>"location.href='".((string)$_smarty_tpl->tpl_vars['back_url']->value)."';",'langLabel'=>"base+pages_back"),$_smarty_tpl);?>
</div>
<?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('name'=>"edit-form"));
$_block_repeat1=true;
echo smarty_block_form(array('name'=>"edit-form"), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

<div class="ow_superwide ow_automargin">
<table class="ow_table_1 ow_form">

    <tr id="title-tr" class="<?php echo smarty_function_cycle(array('values'=>"ow_alt2, ow_alt1"),$_smarty_tpl);?>
 ow_tr_first">
        <td class="ow_label">
        	<?php echo smarty_function_label(array('name'=>"name"),$_smarty_tpl);?>

       	</td>
        <td class="ow_value">
        	<?php echo smarty_function_input(array('name'=>"name"),$_smarty_tpl);?>

        	<br /><?php echo smarty_function_error(array('name'=>"name"),$_smarty_tpl);?>

        </td>        
    </tr>

    <tr class="<?php echo smarty_function_cycle(array('values'=>"ow_alt2, ow_alt1"),$_smarty_tpl);?>
 ow_tr_last" >
        <td class="ow_label"><?php echo smarty_function_label(array('name'=>"visible-for"),$_smarty_tpl);?>
</td>
        <td class="ow_value">
        	<?php echo smarty_function_input(array('name'=>"visible-for"),$_smarty_tpl);?>

        	<br /><?php echo smarty_function_error(array('name'=>"visible-for"),$_smarty_tpl);?>

       </td>
    </tr>

</table>
<div class="clearfix ow_stdmargin"><div class="ow_right"><?php echo smarty_function_submit(array('name'=>"save",'class'=>"ow_positive"),$_smarty_tpl);?>
</div></div>
</div>
<?php $_block_repeat1=false;
echo smarty_block_form(array('name'=>"edit-form"), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);
}
}
