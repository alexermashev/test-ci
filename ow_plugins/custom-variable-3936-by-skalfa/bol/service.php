<?php

/**
 * Copyright (c) 2018, Skalfa LLC
 * All rights reserved.
 *
 * ATTENTION: This commercial software is intended for exclusive use with SkaDate Dating Software (http://www.skadate.com) and is licensed under SkaDate Exclusive License by Skalfa LLC.
 *
 * Full text of this license can be found at http://www.skadate.com/sel.pdf
 */

/**
 * Class SKCV_BOL_Service
 *
 * @author (A. S. B.) (D. P.) <azazel9966@gmail.com>.
 * @package ow_plugins.pluginkey
 * @since 1.8.4
 */
class SKCV_BOL_Service
{
    const PLUGIN_KEY = 'skcv';
    const CUSTOM_KEY_LANG = 'custom_key';
    const CUSTOM_KEY_PATTERN_KEY = 'customKey';

    /**
     * @var BOL_LanguageService
     */
    private $languageService;

    /**
     * @var int
     */
    private $currentLanguage;

    private $customText = null;

    private static $classInstance;

    /**
     * @return SKCV_BOL_Service
     */
    public static function getInstance()
    {
        if ( !isset(self::$classInstance) )
        {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    private function __construct()
    {
        $this->languageService = BOL_LanguageService::getInstance();
        $this->currentLanguage = $this->languageService->getCurrent()->getId();
        $this->customText = OW::getLanguage()->text(self::PLUGIN_KEY, self::CUSTOM_KEY_LANG);
    }

    public function getVars( $vars )
    {
        if ( !empty($vars) && is_array($vars) )
        {
            foreach ( $vars as &$value )
            {
                if ( UTIL_Serialize::isSerializedObject($value) )
                {
                    $object = UTIL_Serialize::unserialize($value);
                    if ( empty($object) || !($object instanceof BASE_CLASS_LanguageParams) )
                    {
                        $value = '';
                    }

                    $value = $object->fetch();
                }
            }
        }

        return $vars;
    }

    public function getNewText( $prefix, $key, $vars )
    {
        if ( !$this->customText || !$this->currentLanguage || $key == 'admin_bottom_text' )
        {
            return null;
        }

        $text = $this->languageService->getTextTemplate($this->currentLanguage, $prefix, $key);

        if ( preg_match('/' . self::CUSTOM_KEY_PATTERN_KEY . '/', $text) )
        {
            $vars[self::CUSTOM_KEY_PATTERN_KEY] = $this->customText;
            $vars = $this->getVars($vars);

            return UTIL_String::replaceVars($text, $vars);
        }

        return null;
    }
}
