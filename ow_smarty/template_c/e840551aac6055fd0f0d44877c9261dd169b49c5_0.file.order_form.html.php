<?php
/* Smarty version 3.1.30, created on 2018-07-21 14:42:17
  from "/var/www/biyebiye/public_html/ow_plugins/billing_paypal/views/controllers/order_form.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b537e8983dea1_00237601',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e840551aac6055fd0f0d44877c9261dd169b49c5' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_plugins/billing_paypal/views/controllers/order_form.html',
      1 => 1479204282,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b537e8983dea1_00237601 (Smarty_Internal_Template $_smarty_tpl) {
?>

<form id="<?php echo $_smarty_tpl->tpl_vars['formId']->value;?>
" action="<?php echo $_smarty_tpl->tpl_vars['fields']->value['formActionUrl'];?>
" method="post">
	<input type="hidden" name="return" value="<?php echo $_smarty_tpl->tpl_vars['fields']->value['return_url'];?>
" />
	<input type="hidden" name="cancel_return" value="<?php echo $_smarty_tpl->tpl_vars['fields']->value['cancel_return_url'];?>
" />
	<input type="hidden" name="notify_url" value="<?php echo $_smarty_tpl->tpl_vars['fields']->value['notify_url'];?>
" />
	<input type="hidden" name="rm" value="2" />
	<input type="hidden" name="business" value="<?php echo $_smarty_tpl->tpl_vars['fields']->value['business'];?>
" />
	<input type="hidden" name="item_name" value="<?php echo $_smarty_tpl->tpl_vars['sale']->value->entityDescription;?>
" />
	<input type="hidden" name="no_note" value="1" />
	<input type="hidden" name="currency_code" value="<?php echo $_smarty_tpl->tpl_vars['sale']->value->currency;?>
" />
	<input type="hidden" name="custom" value="<?php echo $_smarty_tpl->tpl_vars['sale']->value->hash;?>
" />
	<input type="hidden" name="charset" value="utf-8" />
<?php if ($_smarty_tpl->tpl_vars['sale']->value->recurring) {?>
	<input type="hidden" name="cmd" value="_xclick-subscriptions" />
	<input type="hidden" name="a3" value="<?php echo $_smarty_tpl->tpl_vars['sale']->value->totalAmount;?>
" />
	<?php if ($_smarty_tpl->tpl_vars['sale']->value->periodUnits == 'months') {?>
	<input type="hidden" name="p3" value="<?php echo $_smarty_tpl->tpl_vars['sale']->value->period;?>
" />
    <input type="hidden" name="t3" value="M" />
	<?php } else { ?>
	<input type="hidden" name="p3" value="<?php echo $_smarty_tpl->tpl_vars['sale']->value->period;?>
" />
	<input type="hidden" name="t3" value="D" />
	<?php }?>
	<input type="hidden" name="src" value="1" />
<?php } else { ?>
	<input type="hidden" name="cmd" value="_xclick" />
	<input type="hidden" name="bn" value="<?php echo $_smarty_tpl->tpl_vars['partnerCode']->value;?>
"/>
	<input type="hidden" name="amount" value="<?php echo $_smarty_tpl->tpl_vars['sale']->value->totalAmount;?>
" />
<?php }?>
</form><?php }
}
