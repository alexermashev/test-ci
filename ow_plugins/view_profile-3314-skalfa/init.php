<?php

VIEWPROFILE_CLASS_EventHandler::getInstance()->init();

OW::getRouter()->addRoute(new OW_Route(VIEWPROFILE_BOL_Service::PLUGIN_KEY . '_main_index', VIEWPROFILE_BOL_Service::PLUGIN_KEY . '/main/index/', 'VIEWPROFILE_CTRL_Main', 'index'));
OW::getRouter()->addRoute(new OW_Route(VIEWPROFILE_BOL_Service::PLUGIN_KEY . '_main_sent_profiles', '/sent-profiles/', 'VIEWPROFILE_CTRL_Main', 'sentProfiles'));
    OW::getRouter()->addRoute(new OW_Route(VIEWPROFILE_BOL_Service::PLUGIN_KEY . '_main_opinion', '/opinion/', 'VIEWPROFILE_CTRL_Main', 'opinion'));