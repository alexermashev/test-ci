<?php
/* Smarty version 3.1.30, created on 2019-01-28 03:07:38
  from "/Users/esase/Sites/8418/ow_plugins/usearch/views/controllers/search_search_result.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c4eb84ac94a17_55969298',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'dc1b2ade00110ab1a47bac3966d17837d5750601' => 
    array (
      0 => '/Users/esase/Sites/8418/ow_plugins/usearch/views/controllers/search_search_result.html',
      1 => 1547792046,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c4eb84ac94a17_55969298 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_add_content')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.add_content.php';
if (!is_callable('smarty_function_text')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.text.php';
if (isset($_smarty_tpl->tpl_vars['menu']->value)) {
echo $_smarty_tpl->tpl_vars['menu']->value;
}?>



<?php echo smarty_function_add_content(array('key'=>"base.content.user_list_top",'listType'=>'usearch_search_result'),$_smarty_tpl);?>


<?php if (isset($_smarty_tpl->tpl_vars['searchResultMenu']->value)) {?>
    <?php echo $_smarty_tpl->tpl_vars['searchResultMenu']->value;?>

<?php }
if ($_smarty_tpl->tpl_vars['page']->value > 1) {?>
    <div class="ow_fw_menu ow_center usearch_load_earlier_profiles">
        <a href="javascript://"><?php echo smarty_function_text(array('key'=>"usearch+load_earlier_profiles"),$_smarty_tpl);?>
</a>
    </div>
<?php }
if ($_smarty_tpl->tpl_vars['itemCount']->value) {?>
    <div class="ow_search_results_photo_gallery_container ow_photo_list_wrap ow_photo_userlist">
        <?php echo $_smarty_tpl->tpl_vars['cmp']->value;?>

    </div>
<?php } else { ?>
    <div class="ow_nocontent"><?php echo smarty_function_text(array('key'=>"usearch+no_users_found",'searchUrl'=>$_smarty_tpl->tpl_vars['searchUrl']->value),$_smarty_tpl);?>
</div>
<?php }
}
}
