<?php
/* Smarty version 3.1.30, created on 2018-07-19 04:48:26
  from "/var/www/biyebiye/public_html/ow_plugins/usearch/views/controllers/admin_quick_search_settings.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b50505acf6488_62754448',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b0241016b5e63f1a3bf54d9e8592762f188be4bd' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_plugins/usearch/views/controllers/admin_quick_search_settings.html',
      1 => 1479204280,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b50505acf6488_62754448 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_style')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.style.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_question_lang')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.question_lang.php';
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('style', array());
$_block_repeat1=true;
echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

.ow_quicksearch_table {
	width: 730px;
}
.ow_quicksearch_layout, .ow_quicksearch_layout_wrap {
	width: 334px;
}
.ow_quicksearch_list_wrap {
	width: 300px;
}

.ow_quicksearch_position_disabled, .ow_quicksearch_position {
    width: 100%;
	height: 48px;
	border: 0;
	text-align: center;
	overflow: hidden;
	padding: 12px 0 12px 16px;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
}

.ow_quicksearch_item, .ow_quicksearch_item_in_layout, .ow_quicksearch_item_placeholder {
	width: 100%;
	height: 24px;
	border-radius: 2px;
	-moz-border-radius: 2px;
	-webkit-border-radius: 2px;
	border: 1px solid #e8e8e8;
	margin-bottom: 26px;
	text-align: center;
	overflow: hidden;
	padding: 0 8px;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
}

.ow_quicksearch_item {
	background-color: #f0f0f0;
	color: #7b7b7b;
	cursor: move;
}

.ow_quicksearch_item_in_layout {
	background-color: #fff;
	color: #7b7b7b;
	cursor: move;
}

.ow_quicksearch_position_disabled .quicksearch_dnd_disbale_item
{
    cursor: auto;
}

.ow_quicksearch_item_placeholder {
	background-color: #f3f3f3;
	-webkit-box-shadow: inset 0px 1px 3px 0px rgba(0,0,0,0.05);
	-moz-box-shadow: inset 0px 1px 3px 0px rgba(0,0,0,0.05);
	box-shadow: inset 0px 1px 3px 0px rgba(0,0,0,0.05);
	color: #bebebe;
}

.ow_quicksearch_item_placeholder.placeholder_hover {
	border: 1px dashed #b4b4b4;
}

.ow_quicksearch_item.item_draged {
	background: #f9f9f9;
	color: #e6e6e6;
}

.ow_quicksearch_item.item_draging {
	border: 1px dashed #b4b4b4;
}
.ow_quicksearch_label {
	line-height: 24px;
	font-size: 12px;
}
.ow_quicksearch_item_in_layout .ow_quicksearch_label {
	font-weight: bold;
}
.ow_quicksearch_item_placeholder .ow_quicksearch_label {
	font-size: 11px;
}
.ow_quicksearch_layout {
	background: #f3f3f3;
	border-radius: 4px;
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
	border: 1px solid #e8e8e8;
	padding: 16px 16px 16px 0;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
}
.ow_quicksearch_layout_wrap h3 {
	text-transform: uppercase;
	margin-bottom: 4px;
	font-family: 'UbuntuBold',"Trebuchet MS","Helvetica CY",sans-serif;
	padding-left: 16px;
}
.ow_quicksearch_layout_list {
	margin-bottom: 4px;
}
.ow_quicksearch_layout_list .layout_item:last-child {
	margin-bottom: 0;
}

.ow_quicksearch_layout.fixed {
    position: fixed;
    top: 100px;
}

.preview_button_div
{
    height: 24px;
}
<?php $_block_repeat1=false;
echo smarty_block_style(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>



<?php echo '<script'; ?>
 type="text/template" id="allowed_questions_template">
    <div class="ow_quicksearch_item available_field_item"><span class="ow_quicksearch_label"></span></div>
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/template" id="item-quick_search_template">
    <div class="ow_quicksearch_position" ><div class="ow_quicksearch_item_in_layout layout_item"><span class="ow_quicksearch_label"></span></div></div>
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/template" id="item-quick_empty_position">
    <div class="ow_quicksearch_position" ><div class="ow_quicksearch_item_placeholder layout_item"><span class="ow_quicksearch_label"><?php echo smarty_function_text(array('key'=>'usearch+admin_empty_position'),$_smarty_tpl);?>
</span></div></div>
<?php echo '</script'; ?>
>

<?php echo $_smarty_tpl->tpl_vars['contentMenu']->value;?>

<div class="ow_automargin ow_quicksearch_table clearfix" style="position:relative;">
    <div class="ow_std_margin">
    <?php echo smarty_function_text(array('key'=>"usearch+admin_settings_description"),$_smarty_tpl);?>

    </div>

	<div class="ow_quicksearch_list_wrap ow_left" >
		<div class="ow_quicksearch_list" >
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['allowedQuestionList']->value, 'question');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['question']->value) {
?>
                <div class="ow_quicksearch_item available_field_item" question-name="<?php echo $_smarty_tpl->tpl_vars['question']->value->name;?>
"><span class="ow_quicksearch_label" ><?php echo smarty_function_question_lang(array('name'=>$_smarty_tpl->tpl_vars['question']->value->name),$_smarty_tpl);?>
</span></div>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

		</div>
	</div>

    <div class="ow_quicksearch_layout_wrap ow_right" >
		<div class="ow_quicksearch_layout">
			<h3>Quick Search</h3>
			<div class="ow_quicksearch_layout_list" >
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['positions']->value, 'question', false, 'position');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['position']->value => $_smarty_tpl->tpl_vars['question']->value) {
?>
                    <div class=" <?php if (!empty($_smarty_tpl->tpl_vars['question']->value) && ($_smarty_tpl->tpl_vars['question']->value == 'sex' || $_smarty_tpl->tpl_vars['question']->value == 'match_sex')) {?>ow_quicksearch_position_disabled<?php } else { ?>ow_quicksearch_position<?php }?>" position="<?php echo $_smarty_tpl->tpl_vars['position']->value;?>
" >
                    <?php if (!empty($_smarty_tpl->tpl_vars['question']->value)) {?>
                        <div class="ow_quicksearch_item_in_layout <?php if ($_smarty_tpl->tpl_vars['question']->value == 'sex' || $_smarty_tpl->tpl_vars['question']->value == 'match_sex') {?>quicksearch_dnd_disbale_item<?php } else { ?>quicksearch_dnd_item<?php }?> layout_item" question-name="<?php echo $_smarty_tpl->tpl_vars['question']->value;?>
">
                            <span class="ow_quicksearch_label"><?php echo smarty_function_question_lang(array('name'=>$_smarty_tpl->tpl_vars['question']->value),$_smarty_tpl);?>
</span>
                        </div>
                    <?php } else { ?>
                        <div class="ow_quicksearch_item_placeholder layout_item">
                            <span class="ow_quicksearch_label"><?php echo smarty_function_text(array('key'=>'usearch+admin_empty_position'),$_smarty_tpl);?>
</span>
                        </div>
                    <?php }?>
                    </div>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

			</div>
            <div class="preview_button_div">
                <div class="ow_right">
                    <span class="ow_button">
                        <span class=" theme_select_submit ow_positive">
                            <input type="button" value="preview" class="theme_select_submit ow_positive ow_ic_picture" id="quicksearch_preview" />
                        </span>
                    </span>
                </div>
            </div>

		</div>
	</div>

</div><?php }
}
