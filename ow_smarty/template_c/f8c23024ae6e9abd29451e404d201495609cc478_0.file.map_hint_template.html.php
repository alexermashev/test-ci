<?php
/* Smarty version 3.1.30, created on 2019-01-18 01:16:51
  from "/Users/esase/Sites/8418/ow_plugins/google_map_location/views/components/map_hint_template.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c416f53b69464_25902996',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f8c23024ae6e9abd29451e404d201495609cc478' => 
    array (
      0 => '/Users/esase/Sites/8418/ow_plugins/google_map_location/views/components/map_hint_template.html',
      1 => 1547792046,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c416f53b69464_25902996 (Smarty_Internal_Template $_smarty_tpl) {
?>

<?php echo '<script'; ?>
 id="map-hint-template" type="text/html">
        <div class="map-hint-container">
            <div class="map-hint-content ow_border ow_bg_color">

                <div class="map-hint-corner-wrap map-hint-top-corner-wrap">
                    <div class="map-hint-corner"></div>
                </div>

                <div class="map-hint-corner-wrap map-hint-bottom-corner-wrap">
                    <div class="map-hint-corner"></div>
                </div>

                <div class="map-hint-corner-wrap map-hint-right-corner-wrap">
                    <div class="map-hint-corner"></div>
                </div>

                <div class="map-hint-body-wrap">
                    <div class="map-hint-body">
                        <div class="map-hint-preloader" style="display:none">Loading...</div>
                    </div>
                </div>

                <div class="map-hint-foot-wrap" style="display: none;">
                    <div class="map-hint-foot"></div>
                </div>
            </div>
        </div>
<?php echo '</script'; ?>
>

<?php }
}
