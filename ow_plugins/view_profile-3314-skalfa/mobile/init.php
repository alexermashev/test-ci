<?php

VIEWPROFILE_MCLASS_EventHandler::getInstance()->init();

$plugin = OW::getPluginManager()->getPlugin('viewprofile');

$key = strtoupper($plugin->getKey());

OW::getRouter()->addRoute(new OW_Route(VIEWPROFILE_BOL_Service::PLUGIN_KEY . '_main_sent_profiles', '/sent-profiles/', 'VIEWPROFILE_CTRL_Main', 'sentProfiles'));
OW::getRouter()->addRoute(new OW_Route(VIEWPROFILE_BOL_Service::PLUGIN_KEY . '_main_opinion', '/opinion/', 'VIEWPROFILE_CTRL_Main', 'opinion'));