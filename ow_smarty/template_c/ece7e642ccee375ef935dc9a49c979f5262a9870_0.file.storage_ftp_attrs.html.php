<?php
/* Smarty version 3.1.30, created on 2018-07-23 01:00:15
  from "/var/www/biyebiye/public_html/ow_system_plugins/admin/views/controllers/storage_ftp_attrs.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5560df184e97_66391815',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ece7e642ccee375ef935dc9a49c979f5262a9870' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_system_plugins/admin/views/controllers/storage_ftp_attrs.html',
      1 => 1479204252,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b5560df184e97_66391815 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_style')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.style.php';
if (!is_callable('smarty_block_block_decorator')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.block_decorator.php';
if (!is_callable('smarty_block_form')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_label')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.label.php';
if (!is_callable('smarty_function_input')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_submit')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.submit.php';
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('style', array());
$_block_repeat1=true;
echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>


.ow_active_plugins tr, .ow_inactive_plugins tr{
    border-top:1px solid #ccc;
}
.ow_plugin_controls{
    display:none;
}

<?php $_block_repeat1=false;
echo smarty_block_style(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

<div class="ow_narrow ow_automargin" >
<?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('block_decorator', array('name'=>'box','type'=>'empty','addClass'=>'ow_stdmargin','iconClass'=>'ow_ic_trash','langLabel'=>'admin+manage_plugins_ftp_box_cap_label'));
$_block_repeat1=true;
echo smarty_block_block_decorator(array('name'=>'box','type'=>'empty','addClass'=>'ow_stdmargin','iconClass'=>'ow_ic_trash','langLabel'=>'admin+manage_plugins_ftp_box_cap_label'), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

<?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('name'=>'ftp'));
$_block_repeat2=true;
echo smarty_block_form(array('name'=>'ftp'), null, $_smarty_tpl, $_block_repeat2);
while ($_block_repeat2) {
ob_start();
?>

<table class="ow_form ow_smallmargin">
       <tr>
           <td class="ow_label"><?php echo smarty_function_label(array('name'=>'host'),$_smarty_tpl);?>
</td>
           <td class="ow_input"><?php echo smarty_function_input(array('name'=>'host'),$_smarty_tpl);?>
</td>
       </tr>
       <tr>
           <td class="ow_label"><?php echo smarty_function_label(array('name'=>'login'),$_smarty_tpl);?>
</td>
           <td class="ow_input"><?php echo smarty_function_input(array('name'=>'login'),$_smarty_tpl);?>
</td>
       </tr>
       <tr>
           <td class="ow_label"><?php echo smarty_function_label(array('name'=>'password'),$_smarty_tpl);?>
</td>
           <td class="ow_input"><?php echo smarty_function_input(array('name'=>'password'),$_smarty_tpl);?>
</td>
       </tr>
       <tr>
           <td class="ow_label"><?php echo smarty_function_label(array('name'=>'port'),$_smarty_tpl);?>
</td>
           <td class="ow_input"><?php echo smarty_function_input(array('name'=>'port'),$_smarty_tpl);?>
</td>
       </tr>
   </table>
   <div class="clearfix"><div class="ow_right"><?php echo smarty_function_submit(array('name'=>'submit','class'=>'ow_positive'),$_smarty_tpl);?>
</div></div>
<?php $_block_repeat2=false;
echo smarty_block_form(array('name'=>'ftp'), ob_get_clean(), $_smarty_tpl, $_block_repeat2);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

<?php $_block_repeat1=false;
echo smarty_block_block_decorator(array('name'=>'box','type'=>'empty','addClass'=>'ow_stdmargin','iconClass'=>'ow_ic_trash','langLabel'=>'admin+manage_plugins_ftp_box_cap_label'), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

</div><?php }
}
