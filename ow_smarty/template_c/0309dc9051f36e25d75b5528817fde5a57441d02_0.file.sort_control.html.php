<?php
/* Smarty version 3.1.30, created on 2019-01-28 03:07:38
  from "/Users/esase/Sites/8418/ow_system_plugins/base/views/components/sort_control.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c4eb84aaabf21_71119096',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0309dc9051f36e25d75b5528817fde5a57441d02' => 
    array (
      0 => '/Users/esase/Sites/8418/ow_system_plugins/base/views/components/sort_control.html',
      1 => 1547792051,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c4eb84aaabf21_71119096 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.text.php';
?>

<div class="ow_sort_control ow_smallmargin ow_small ow_alt2"><span class="ow_sort_control_label"><?php echo smarty_function_text(array('key'=>"base+sort_control_sortby"),$_smarty_tpl);?>
:</span><?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['itemList']->value, 'item');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
?><a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['url'];?>
" <?php if ($_smarty_tpl->tpl_vars['item']->value['isActive']) {?>class="active"<?php }?>><span><?php echo $_smarty_tpl->tpl_vars['item']->value['label'];?>
</span></a><?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</div><?php }
}
