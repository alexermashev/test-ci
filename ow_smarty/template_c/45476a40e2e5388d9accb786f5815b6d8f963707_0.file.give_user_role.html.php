<?php
/* Smarty version 3.1.30, created on 2018-07-20 03:38:03
  from "/var/www/biyebiye/public_html/ow_system_plugins/base/views/components/give_user_role.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b51915bf31072_54398090',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '45476a40e2e5388d9accb786f5815b6d8f963707' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_system_plugins/base/views/components/give_user_role.html',
      1 => 1479204252,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b51915bf31072_54398090 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_form')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_cycle')) require_once '/var/www/biyebiye/public_html/ow_libraries/vendor/smarty/smarty/libs/plugins/function.cycle.php';
if (!is_callable('smarty_function_input')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_label')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.label.php';
if (!is_callable('smarty_function_submit')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.submit.php';
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('name'=>"give-role"));
$_block_repeat1=true;
echo smarty_block_form(array('name'=>"give-role"), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

<table class="ow_table_2 ow_form ow_stdmargin ow_center">
<tr class="ow_tr_first">
	<th></th>
	<th>User Role</th>
</th>
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['list']->value, 'role', false, NULL, 'role', array (
  'last' => true,
  'iteration' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['role']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_role']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_role']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_role']->value['iteration'] == $_smarty_tpl->tpl_vars['__smarty_foreach_role']->value['total'];
?>
<tr class="<?php echo smarty_function_cycle(array('values'=>'ow_alt1, ow_alt2'),$_smarty_tpl);?>
 <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_role']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_role']->value['last'] : null)) {?>ow_tr_last<?php }?>">
	<td><?php echo smarty_function_input(array('name'=>"roles[".((string)$_smarty_tpl->tpl_vars['role']->value->id)."]"),$_smarty_tpl);?>
</td>
	<td><?php echo smarty_function_label(array('name'=>"roles[".((string)$_smarty_tpl->tpl_vars['role']->value->id)."]"),$_smarty_tpl);?>
</td>
</tr>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</table>

	<div class="clearfix ow_smallmargin"><div class="ow_right">
		<?php echo smarty_function_submit(array('name'=>"submit"),$_smarty_tpl);?>

	</div></div>	
<?php $_block_repeat1=false;
echo smarty_block_form(array('name'=>"give-role"), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);
}
}
