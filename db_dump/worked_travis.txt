sudo: required
language: php

php:
  - '7.0'

dist: trust
sudo: required

services:
  - mysql

addons:
  apt:
    sources:
      - google-chrome
    packages:
      - google-chrome-stable
      - xvfb

env:
  global:
    - DB_NAME="skadate"

before_install:
  - mysql -e "create database IF NOT EXISTS $DB_NAME;" -uroot
  - mysql < dump.sql


before_script:
  - export DISPLAY=':99.0'
  - Xvfb :99 -screen 0 1024x768x24 > /dev/null 2>&1 &
  - sudo apt-get update
  - sudo apt-get install apache2 libapache2-mod-fastcgi
  - sudo cp ~/.phpenv/versions/$(phpenv version-name)/etc/php-fpm.conf.default ~/.phpenv/versions/$(phpenv version-name)/etc/php-fpm.conf
  - sudo a2enmod rewrite actions fastcgi alias
  - echo "cgi.fix_pathinfo = 1" >> ~/.phpenv/versions/$(phpenv version-name)/etc/php.ini
  - sudo sed -i -e "s,www-data,travis,g" /etc/apache2/envvars
  - sudo chown -R travis:travis /var/lib/apache2/fastcgi
  - ~/.phpenv/versions/$(phpenv version-name)/sbin/php-fpm
  - sudo cp -f build/travis-ci-apache /etc/apache2/sites-available/000-default.conf
  - sudo sed -e "s?%TRAVIS_BUILD_DIR%?$(pwd)?g" --in-place /etc/apache2/sites-available/000-default.conf
  - sudo service apache2 restart

script:
  - curl http://localhost
 
