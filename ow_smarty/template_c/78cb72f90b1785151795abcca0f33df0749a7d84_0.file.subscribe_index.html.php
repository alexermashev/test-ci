<?php
/* Smarty version 3.1.30, created on 2018-07-20 01:34:43
  from "/var/www/biyebiye/public_html/ow_plugins/membership/views/controllers/subscribe_index.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5174737ba5b4_19445744',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '78cb72f90b1785151795abcca0f33df0749a7d84' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_plugins/membership/views/controllers/subscribe_index.html',
      1 => 1479204282,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b5174737ba5b4_19445744 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_style')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.style.php';
if (!is_callable('smarty_block_block_decorator')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.block_decorator.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_block_form')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_math')) require_once '/var/www/biyebiye/public_html/ow_libraries/vendor/smarty/smarty/libs/plugins/function.math.php';
if (!is_callable('smarty_function_cycle')) require_once '/var/www/biyebiye/public_html/ow_libraries/vendor/smarty/smarty/libs/plugins/function.cycle.php';
if (!is_callable('smarty_function_input')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_submit')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.submit.php';
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('style', array());
$_block_repeat1=true;
echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>


.ow_action_allowed {
    display: block;
    height: 16px;
    width: 100%;
    background-repeat: no-repeat;
    background-position: center center;
}

.ow_membership_column {
    width: 110px;
}

td.ow_membership_plans {
    text-align: left;
    vertical-align: top;
}

.ow_membership_plans ul li {
    margin-bottom: 5px;
}

.ow_table_1 tr td.ow_plans_td_empty,
.ow_table_1 tr td.ow_gateways_td_empty {
    border: none;
}
.ow_table_1 tr td.ow_plans_td_empty + td,
.ow_table_1 tr td.ow_gateways_td_empty + td {
    border-left-width: 1px;
}

<?php $_block_repeat1=false;
echo smarty_block_style(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>


<?php if (isset($_smarty_tpl->tpl_vars['current']->value)) {?>
	<?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('block_decorator', array('name'=>'box','type'=>'empty','addClass'=>"ow_stdmargin"));
$_block_repeat1=true;
echo smarty_block_block_decorator(array('name'=>'box','type'=>'empty','addClass'=>"ow_stdmargin"), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

		<?php echo smarty_function_text(array('key'=>'membership+membership'),$_smarty_tpl);?>
: <span class="ow_remark ow_small"><?php if (isset($_smarty_tpl->tpl_vars['currentTitle']->value)) {
echo $_smarty_tpl->tpl_vars['currentTitle']->value;
if ($_smarty_tpl->tpl_vars['current']->value->recurring) {?> (<?php echo smarty_function_text(array('key'=>'membership+recurring'),$_smarty_tpl);?>
)<?php }
}?></span><br />
		<?php echo smarty_function_text(array('key'=>'membership+expires'),$_smarty_tpl);?>
: <span class="ow_remark ow_small"><?php echo MEMBERSHIP_BOL_MembershipService::formatDate(array('timestamp'=>$_smarty_tpl->tpl_vars['current']->value->expirationStamp),$_smarty_tpl);?>
</span>
	<?php $_block_repeat1=false;
echo smarty_block_block_decorator(array('name'=>'box','type'=>'empty','addClass'=>"ow_stdmargin"), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

<?php }
if (!empty($_smarty_tpl->tpl_vars['menu']->value)) {?>
<div class="ow_content_menu_wrap ow_padding">
    <?php echo $_smarty_tpl->tpl_vars['menu']->value;?>

</div>
<?php }?>

<?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('name'=>'subscribe-form'));
$_block_repeat1=true;
echo smarty_block_form(array('name'=>'subscribe-form'), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

<div class="ow_automargin ow_superwide">
<table class="ow_stdmargin ow_table_1 ow_subscribe_table">

<tr class="ow_tr_first ow_tr_last">
    <th><?php echo smarty_function_text(array('key'=>'membership+action'),$_smarty_tpl);?>
</th>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['mTypePermissions']->value, 'mt');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['mt']->value) {
?>
    <th class="ow_membership_column <?php if ($_smarty_tpl->tpl_vars['mt']->value['current']) {?> ow_highbox<?php }?>"><?php echo $_smarty_tpl->tpl_vars['mt']->value['title'];?>
</th>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</tr>

<tr class="ow_tr_delimiter"><td></td></tr>

<?php $_smarty_tpl->_assignInScope('perm', "permissions");
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['groupActionList']->value, 'groupAction');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['groupAction']->value) {
?>
    <?php smarty_function_math(array('equation'=>"count",'count'=>count($_smarty_tpl->tpl_vars['groupAction']->value['actions']),'assign'=>'size'),$_smarty_tpl);?>

    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['groupAction']->value['actions'], 'action', false, NULL, 'itm', array (
  'first' => true,
  'last' => true,
  'index' => true,
  'iteration' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['action']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_itm']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_itm']->value['index']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_itm']->value['first'] = !$_smarty_tpl->tpl_vars['__smarty_foreach_itm']->value['index'];
$_smarty_tpl->tpl_vars['__smarty_foreach_itm']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_itm']->value['iteration'] == $_smarty_tpl->tpl_vars['__smarty_foreach_itm']->value['total'];
?>
	<tr class="ow_tr_first">
        <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_itm']->value['first']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_itm']->value['first'] : null)) {?>
            <th colspan="<?php echo $_smarty_tpl->tpl_vars['typesNumber']->value+1;?>
" ><?php if (!empty($_smarty_tpl->tpl_vars['labels']->value[$_smarty_tpl->tpl_vars['groupAction']->value['name']])) {
echo $_smarty_tpl->tpl_vars['labels']->value[$_smarty_tpl->tpl_vars['groupAction']->value['name']]['label'];
} else {
echo $_smarty_tpl->tpl_vars['groupAction']->value['name'];
}?></th>
        <?php }?>
	</tr>
	<tr <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_itm']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_itm']->value['last'] : null)) {?>class="ow_tr_last"<?php }?>>
        <?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'default', 'altClass', null);
echo smarty_function_cycle(array('values'=>'ow_alt1,ow_alt2'),$_smarty_tpl);
$_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);
?>

        <?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'default', 'actionName', null);
echo $_smarty_tpl->tpl_vars['action']->value->name;
$_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);
?>

        <td class="<?php echo $_smarty_tpl->tpl_vars['altClass']->value;?>
 ow_txtleft ow_small"><?php if (!empty($_smarty_tpl->tpl_vars['labels']->value[$_smarty_tpl->tpl_vars['groupAction']->value['name']]['actions'][$_smarty_tpl->tpl_vars['actionName']->value])) {
echo $_smarty_tpl->tpl_vars['labels']->value[$_smarty_tpl->tpl_vars['groupAction']->value['name']]['actions'][$_smarty_tpl->tpl_vars['actionName']->value];
} else {
echo $_smarty_tpl->tpl_vars['actionName']->value;
}?></td>
        <?php $_smarty_tpl->_assignInScope('actionId', $_smarty_tpl->tpl_vars['action']->value->id);
?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['mTypePermissions']->value, 'mt');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['mt']->value) {
?>
        <td class="<?php echo $_smarty_tpl->tpl_vars['altClass']->value;
if ($_smarty_tpl->tpl_vars['mt']->value['current']) {?> ow_highbox<?php }?>">
            <?php if (isset($_smarty_tpl->tpl_vars['mt']->value[$_smarty_tpl->tpl_vars['perm']->value][$_smarty_tpl->tpl_vars['actionId']->value])) {?><span class="ow_action_allowed ow_ic_ok"> </span><?php }?>
        </td>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

    </tr>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

    <tr class="ow_tr_delimiter"><td></td></tr>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

<?php if ($_smarty_tpl->tpl_vars['plansNumber']->value) {?>
<tr class="ow_tr_first">
    <td class="ow_plans_td_empty"></td>
    <?php $_smarty_tpl->_assignInScope('firstSet', '0');
?>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['mTypePermissions']->value, 'mt');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['mt']->value) {
?>
    <td class="ow_alt1 ow_membership_plans<?php if ($_smarty_tpl->tpl_vars['mt']->value['current']) {?> ow_highbox<?php }?>">

    <ul class="ow_small">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['mt']->value['plans'], 'plan');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['plan']->value) {
?>
        <li>
        <?php if (!$_smarty_tpl->tpl_vars['firstSet']->value) {?>
            <?php echo smarty_function_input(array('name'=>'plan','value'=>$_smarty_tpl->tpl_vars['plan']->value['dto']->id,'label'=>$_smarty_tpl->tpl_vars['plan']->value['plan_format'],'checked'=>'checked'),$_smarty_tpl);?>

            <?php $_smarty_tpl->_assignInScope('firstSet', '1');
?>
        <?php } else { ?>
            <?php echo smarty_function_input(array('name'=>'plan','value'=>$_smarty_tpl->tpl_vars['plan']->value['dto']->id,'label'=>$_smarty_tpl->tpl_vars['plan']->value['plan_format']),$_smarty_tpl);?>

        <?php }?>
        </li>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

    </ul>

    </td>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</tr>
<tr class="ow_center ow_tr_last">
    <td class="ow_gateways_td_empty"></td>
    <td colspan="<?php echo $_smarty_tpl->tpl_vars['typesNumber']->value;?>
">
        <?php echo smarty_function_input(array('name'=>'gateway'),$_smarty_tpl);?>

    </td>
</tr>
<?php }?>

</table>
<?php if ($_smarty_tpl->tpl_vars['gatewaysActive']->value && $_smarty_tpl->tpl_vars['plansNumber']->value) {?><div class="clearfix ow_stdmargin"><div class="ow_right"><?php echo smarty_function_submit(array('name'=>'subscribe','class'=>'ow_positive'),$_smarty_tpl);?>
</div></div><?php }?>
</div>
<?php $_block_repeat1=false;
echo smarty_block_form(array('name'=>'subscribe-form'), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);
}
}
