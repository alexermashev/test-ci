<?php
/* Smarty version 3.1.30, created on 2018-07-19 04:48:19
  from "/var/www/biyebiye/public_html/ow_plugins/bookmarks/views/components/avatar_user_list.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b505053c92c75_22430106',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '47bad663c9e3cfb49bc1c4afd0031f07e8716932' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_plugins/bookmarks/views/components/avatar_user_list.html',
      1 => 1479204280,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b505053c92c75_22430106 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
?>


<div class="ow_lp_avatars<?php if (!empty($_smarty_tpl->tpl_vars['css_class']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['css_class']->value;
}?>">
    <?php if (empty($_smarty_tpl->tpl_vars['users']->value)) {?>
        <div class="ow_nocontent"><?php echo smarty_function_text(array('key'=>'base+empty_user_avatar_list'),$_smarty_tpl);?>
</div>
    <?php } else { ?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['users']->value, 'user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['user']->value) {
?>
            <div class="ow_avatar<?php if (!empty($_smarty_tpl->tpl_vars['user']->value['class'])) {?> <?php echo $_smarty_tpl->tpl_vars['user']->value['class'];
}?>">
                <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['url'])) {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['user']->value['url'];?>
"><img alt=""<?php if (!empty($_smarty_tpl->tpl_vars['user']->value['title'])) {?> title="<?php echo $_smarty_tpl->tpl_vars['user']->value['title'];?>
"<?php }?> src="<?php echo $_smarty_tpl->tpl_vars['user']->value['src'];?>
" /></a>
                <?php } else { ?>
                <img alt="" <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['title'])) {?> title="<?php echo $_smarty_tpl->tpl_vars['user']->value['title'];?>
"<?php }?> src="<?php echo $_smarty_tpl->tpl_vars['user']->value['src'];?>
" />
                <?php }?>
                <?php if (!empty($_smarty_tpl->tpl_vars['user']->value['label'])) {?><span class="ow_avatar_label"<?php if (!empty($_smarty_tpl->tpl_vars['user']->value['labelColor'])) {?> style="background-color: <?php echo $_smarty_tpl->tpl_vars['user']->value['labelColor'];?>
"<?php }?>><?php echo $_smarty_tpl->tpl_vars['user']->value['label'];?>
</span><?php }?>
            </div>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

        <?php if (!empty($_smarty_tpl->tpl_vars['view_more_array']->value)) {?>
            <a href="<?php echo $_smarty_tpl->tpl_vars['view_more_array']->value['url'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['view_more_array']->value['title'];?>
" class="avatar_list_more_icon"></a>
        <?php }?>
    <?php }?>
</div>
<?php }
}
