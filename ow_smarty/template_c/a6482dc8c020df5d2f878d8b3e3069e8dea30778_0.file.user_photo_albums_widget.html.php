<?php
/* Smarty version 3.1.30, created on 2019-01-22 05:17:54
  from "/Users/esase/Sites/8418/ow_plugins/photo/views/components/user_photo_albums_widget.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c46edd20dffb2_60339227',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a6482dc8c020df5d2f878d8b3e3069e8dea30778' => 
    array (
      0 => '/Users/esase/Sites/8418/ow_plugins/photo/views/components/user_photo_albums_widget.html',
      1 => 1547792046,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c46edd20dffb2_60339227 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_url_for_route')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.url_for_route.php';
if (!is_callable('smarty_function_text')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.text.php';
?>
<div class="clearfix ow_lp_albums">	
    <?php if ($_smarty_tpl->tpl_vars['showTitles']->value) {?>
	    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['albums']->value, 'album');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['album']->value) {
?>
            <div class="clearfix ow_smallmargin">
                <div class="ow_lp_wrapper<?php if (!empty($_smarty_tpl->tpl_vars['album']->value['class'])) {?> <?php echo $_smarty_tpl->tpl_vars['album']->value['class'];
}?>">
                    <?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'url', null, null);
?>

                        <?php echo smarty_function_url_for_route(array('for'=>"photo_user_album:[user=>".((string)$_smarty_tpl->tpl_vars['user']->value).", album=>".((string)$_smarty_tpl->tpl_vars['album']->value['dto']->id)."]"),$_smarty_tpl);?>

                    <?php $_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);
?>

		            <a href="<?php echo $_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'url');?>
">
                        <img title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['album']->value['dto']->name, ENT_QUOTES, 'UTF-8', true);?>
"  alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['album']->value['dto']->name, ENT_QUOTES, 'UTF-8', true);?>
"  src="<?php echo $_smarty_tpl->tpl_vars['album']->value['cover'];?>
" width="100" height="100" />
                    </a>
                </div>
                <div class="ow_lp_label ow_small">
                    <a href="<?php echo $_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'url');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['album']->value['dto']->name, ENT_QUOTES, 'UTF-8', true);?>
</a>
                </div>
            </div>
	    <?php
}
} else {
?>

	        <div class="ow_nocontent"><?php echo smarty_function_text(array('key'=>'photo+no_photo_found'),$_smarty_tpl);?>
</div>
	    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

    <?php } else { ?>
        <div class="ow_lp_photos ow_center">
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['albums']->value, 'album');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['album']->value) {
?>
                <?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'url', null, null);
?>

                    <?php echo smarty_function_url_for_route(array('for'=>"photo_user_album:[user=>".((string)$_smarty_tpl->tpl_vars['user']->value).", album=>".((string)$_smarty_tpl->tpl_vars['album']->value['dto']->id)."]"),$_smarty_tpl);?>

                <?php $_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);
?>

                <a class="ow_lp_wrapper<?php if (!empty($_smarty_tpl->tpl_vars['album']->value['class'])) {?> <?php echo $_smarty_tpl->tpl_vars['album']->value['class'];
}?>" href="<?php echo $_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'url');?>
">
                    <img title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['album']->value['dto']->name, ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['album']->value['dto']->name, ENT_QUOTES, 'UTF-8', true);?>
" src="<?php echo $_smarty_tpl->tpl_vars['album']->value['cover'];?>
" />
                </a>
            <?php
}
} else {
?>

                <div class="ow_nocontent"><?php echo smarty_function_text(array('key'=>'photo+no_photo_found'),$_smarty_tpl);?>
</div>
            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

        </div>
    <?php }?>
</div>
<?php }
}
