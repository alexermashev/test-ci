<?php
/* Smarty version 3.1.30, created on 2018-07-20 14:17:02
  from "/var/www/biyebiye/public_html/ow_system_plugins/base/views/sitemap_part.xml" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b52271e8cbb28_21248844',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c64ca9b05004dbe77e74f9f8b6c9a250f895e193' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_system_plugins/base/views/sitemap_part.xml',
      1 => 1479204252,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b52271e8cbb28_21248844 (Smarty_Internal_Template $_smarty_tpl) {
echo '<?xml ';?>
version="1.0" encoding="UTF-8"<?php echo '?>';?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['urls']->value, 'url');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['url']->value) {
?>
    <url>
        <changefreq><?php echo $_smarty_tpl->tpl_vars['url']->value['changefreq'];?>
</changefreq>
        <priority><?php echo $_smarty_tpl->tpl_vars['url']->value['priority'];?>
</priority>
        <loc><?php echo $_smarty_tpl->tpl_vars['url']->value['url'];?>
</loc>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['url']->value['alternateLanguages'], 'langData');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['langData']->value) {
?>
        <xhtml:link rel="alternate" hreflang="<?php echo $_smarty_tpl->tpl_vars['langData']->value['code'];?>
" href="<?php echo $_smarty_tpl->tpl_vars['langData']->value['url'];?>
" />
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

    </url>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</urlset>
<?php }
}
