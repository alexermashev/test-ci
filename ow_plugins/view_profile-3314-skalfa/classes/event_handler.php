<?php

class VIEWPROFILE_CLASS_EventHandler
{
    /**
     * @var VIEWPROFILE_BOL_Service
     */
    private $service;

    /**
     * Class instance
     *
     * @var VIEWPROFILE_CLASS_EventHandler
     */
    private static $classInstance;

    /**
     * Class constructor
     */
    private function __construct()
    {
        $this->service = VIEWPROFILE_BOL_Service::getInstance();
    }

    /**
     * Get instance
     *
     * @return VIEWPROFILE_CLASS_EventHandler
     */
    public static function getInstance()
    {
        if (self::$classInstance === null) {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    public function init()
    {
        OW::getEventManager()->bind(OW_EventManager::ON_PLUGINS_INIT, [$this, 'afterInit']);
    }

    public function afterInit()
    {
        $em = OW::getEventManager();

        $em->bind('admin.add_auth_labels', [$this, 'addAuthLabels']);
        $em->bind($em::ON_AFTER_ROUTE, array($this, 'showPopup'));
        $em->bind($em::ON_USER_REGISTER, array($this, 'setLastTimeInEdit'));
        $em->bind('base.event.on_get_empty_questions_list', array($this, 'onGetEmptyQuestionsList'));
//        $em->bind('base.questions_field_get_value', array($this, 'onQuestionsFieldGetValue'));

        //view_profile action
        $em->bind('base.get_user_view_questions', array($this, 'onGetUserViewQuestions'));
        $em->bind('class.get_instance.BASE_CMP_UserViewWidget', array($this, 'onUserViewWidget'));

        //send_profile action
        $em->bind(BASE_CMP_ProfileActionToolbar::EVENT_NAME, array($this, 'addProfileActionToolbar'));
        $em->bind('class.get_instance.BASE_CMP_UserViewSection', array($this, 'onUserViewSection'));
        $em->bind('navigation.onGetMenuItems', array($this, 'onGetMenuItems'));
        $em->bind(BASE_CMP_QuickLinksWidget::EVENT_NAME, array($this, 'addQuickLink'));
    }

    /**
     * Add auth labels
     * @param BASE_CLASS_EventCollector $event
     */
    public function addAuthLabels( BASE_CLASS_EventCollector $event )
    {
        $language = OW::getLanguage();

        $event->add(
            [
                'viewprofile' => [
                    'label' => $language->text('viewprofile', 'auth_group_label'),
                    'actions' => [
                        'view_profile' => $language->text('viewprofile', 'auth_action_label_view_profile'),
                        'send_profile' => $language->text('viewprofile', 'auth_action_label_send_profile'),
                    ],
                ]
            ]
        );
    }

    /**
     * Several functions
     * @param OW_Event $event
     */
    public function showPopup( OW_Event $event )
    {
        if ( OW::getRequest()->isAjax() )
        {
            return;
        }

        if (!OW::getUser()->isAuthenticated())
        {
            return;
        }

        if ( OW::getUser()->isAdmin() )
        {
            return;
        }

        $handlerAtr = OW::getRequestHandler()->getHandlerAttributes();

        if ( $handlerAtr['controller'] == 'MEMBERSHIP_CTRL_Subscribe' && $_POST['back'] == 'profile' )
        {
            if ( !OW::getUser()->isAuthorized('viewprofile', 'view_profile') )
            {
                OW::getFeedback()->info(OW::getLanguage()->text('viewprofile', 'info_redirect_to_subscribe'));
            }
        }
        else if ( $handlerAtr['controller'] == 'BASE_CTRL_Edit' && $handlerAtr['action'] == 'index' )
        {
            $this->service->setLastTimeInEdit(1);
        }
        else
        {
            $userId = OW::getUser()->getId();

            $questionList = BOL_QuestionService::getInstance()->getEmptyRequiredQuestionsList($userId);

            if ( !empty($questionList) )
            {
                $lastTime = $this->service->findLastVisitByUserId($userId);
                $questionsEditStamp = OW::getConfig()->getValue('base', 'profile_question_edit_stamp');

                if ( $lastTime )
                {
                    return;
                }

                if ( time() - $lastTime->dateTime >= VIEWPROFILE_BOL_Service::DATE_FOR_REDIRECT  && $questionsEditStamp < (int) $lastTime->completeTime )
                {
                    $this->service->setLastTimeInEdit(1);
                    OW::getApplication()->redirect(OW::getRouter()->uriForRoute('base_edit_user_datails', array('userId' => $userId)));
                }
            }
        }
    }

    /**
     * Visit a edit profile
     */
    public function setLastTimeInEdit( OW_Event $event )
    {
        $params = $event->getParams();

        if ( $params['method'] != 'facebook' )
        {
            $this->service->setLastTimeInEdit();
        }
    }

    /**
     * Return empty array if don't spent a week
     * @param OW_Event $event
     */
    public function onGetEmptyQuestionsList( OW_Event $event )
    {
        $data = $event->getData();
        $questionsCount = count($data);

        if ( $questionsCount > 0 )
        {
            $userId = OW::getUser()->getId();
            $lastTime = $this->service->findLastVisitByUserId($userId);
            $questionsEditStamp = OW::getConfig()->getValue('base', 'profile_question_edit_stamp');

            if ( $lastTime )
            {
                return;
            }

            if ( time() - $lastTime->dateTime < VIEWPROFILE_BOL_Service::DATE_FOR_REDIRECT && $questionsEditStamp < (int) $lastTime->completeTime )
            {
                $event->setData([]);
            }
            else
            {
                $this->service->setLastTimeInEdit(2);
            }
        }
    }

    /**
     * Replace empty question's value by default
     * @param OW_Event $event
     */
    public function onQuestionsFieldGetValue( OW_Event $event )
    {
        $params = $event->getParams();
        $match = explode('_', $params['fieldName']);

        if ( $params['value'] == null && $match[0] != 'match' )
        {
            $event->setData(OW::getLanguage()->text('viewprofile', 'label_no_question_value'));
        }
    }

    /**
     * Return questions for free or VIP user
     * @param OW_Event $event
     */
    public function onGetUserViewQuestions( OW_Event $event )
    {
        $params = $event->getParams();
        $authService = BOL_AuthorizationService::getInstance();
        $userId = OW::getUser()->getId();
        $widgetUserId = $params['userId'];

        if ( $widgetUserId == $userId || $params['adminMode'] == true || $authService->isActionAuthorizedForUser($userId, $authService::ADMIN_GROUP_NAME) ) return;

        $isActionViewProfile = OW::getUser()->isAuthorized('viewprofile', 'view_profile');

        if ( $isActionViewProfile && isset($_GET['view_profile']) ) return;

        $data = $event->getData();
        $viewQuestions = VIEWPROFILE_BOL_Service::getFreeQuestions();
        $newQuestions = [];

        foreach ( $data as $question )
        {
            if ( in_array($question['name'], $viewQuestions) )
            {
                $newQuestions[] = $question;
            }
        }

        $event->setData($newQuestions);
    }

    /**
     * On user view section (desktop)
     * @param OW_Event $event
     */
    public function onUserViewSection( OW_Event $event )
    {
        if ( OW::getRequest()->isPost() && isset($_POST['form_name']) || $_POST['form_name'] == 'sendingForm' )
        {
            $params = $event->getParams();
            $event->setData(OW::getClassInstanceArray('VIEWPROFILE_CMP_UserViewSection', $params['arguments']));
        }
    }

    /**
     * On user view widget (desktop)
     * @param OW_Event $event
     */
    public function onUserViewWidget( OW_Event $event )
    {
        $authService = BOL_AuthorizationService::getInstance();
        $userId = OW::getUser()->getId();

        $params = $event->getParams();

        $paramsWidget = $params['arguments'][0];
        $widgetUserId = $paramsWidget->additionalParamList['entityId'];

        if ( $widgetUserId == $userId || $authService->isActionAuthorizedForUser($userId, $authService::ADMIN_GROUP_NAME) )
        {
            return;
        }

        $event->setData(OW::getClassInstanceArray('VIEWPROFILE_CMP_UserViewWidget', $params['arguments']));
    }

    /**
     * Add a button SendProfileTo to ProfileActionToolbar
     * @param BASE_CLASS_EventCollector $event
     */
    public function addProfileActionToolbar( BASE_CLASS_EventCollector $event )
    {
        $language = OW::getLanguage();
        $params = $event->getParams();
        $uniqId = 'vp' . rand(10, 1000000);
        $error = false;

        if ( empty($params['userId']) )
        {
            return;
        }

        $loggedUserId = OW::getUser()->getId();
        $userId = (int) $params['userId'];

        $status = BOL_AuthorizationService::getInstance()->getActionStatus('viewprofile', 'send_profile');

        if ( $status['status'] == BOL_AuthorizationService::STATUS_PROMOTED )
        {
            $script = '$({$link}).click(function(){
                        window.OW.error({$message});
                    });';

            $script = UTIL_JsGenerator::composeJsString($script, array('link' => '#'. $uniqId, 'message' => $status['msg'] ));
            OW::getDocument()->addOnloadScript($script);

            $error = true;
        }
        else if ( $status['status'] != BOL_AuthorizationService::STATUS_AVAILABLE )
        {
            return;
        }

        $resultArray = array(
            BASE_CMP_ProfileActionToolbar::DATA_KEY_LINK_ID => $uniqId,
            BASE_CMP_ProfileActionToolbar::DATA_KEY_ITEM_KEY => VIEWPROFILE_BOL_Service::PLUGIN_KEY . '.action',
            BASE_CMP_ProfileActionToolbar::DATA_KEY_LABEL => $language->text('viewprofile', 'label_send_email'),
            BASE_CMP_ProfileActionToolbar::DATA_KEY_LINK_ORDER => 1
        );

        $event->add($resultArray);

        if ( $error ) return;

        $whoseId = $userId;

        $routePath = '';
        if ( !empty(OW::getRouter()->getUsedRoute()) )
        {
            $routePath = OW::getRouter()->getUsedRoute()->getRoutePath();
            if ( $routePath == 'user/:username' )
            {
                $username = BOL_UserService::getInstance()->getUserName($userId);
                $routePath = 'user/' . $username;
            }
        }

        if ( empty($whoseId) )
        {
            OW::getDocument()->addScriptDeclaration(
                UTIL_JsGenerator::composeJsString(';
                    $(document.getElementById({$id})).on("click", function()
                    {
                        OW.error({$msg});
                    });', array(
                        'id' => $uniqId,
                        'msg' => $language->text(VIEWPROFILE_BOL_Service::PLUGIN_KEY, 'not_in_interact_error_msg')
                    )
                )
            );

            return;
        }

        OW::getDocument()->addScriptDeclaration(
            UTIL_JsGenerator::composeJsString(';
                $(document.getElementById({$id})).on("click", function()
                {
                    var ajaxFB = OW.ajaxFloatBox("VIEWPROFILE_CMP_SendEmail", [{$userId}, {$whoseId}, {$backUrl}], {
                        width : 500,
                        title: {$title}
                    });
                });', array(
                    'id' => $uniqId,
                    'userId' => $loggedUserId,
                    'backUrl' => urlencode($routePath),
                    'whoseId' => $whoseId,
                    'title' => OW::getLanguage()->text(VIEWPROFILE_BOL_Service::PLUGIN_KEY, "send_email_floatbox_title")
                )
            )
        );
    }

    /**
     * Get menu items
     * @param OW_Event $event
     */
    public function onGetMenuItems( OW_Event $event )
    {
        $loggedUserId = OW::getUser()->getId();

        $isActionSendProfile = BOL_AuthorizationService::getInstance()->isActionAuthorizedForUser($loggedUserId, 'viewprofile', 'send_profile');

        if ( $isActionSendProfile )
        {
            return;
        }

        $data = $event->getData();
        $newData = [];

        foreach ( $data as $item )
        {
            if ( $item->getPrefix() != VIEWPROFILE_BOL_Service::PLUGIN_KEY )
            {
                $newData[] = $item;
            }
        }

        $event->setData($newData);
    }

    /**
     * Add quick link
     * @param BASE_CLASS_EventCollector $event
     */
    public function addQuickLink( BASE_CLASS_EventCollector $event )
    {
        $count = VIEWPROFILE_BOL_Service::getInstance()->findMySentProfilesCount(OW::getUser()->getId());

        if ( empty($count) )
        {
            return;
        }

        $router = OW_Router::getInstance();

        $event->add(array(
            BASE_CMP_QuickLinksWidget::DATA_KEY_LABEL => OW::getLanguage()->text('viewprofile', 'quick_link_index'),
            BASE_CMP_QuickLinksWidget::DATA_KEY_URL => $router->urlForRoute(VIEWPROFILE_BOL_Service::PLUGIN_KEY . '_main_sent_profiles'),
            BASE_CMP_QuickLinksWidget::DATA_KEY_COUNT => $count,
            BASE_CMP_QuickLinksWidget::DATA_KEY_COUNT_URL => $router->urlForRoute(VIEWPROFILE_BOL_Service::PLUGIN_KEY . '_main_sent_profiles'),
        ));
    }
}