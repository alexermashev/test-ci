<?php
/* Smarty version 3.1.30, created on 2019-01-18 01:23:36
  from "/Users/esase/Sites/8418/ow_plugins/membership/views/components/my_membership_widget.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c4170e84ceca0_65104262',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fc3ded502c61b6fda2228a0232424e82c7e6bf78' => 
    array (
      0 => '/Users/esase/Sites/8418/ow_plugins/membership/views/components/my_membership_widget.html',
      1 => 1547792046,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c4170e84ceca0_65104262 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/function.text.php';
?>
<table class="ow_table_3 ow_automargin ow_smallmargin">
    <tr class="ow_tr_first">
        <td class="ow_label"><?php echo smarty_function_text(array('key'=>'membership+membership'),$_smarty_tpl);?>
</td>
        <td class="ow_value"><?php if (isset($_smarty_tpl->tpl_vars['title']->value)) {
echo $_smarty_tpl->tpl_vars['title']->value;
if ($_smarty_tpl->tpl_vars['membership']->value->recurring) {?> <span class="ow_small">(<?php echo smarty_function_text(array('key'=>'membership+recurring'),$_smarty_tpl);?>
)</span><?php }
}?></td>
    </tr>
    <tr class="ow_tr_last">
        <td class="ow_label"><?php echo smarty_function_text(array('key'=>'membership+expires'),$_smarty_tpl);?>
</td>
        <td class="ow_value"><?php echo MEMBERSHIP_BOL_MembershipService::formatDate(array('timestamp'=>$_smarty_tpl->tpl_vars['membership']->value->expirationStamp),$_smarty_tpl);?>
</td>
    </tr>
</table><?php }
}
