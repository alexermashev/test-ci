<?php

class CUSTOMLAYOUT_BOL_Service
{
    const PLUGIN_KEY = 'customlayout';

    /**
     * Constructor.
     */
    private function __construct()
    {

    }

    /**
     * Singleton instance.
     *
     * @var CUSTOMLAYOUT_BOL_Service
     */
    private static $classInstance;

    /**
     * Returns an instance of class (singleton pattern implementation).
     *
     * @return CUSTOMLAYOUT_BOL_Service
     */
    public static function getInstance()
    {
        if (self::$classInstance === null) {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    public function getGenderType($userGender)
    {
        SKADATE_BOL_AccountTypeToGenderService::getInstance()->getGender($userGender);

        if ($userGender == 2) {
            return 'female';
        }

        return 'male';
    }

    public function getCustomLineByUserIds ( $userIds )
    {
        $customQuestions = self::getCustomQuestions();

        $viewQuestionList = BOL_QuestionService::getInstance()->getQuestionData($userIds, $customQuestions);

        $customLineByUserIds = [];

        foreach ( $viewQuestionList as $userId => $questions )
        {
            $customLine = [];
            foreach ( $questions as $key => $value )
            {
                $customLine[] = BOL_QuestionService::getInstance()->getQuestionValueLang($key, $value);
            }

            $customLineByUserIds[$userId] = implode(", ", $customLine);;
        }

        return $customLineByUserIds;
    }

    public static function getCustomQuestions()
    {
        return [
            'relationship',
            '31d5d3d7ff420e340cfe3f17c534eba3'
        ];
    }
}