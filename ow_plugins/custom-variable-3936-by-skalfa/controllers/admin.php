<?php

class SKCV_CTRL_Admin extends ADMIN_CTRL_Abstract
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $script = '
        $("a.lang_label").on( "click",  function(){
            OW.ajaxFloatBox("BASE_CMP_LanguageValueEdit", [\'skcv\', \'custom_key\', true], { title: '.json_encode(OW::getLanguage()->text('base', 'questions_edit_question_label_title')).' } );
        });
        
        OW.bind( "admin.language_key_edit_success", function( params ) {
            
            console.log(params);
            if ( params && params.result == "success" && params.key )
            {
                    // close floatbox
                    var floatbox = OW.getActiveFloatBox();
                    floatbox.close();
                    
                    location.reload(true);
            }
        });
        ';

        OW::getDocument()->addOnloadScript($script);

        $langLabel = OW::getLanguage()->text(SKCV_BOL_Service::PLUGIN_KEY, SKCV_BOL_Service::CUSTOM_KEY_LANG);
        $this->assign('langLabel', $langLabel);

        // set current page's settings
        $this->setPageTitle(OW::getLanguage()->text(SKCV_BOL_Service::PLUGIN_KEY, 'admin_skcv'));
        $this->setPageHeading(OW::getLanguage()->text(SKCV_BOL_Service::PLUGIN_KEY, 'admin_skcv'));
    }
}