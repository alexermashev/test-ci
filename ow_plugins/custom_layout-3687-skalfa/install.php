<?php

$pluginKey = 'customlayout';

$plugin = OW::getPluginManager()->getPlugin($pluginKey);
OW::getLanguage()->importLangsFromDir($plugin->getRootDir() . 'langs');

OW::getPluginManager()->addUninstallRouteName($pluginKey, $pluginKey . '.uninstall');

OW::getPluginManager()->addPluginSettingsRouteName($pluginKey, 'customlayout-admin-settings');