<?php
/* Smarty version 3.1.30, created on 2018-07-26 01:47:34
  from "/var/www/biyebiye/public_html/ow_system_plugins/admin/views/controllers/pages_edit_local_index.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b59607664d1b9_47977929',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'da1e381294d8d2b042a6afa98f76f2d3a69f12c8' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_system_plugins/admin/views/controllers/pages_edit_local_index.html',
      1 => 1479204252,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b59607664d1b9_47977929 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_url_for_route')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.url_for_route.php';
if (!is_callable('smarty_function_decorator')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.decorator.php';
if (!is_callable('smarty_block_form')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_cycle')) require_once '/var/www/biyebiye/public_html/ow_libraries/vendor/smarty/smarty/libs/plugins/function.cycle.php';
if (!is_callable('smarty_function_label')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.label.php';
if (!is_callable('smarty_function_input')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_error')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.error.php';
if (!is_callable('smarty_function_desc')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.desc.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_url_for')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.url_for.php';
if (!is_callable('smarty_function_submit')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.submit.php';
$_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'default', "back_url", null);
echo smarty_function_url_for_route(array('for'=>"admin_pages_main"),$_smarty_tpl);
$_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);
?>


<div class="ow_stdmargin"><?php echo smarty_function_decorator(array('name'=>"button",'class'=>"ow_ic_left_arrow",'onclick'=>"location.href='".((string)$_smarty_tpl->tpl_vars['back_url']->value)."';",'langLabel'=>"base+pages_back"),$_smarty_tpl);?>
</div>

<?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('form', array('name'=>"edit-form"));
$_block_repeat1=true;
echo smarty_block_form(array('name'=>"edit-form"), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

<input id="edit-form-command" type="hidden" name="command" value="save" />
<table class="ow_form ow_table_1">

    <tr id="title-tr" class="<?php echo smarty_function_cycle(array('values'=>"ow_alt2,ow_alt1"),$_smarty_tpl);?>
 ow_tr_first">
        <td class="ow_label">
        	<?php echo smarty_function_label(array('name'=>"title"),$_smarty_tpl);?>

       	</td>
        <td class="ow_value">
        	<?php echo smarty_function_input(array('name'=>"title"),$_smarty_tpl);?>

        	<br /><?php echo smarty_function_error(array('name'=>"title"),$_smarty_tpl);?>

        </td>
        <td></td>
    </tr>

    <tr id="title-tr" class="<?php echo smarty_function_cycle(array('values'=>"ow_alt2,ow_alt1"),$_smarty_tpl);?>
">
        <td class="ow_label">
        	<?php echo smarty_function_label(array('name'=>"name"),$_smarty_tpl);?>

       	</td>
        <td class="ow_value">
        	<?php echo smarty_function_input(array('name'=>"name"),$_smarty_tpl);?>

        	<br /><?php echo smarty_function_error(array('name'=>"name"),$_smarty_tpl);?>

        </td>
        <td></td>                
    </tr>

    <tr id="title-tr" class="<?php echo smarty_function_cycle(array('values'=>"ow_alt2,ow_alt1"),$_smarty_tpl);?>
">
        <td class="ow_label">
        	<?php echo smarty_function_label(array('name'=>"url"),$_smarty_tpl);?>

       	</td>
        <td class="ow_value">
        	http://yoursite.com/<?php echo smarty_function_input(array('name'=>"url",'style'=>"width: 200px;margin-left: 6px;"),$_smarty_tpl);?>
<br />
        	<br /><?php echo smarty_function_error(array('name'=>"url"),$_smarty_tpl);?>

        </td>
        <td></td>                
      </tr>

    <tr class="<?php echo smarty_function_cycle(array('values'=>"ow_alt2,ow_alt1"),$_smarty_tpl);?>
" >
        <td class="ow_label"><?php echo smarty_function_label(array('name'=>"visible-for"),$_smarty_tpl);?>
</td>
        <td class="ow_value">
        	<?php echo smarty_function_input(array('name'=>"visible-for"),$_smarty_tpl);?>

        	<br /><?php echo smarty_function_error(array('name'=>"visible-for"),$_smarty_tpl);?>

       </td>
        <td></td>       
    </tr>

    <tr id="title-tr" class="<?php echo smarty_function_cycle(array('values'=>"ow_alt2,ow_alt1"),$_smarty_tpl);?>
 ow_tr_last">
        <td class="ow_label">
        	<?php echo smarty_function_label(array('name'=>"content"),$_smarty_tpl);?>

       	</td>
        <td class="ow_value">
        	<?php echo smarty_function_input(array('name'=>"content"),$_smarty_tpl);?>

        	<br /><?php echo smarty_function_error(array('name'=>"content"),$_smarty_tpl);?>

        </td>
        <td class="ow_description ow_small"><?php echo smarty_function_desc(array('name'=>"content"),$_smarty_tpl);?>
</td>
    </tr>

    <tr  id="title-tr" class="<?php echo smarty_function_cycle(array('values'=>"ow_alt2,ow_alt1"),$_smarty_tpl);?>
">
    <td class="ow_label">
        <?php echo smarty_function_label(array('name'=>"meta_desc"),$_smarty_tpl);?>

    </td>
    <td class="ow_value">
        <?php echo smarty_function_input(array('name'=>"meta_desc"),$_smarty_tpl);?>

        <br /><?php echo smarty_function_error(array('name'=>"meta_desc"),$_smarty_tpl);?>

    </td>
    <td class="ow_description ow_small" style="width: 30%;"><?php echo smarty_function_desc(array('name'=>"meta_desc"),$_smarty_tpl);?>
</td>
    </tr>

    <tr  id="title-tr" class="<?php echo smarty_function_cycle(array('values'=>"ow_alt2,ow_alt1"),$_smarty_tpl);?>
">
    <td class="ow_label">
        <?php echo smarty_function_label(array('name'=>"meta_keywords"),$_smarty_tpl);?>

    </td>
    <td class="ow_value">
        <?php echo smarty_function_input(array('name'=>"meta_keywords"),$_smarty_tpl);?>

        <br /><?php echo smarty_function_error(array('name'=>"meta_keywords"),$_smarty_tpl);?>

    </td>
    <td class="ow_description ow_small" style="width: 30%;"><?php echo smarty_function_desc(array('name'=>"meta_keywords"),$_smarty_tpl);?>
</td>
    </tr>

 </table>
<div class="clearfix ow_stdmargin ow_btn_delimiter">
    <div class="ow_right">
        <?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'default', "del_js", null);
?>
if(confirm('<?php echo smarty_function_text(array('key'=>'admin+are_you_sure'),$_smarty_tpl);?>
')) location.href='<?php echo smarty_function_url_for(array('for'=>"ADMIN_CTRL_PagesEditLocal:delete:[id=>".((string)$_smarty_tpl->tpl_vars['id']->value)."]"),$_smarty_tpl);?>
';<?php $_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);
?>

        <?php echo smarty_function_decorator(array('name'=>"button",'class'=>"ow_red ow_ic_delete ow_negative",'extraString'=>"onclick=\"".((string)$_smarty_tpl->tpl_vars['del_js']->value)."\"",'langLabel'=>"admin+delete_btn_label"),$_smarty_tpl);?>

        
        <?php echo smarty_function_submit(array('name'=>"save",'class'=>"ow_positive"),$_smarty_tpl);?>

    </div>
</div>    
<?php $_block_repeat1=false;
echo smarty_block_form(array('name'=>"edit-form"), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

<?php }
}
