<?php

class VIEWPROFILE_BOL_LastVisitDao extends OW_BaseDao
{
    /**
     * Singleton instance.
     *
     * @var VIEWPROFILE_BOL_LastVisitDao
     */
    private static $classInstance;

    /**
     * Returns an instance of class (singleton pattern implementation).
     *
     * @return VIEWPROFILE_BOL_LastVisitDao
     */
    public static function getInstance()
    {
        if (self::$classInstance === null) {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    /**
     * @see OW_BaseDao::getDtoClassName()
     *
     */
    public function getDtoClassName()
    {
        return 'VIEWPROFILE_BOL_LastVisit';
    }

    /**
     * @see OW_BaseDao::getTableName()
     *
     */
    public function getTableName()
    {
        return OW_DB_PREFIX . 'viewprofile_last_visit';
    }

    /**
     * Find by userId
     *
     * @param $userId
     * @return array|void
     */
    public function findByUserId( $userId )
    {
        if ( !$userId ) {
            return;
        }

        $example = new OW_Example();
        $example->andFieldEqual('userId', $userId);

        return $this->findObjectByExample($example);
    }
}