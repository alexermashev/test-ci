<?php
/* Smarty version 3.1.30, created on 2018-07-20 01:45:14
  from "/var/www/biyebiye/public_html/ow_plugins/advertisement/views/controllers/admin_index.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b5176ea786d49_22928699',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e2fe8ed6c04500dd2ad0fceb899a2a4adfc4e6f7' => 
    array (
      0 => '/var/www/biyebiye/public_html/ow_plugins/advertisement/views/controllers/admin_index.html',
      1 => 1479204280,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b5176ea786d49_22928699 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_style')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.style.php';
if (!is_callable('smarty_function_decorator')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.decorator.php';
if (!is_callable('smarty_block_block_decorator')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/block.block_decorator.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/biyebiye/public_html/ow_smarty/plugin/function.text.php';
$_smarty_tpl->smarty->_cache['_tag_stack'][] = array('style', array());
$_block_repeat1=true;
echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>


.ow_banner_list tr{
border-top:1px solid #ccc;
}
.ow_banner_controls{
display:none;
}
.banner_label{
    padding-bottom:5px;
    font-weight:bold;
}
.banner_locations{
    font-size:11px;
    padding-top:10px;
}
.banner_code{
width:550px;
overflow:hidden;
}

<?php $_block_repeat1=false;
echo smarty_block_style(array(), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

<?php echo $_smarty_tpl->tpl_vars['menu']->value;?>

<div>
    <div class="ow_stdmargin" style="text-align: right;"><?php echo smarty_function_decorator(array('name'=>'button','langLabel'=>'ads+ads_add_banner','extraString'=>" onclick=\"window.location='".((string)$_smarty_tpl->tpl_vars['addUrl']->value)."'\""),$_smarty_tpl);?>
</div>
    <?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('block_decorator', array('name'=>'box','type'=>'empty','addClass'=>'ow_stdmargin','iconClass'=>'ow_ic_files','langLabel'=>'ads+ads_index_list_box_cap_label'));
$_block_repeat1=true;
echo smarty_block_block_decorator(array('name'=>'box','type'=>'empty','addClass'=>'ow_stdmargin','iconClass'=>'ow_ic_files','langLabel'=>'ads+ads_index_list_box_cap_label'), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

       <table class="ow_banner_list" style="width:100%;">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['banners']->value, 'banner');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['banner']->value) {
?>
            <tr class="ow_high1 <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_banner']->value['first']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_banner']->value['first'] : null)) {?>ow_tr_first<?php }?> <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_banner']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_banner']->value['last'] : null)) {?>ow_tr_last<?php }?>" onmouseover="$('span.ow_banner_controls', $(this)).css({display:'block'});" onmouseout="$('span.ow_banner_controls', $(this)).css({display:'none'});">
                <td style="padding: 10px 15px; width: 68%;">
                    <div class="banner_label"><?php echo $_smarty_tpl->tpl_vars['banner']->value['label'];?>
</div>
                    <div class="banner_code"><?php echo $_smarty_tpl->tpl_vars['banner']->value['code'];?>
</div>
             </td>
             <td style="width: 20%;">
                 <div class="banner_locations">
                 <?php if (empty($_smarty_tpl->tpl_vars['banner']->value['location'])) {?>
                    <?php echo smarty_function_text(array('key'=>'ads+ads_manage_global_label'),$_smarty_tpl);?>

                 <?php } else { ?>
                    <ul>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['banner']->value['location'], 'loc');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['loc']->value) {
?>
                        <li><?php echo $_smarty_tpl->tpl_vars['loc']->value;?>
</li>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                    </ul>
                 <?php }?>
                 </div>
             </td>
             <td class="ow_small" style="text-align:right;width:12%;vertical-align:middle;">
                 <span class="ow_banner_controls">
                     <a class="ow_lbutton" href="<?php echo $_smarty_tpl->tpl_vars['banner']->value['editUrl'];?>
"><?php echo smarty_function_text(array('key'=>'ads+ads_edit_banner_button_label'),$_smarty_tpl);?>
</a>
                     <a onclick="return confirm('<?php echo smarty_function_text(array('key'=>'ads+ads_delete_banner_confirm_message'),$_smarty_tpl);?>
');" class="ow_lbutton ow_red" href="<?php echo $_smarty_tpl->tpl_vars['banner']->value['deleteUrl'];?>
"><?php echo smarty_function_text(array('key'=>'ads+ads_delete_button_label'),$_smarty_tpl);?>
</a>
                    </span>
             </td>
          </tr>
          <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

       </table>
    <?php $_block_repeat1=false;
echo smarty_block_block_decorator(array('name'=>'box','type'=>'empty','addClass'=>'ow_stdmargin','iconClass'=>'ow_ic_files','langLabel'=>'ads+ads_index_list_box_cap_label'), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

</div>
<?php }
}
