<?php
/* Smarty version 3.1.30, created on 2019-01-18 01:23:36
  from "/Users/esase/Sites/8418/ow_system_plugins/base/views/components/context_action.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c4170e87efc25_63306128',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '668cb0a24e4ee668bd1ccc0b258e1df6d21bddc1' => 
    array (
      0 => '/Users/esase/Sites/8418/ow_system_plugins/base/views/components/context_action.html',
      1 => 1547792051,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c4170e87efc25_63306128 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_block_block_decorator')) require_once '/Users/esase/Sites/8418/ow_smarty/plugin/block.block_decorator.php';
?>
<div class="ow_context_action_block clearfix <?php if (!empty($_smarty_tpl->tpl_vars['class']->value)) {
echo $_smarty_tpl->tpl_vars['class']->value;
}?>">
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['actions']->value, 'a');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['a']->value) {
?>
        <div class="ow_context_action">
			<?php if ($_smarty_tpl->tpl_vars['a']->value['action']->getLabel()) {?>
			<a href="<?php if ($_smarty_tpl->tpl_vars['a']->value['action']->getUrl() != null) {
echo $_smarty_tpl->tpl_vars['a']->value['action']->getUrl();
} else { ?>javascript://<?php }?>"<?php if ($_smarty_tpl->tpl_vars['a']->value['action']->getId() != null) {?> id="<?php echo $_smarty_tpl->tpl_vars['a']->value['action']->getId();?>
"<?php }
if ($_smarty_tpl->tpl_vars['a']->value['action']->getAttributes()) {
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['a']->value['action']->getAttributes(), 'attr', false, 'aname');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['aname']->value => $_smarty_tpl->tpl_vars['attr']->value) {
?> <?php echo $_smarty_tpl->tpl_vars['aname']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['attr']->value;?>
"<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
}?>class="ow_context_action_value<?php if ($_smarty_tpl->tpl_vars['a']->value['action']->getClass() != null) {?> <?php echo $_smarty_tpl->tpl_vars['a']->value['action']->getClass();
}?>"><?php echo $_smarty_tpl->tpl_vars['a']->value['action']->getLabel();?>
</a><?php }?>
			<?php if (!empty($_smarty_tpl->tpl_vars['a']->value['subactions'])) {?>
			<span class="ow_context_more"></span>

			<!-- div class="ow_context_action_wrap" -->
			    <?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'default', 'tooltipClass', null);
if ($_smarty_tpl->tpl_vars['a']->value['action']->getClass() != null) {
echo $_smarty_tpl->tpl_vars['a']->value['action']->getClass();?>
_tooltip<?php }
$_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);
?>

			    
				<?php $_smarty_tpl->smarty->_cache['_tag_stack'][] = array('block_decorator', array('name'=>'tooltip','addClass'=>((string)$_smarty_tpl->tpl_vars['tooltipClass']->value)." ow_small ".((string)$_smarty_tpl->tpl_vars['position']->value)));
$_block_repeat1=true;
echo smarty_block_block_decorator(array('name'=>'tooltip','addClass'=>((string)$_smarty_tpl->tpl_vars['tooltipClass']->value)." ow_small ".((string)$_smarty_tpl->tpl_vars['position']->value)), null, $_smarty_tpl, $_block_repeat1);
while ($_block_repeat1) {
ob_start();
?>

				<ul class="ow_context_action_list ow_border">
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['a']->value['subactions'], 'subact');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['subact']->value) {
?>
					<li><a href="<?php if ($_smarty_tpl->tpl_vars['subact']->value->getUrl() != null) {
echo $_smarty_tpl->tpl_vars['subact']->value->getUrl();
} else { ?>javascript://<?php }?>"<?php if ($_smarty_tpl->tpl_vars['subact']->value->getId() != null) {?> id="<?php echo $_smarty_tpl->tpl_vars['subact']->value->getId();?>
"<?php }
if ($_smarty_tpl->tpl_vars['subact']->value->getClass() != null) {?> class="<?php echo $_smarty_tpl->tpl_vars['subact']->value->getClass();?>
"<?php }
if ($_smarty_tpl->tpl_vars['subact']->value->getAttributes()) {
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['subact']->value->getAttributes(), 'sattr', false, 'name');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['name']->value => $_smarty_tpl->tpl_vars['sattr']->value) {
?> <?php echo $_smarty_tpl->tpl_vars['name']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['sattr']->value;?>
"<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
}?>><?php echo $_smarty_tpl->tpl_vars['subact']->value->getLabel();?>
</a></li>
				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

				</ul>
                <?php $_block_repeat1=false;
echo smarty_block_block_decorator(array('name'=>'tooltip','addClass'=>((string)$_smarty_tpl->tpl_vars['tooltipClass']->value)." ow_small ".((string)$_smarty_tpl->tpl_vars['position']->value)), ob_get_clean(), $_smarty_tpl, $_block_repeat1);
}
array_pop($_smarty_tpl->smarty->_cache['_tag_stack']);?>

			<!-- /div -->
			<?php }?>
        </div>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</div><?php }
}
