<?php

AVATARDIMENSIONS_CLASS_EventHandler::getInstance()->init();

$plugin = OW::getPluginManager()->getPlugin('avatardimensions');
$key = strtoupper($plugin->getKey());

OW::getRouter()->addRoute(new OW_Route('avatardimensions-admin-settings', 'admin/avatardimensions/settings', "{$key}_CTRL_Admin", 'index'));