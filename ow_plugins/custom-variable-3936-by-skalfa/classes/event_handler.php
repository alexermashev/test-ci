<?php

/**
 * Copyright (c) 2018, Skalfa LLC
 * All rights reserved.
 *
 * ATTENTION: This commercial software is intended for exclusive use with SkaDate Dating Software (http://www.skadate.com) and is licensed under SkaDate Exclusive License by Skalfa LLC.
 *
 * Full text of this license can be found at http://www.skadate.com/sel.pdf
 */

/**
 * Class SKCV_CLASS_EventHandler
 *
 * @author (A. S. B.) (D. P.) <azazel9966@gmail.com>.
 * @package ow_plugins.pluginkey
 * @since 1.8.4
 */
class SKCV_CLASS_EventHandler
{
    /**
     * @var SKCV_BOL_Service
     */
    private $service;

    /**
     * @var SKCV_CLASS_EventHandler
     */
    private static $instance;

    /**
     * @return SKCV_CLASS_EventHandler
     */
    public static function getInstance()
    {
        if ( static::$instance == null )
        {
            try
            {
                static::$instance = OW::getClassInstance(static::class);
            }
            catch ( ReflectionException $ex )
            {
                static::$instance = new static();
            }
        }

        return static::$instance;
    }

    private function __construct()
    {
        $this->service = SKCV_BOL_Service::getInstance();
    }

    public function init()
    {
        OW::getEventManager()->bind(OW_EventManager::ON_PLUGINS_INIT, [$this, 'afterInit']);
    }

    public function afterInit()
    {
        $eventManager = OW::getEventManager();
        $eventManager->bind('core.get_text', [$this, 'onGetText']);
        $eventManager->bind('admin.plugins_list_view', [$this, 'pluginsListView']);
    }

    public function onGetText( OW_Event $event )
    {
        $params = $event->getParams();

        $text = $this->service->getNewText($params['prefix'], $params['key'], $params['vars']);

        if ( $text )
        {
            $event->setData($text);
        }
    }

    public function pluginsListView( OW_Event $event )
    {
        $data = $event->getData();

        if ( !empty($data['active']) || !empty($data['inactive']) )
        {
            if ( isset($data['active'][SKCV_BOL_Service::PLUGIN_KEY]['un_url']) )
            {
                $data['active'][SKCV_BOL_Service::PLUGIN_KEY]['un_url'] = null;
            }
        }

        $event->setData($data);
    }
}